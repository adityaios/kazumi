//
//  ProductListViewController.h
//  kazumi
//
//  Created by Yashvir on 26/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CatModel.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#import "GlobalValues.h"
#import "Network.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "TSMessage.h"
#import "ProductCollectionViewCell.h"
#import "KazumiMacros.h"
#import "ProductModelClass.h"
#import "UIImageView+AFNetworking.h"
#import "ProductDetailsViewController.h"
#import "MKNumberBadgeView.h"
#import "CartViewController.h"
#import "FilterOptionsViewController.h"
#import "TSMessage.h"
#import "ProductSingleListCollectionViewCell.h"
#import "Helper Class.h"


@interface ProductListViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,FilterProductListDelegate,UISearchBarDelegate>{
    GlobalValues * objglobalValues;
    NSMutableArray * marrProductsList;
    NSInteger intWebPagination;
    UIButton *btnLoadMore;
    BOOL flagProductGridList;
    UIButton *btnGrid;
}


@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic,strong) CatModel * objCatModelPass;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewProduct;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (strong,nonatomic)IBOutlet UILabel*lblNoProduct ;





-(void)setUpUI;
-(void)intializerMethod;
-(void)webserviceCallForProducts;
-(void)webserviceResponseParsing:(NSArray*)arrResponse;
-(void)customBackButton;
-(void)webserviceCallForSearchingProducts:(NSString *)strSearchtext;;

@end
