//
//  Helper Class.m
//  kazumi
//
//  Created by Yashvir on 02/01/16.
//  Copyright © 2016 Nishkrant Media. All rights reserved.
//

#import "Helper Class.h"

@implementation Helper_Class



//if( [@"Some String" caseInsensitiveCompare:@"some string"] == NSOrderedSame ) {
//    // strings are equal except for possibly case
//}


+(BOOL)nullValueCheck:(NSString*)str{
    BOOL flag;
    if (str == nil || str == (id)[NSNull null] || [str caseInsensitiveCompare:@"null"] == NSOrderedSame) {
        flag = TRUE;
    } else {
        flag = FALSE;
    }
    return flag;
}





+ (BOOL) validateUrl: (NSString *)strUrl {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:strUrl];
}




@end
