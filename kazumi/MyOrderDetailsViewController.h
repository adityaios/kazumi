//
//  MyOrderDetailsViewController.h
//  kazumi
//
//  Created by Yashvir on 02/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "Network.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "GlobalValues.h"
#import "Network.h"
#import "MyOrders.h"
#import "Constant.h"
#import "KazumiMacros.h"
#import "UIImageView+AFNetworking.h"

@interface MyOrderDetailsViewController : UIViewController{
    GlobalValues * objglobalValues;
    NSDictionary * dictOrderDetails;
    NSDictionary * dictShippingAddress;
    NSDictionary * dictPaymentAddress;
    NSDictionary * dictShipping;
    NSDictionary * dictSubTotal;
    NSArray * arrProductsList;
    
    NSString  * strProdCount;
}
@property (strong,nonatomic) MyOrders * objOrderPass;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIView *viewFooter;




@property (weak, nonatomic) IBOutlet UILabel *lblOrderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderText;


@property (weak, nonatomic) IBOutlet UILabel *lblItemTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemText;


@property (weak, nonatomic) IBOutlet UILabel *lblTotalTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalText;

@property (weak, nonatomic) IBOutlet UILabel *lblProductDetails;


//SHIPPING ADDRESS
@property (weak, nonatomic) IBOutlet UIView *viewShippingAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingAddressHead;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingAdd1;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingAdd2;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingAdd3;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingAdd4;


//PERMANENT ADDRESS
@property (weak, nonatomic) IBOutlet UIView *viewPermanentAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPermanentAddressHead;
@property (weak, nonatomic) IBOutlet UILabel *lblPermanentAdd1;
@property (weak, nonatomic) IBOutlet UILabel *lblPermanentAdd2;
@property (weak, nonatomic) IBOutlet UILabel *lblPermanentAdd3;
@property (weak, nonatomic) IBOutlet UILabel *lblPermanentAdd4;

//SHIPPING METHOD
@property (weak, nonatomic) IBOutlet UIView *viewShippingMethod;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingMethodHead;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingMethodtitle;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingMethodValue;

//PAYMENT METHOD
@property (weak, nonatomic) IBOutlet UIView * viewPaymentMethod;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentMethodHead;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentMethodtitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentMethodValue;












//Methods
-(void)setUI;
-(void)intializerMethod;
-(void)customBackButton;
-(void)webserviceCallForMyOrders;


@end
