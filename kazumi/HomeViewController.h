//
//  HomeViewController.h
//  kazumi
//
//  Created by Yashvir on 14/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "HomeViewController+HomeWebservice.h"
#import "Network.h"
#import "WebserviceConstant.h"
#import "Reachability.h"
#import "TSMessage.h"
#import "SCLAlertView.h"
#import "Constant.h"
#import "CatModel.h"
#import "DejalActivityView.h"
#import "UIImageView+AFNetworking.h"
#import "GlobalValues.h"
#import "KazumiMacros.h"
#import "ProductListViewController.h"
#import "MKNumberBadgeView.h"
#import "CartViewController.h"

@interface HomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    NSMutableArray * marrCatModelData;
    NSArray * arrFilteredCatModelData;
    GlobalValues * objglobalValues;
}

@property (weak, nonatomic) IBOutlet UISearchBar *serachBar;
@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButton;
@property (weak, nonatomic) IBOutlet UITableView *tblvHomeCat;

-(void)setUpUI;
-(void)intializerMethod;
-(void)cartBarButton;


@end
