//
//  ProductDetailsViewController.h
//  kazumi
//
//  Created by Yashvir on 27/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DejalActivityView.h"
#import "GlobalValues.h"
#import "Network.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "TSMessage.h"
#import "KazumiMacros.h"
#import "UIImageView+AFNetworking.h"
#import "ProductModelClass.h"
#import "Reachability.h"
#import "KazumiMacros.h"
#import "EXPhotoViewer.h"
#import "UIImageView+AFNetworking.h"
#import "ProductDetailOptionViewController.h"
#import "WriteReviewViewController.h"
#import "ReviewsListViewController.h"
#import "RNBlurModalView.h"
#import <Accelerate/Accelerate.h>
#import <QuartzCore/QuartzCore.h>
#import "JTSImageViewController.h"
#import "JTSImageInfo.h"
#import "MKNumberBadgeView.h"
#import "CartViewController.h"
#import "Helper Class.h"




@interface ProductDetailsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    GlobalValues * objglobalValues;
    NSArray * arrImg;
    NSDictionary* dictProductDetail;
    NSArray* arrOptions;
    NSArray* arrProductImages;
    
    NSMutableDictionary * mdictOptionValuesPass;
}


//Methods


@property(nonatomic,strong) ProductModelClass * objProductModelPass;


@property (weak, nonatomic) IBOutlet UITableView *tblvProductDetails;


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *viewProductImgPager;


@property (weak, nonatomic) IBOutlet UIButton *btnAddToWishlist;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCart;

@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblProductCount;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbldiscountValue;
@property (weak, nonatomic) IBOutlet UILabel *lbldiscountedValue;


@property (weak, nonatomic) IBOutlet UIStepper *stepperCount;


-(void)setImageScrollingView:(NSArray*)arrProduct;
-(void)setupScrollView:(UIScrollView*)scrMain;
-(void)setTextOnProductDetailScreen:(NSDictionary *)dict;



- (IBAction)stepperValueChanged:(UIStepper *)sender;
- (IBAction)btnAddToCartClicked:(UIButton *)sender;
- (IBAction)btnAddToWishListClicked:(UIButton *)sender;




-(void)customBackButton;
-(void)setUpUI;
-(void)intializerMethod;
-(void)webserviceCallForProductsDetails;
-(void)showPhotoBrowser:(NSArray *)photoArray;




@end
