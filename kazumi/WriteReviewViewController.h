//
//  WriteReviewViewController.h
//  kazumi
//
//  Created by Yashvir on 05/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "DejalActivityView.h"
#import "GlobalValues.h"
#import "Network.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "TSMessage.h"
#import "KazumiMacros.h"
#import "Reachability.h"
#import "ProductModelClass.h"
#import "TSMessage.h"

@interface WriteReviewViewController : UIViewController
{
    GlobalValues * objGlogalValues;
}


@property(nonatomic,strong) ProductModelClass * objProductModelWriteReview;

@property (weak, nonatomic) IBOutlet ASStarRatingView *starView;
@property (weak, nonatomic) IBOutlet UILabel *lblReview;
@property (weak, nonatomic) IBOutlet UITextView *txtViewReview;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;





- (IBAction)btnCancelClicked:(UIButton *)sender;
- (IBAction)btnSubmitClicked:(UIButton *)sender;



//METHODS
-(void)customBackButton;
-(void)setUI;
-(void)intializerMethod;
-(void)textfieldToolBar:(UITextView *)txfd;


@end
