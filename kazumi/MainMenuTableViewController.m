//
//  MainMenuTableViewController.m
//  kazumi
//
//  Created by Yashvir on 14/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "MainMenuTableViewController.h"

@interface MainMenuTableViewController ()

@end

@implementation MainMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    marr_menu = [[NSMutableArray  alloc]  initWithObjects:@"Home",@"Category",@"About Us",@"Privacy Policy",@"Terms & Conditions",@"My Account",@"Log Out",nil];
    marr_menuImg = [[NSMutableArray  alloc]  initWithObjects:@"ic_home",@"ic_people",@"ic_photos",@"ic_communities",@"ic_pages",@"ic_whats_hot",@"ic_people",nil];
    
    
    //TBLV SET UP
    [self    setUpUI];
    
    //TBLV INITALIZER
    [self  intializerMethod];
}


#pragma mark -
#pragma mark - setUpUI
#pragma mark -
-(void)setUpUI
{
    self.tableView.frame = CGRectMake(0, 0, SelfViewWidth, SelfViewHeight);
    self.tableView.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    //TBLV HEADER
    self.tblvHeader.frame = CGRectMake(0,0,SelfViewWidth,60);
    self.tblvHeader.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    
    self.imgViewKazumi.frame = CGRectMake(02,10,30,30);
    
    self.lblKazumi.frame = CGRectMake(self.imgViewKazumi.frame.origin.x+self.imgViewKazumi.frame.size.width+02,05, SelfViewWidth - self.imgViewKazumi.frame.origin.x - self.imgViewKazumi.frame.size.width, 40);
    self.lblKazumi.font = [UIFont  fontWithName:kFontSFUIDisplay_Heavy size:17];
}


-(void)intializerMethod
{
    objGlobalValues = [GlobalValues  sharedManager];
    
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return marr_menu.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"strCellIdentifier" forIndexPath:indexPath];
    
    
    //Cell Seperator
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,2)];
    separatorLineView.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    [cell.contentView addSubview:separatorLineView];

    
    
    
    UILabel * lblText = (UILabel*)[cell   viewWithTag:100];
    lblText.frame = CGRectMake(60, 04, 200, 40);
    lblText.font = [UIFont   fontWithName:kFontSFUIDisplay_Regular size:17];
    lblText.text = [marr_menu  objectAtIndex:indexPath.row];
   
    
    
    
    UIImageView * imgViewMenu = (UIImageView*)[cell   viewWithTag:200];
    imgViewMenu.frame = CGRectMake(02, 12, 38, 38);
    UIImage * imgMenu = [UIImage   imageNamed:[marr_menuImg  objectAtIndex:indexPath.row]];
    imgViewMenu.image = imgMenu;
    
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    switch (indexPath.row) {
        case 0:
            [self  performSegueWithIdentifier:ksegueHome sender:nil];
            break;
            
        case 1:
            [self  performSegueWithIdentifier:ksegueCategory sender:nil];
            break;
            
        case 2:
            [self  performSegueWithIdentifier:ksegueAbout sender:nil];
            break;
            
            
        case 3:
            [self  performSegueWithIdentifier:kseguePrivacy sender:nil];
            break;
            
            
        case 4:
            [self  performSegueWithIdentifier:ksegueTerms sender:nil];
            break;
            
        case 5:
            [self  performSegueWithIdentifier:ksegueProfile sender:nil];
            break;
            
        case 6:
            
            [self  logOutFaceBookLogin];
            [self  logOutGoogleLogin];
            [self   dismissViewControllerAnimated:YES completion:nil];
            break;
            
        default:
            break;
    }
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52;
}




-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * objViewHeader = [[UIView alloc] init];
    objViewHeader.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    objViewHeader.frame = CGRectMake(0, 0,SelfViewWidth,70);
    objViewHeader.layer.borderWidth = 0.3;
    objViewHeader.layer.borderColor = [UIColor   whiteColor].CGColor;
    objViewHeader.layer.cornerRadius = 0.3;
    
    
    
    
    //PROFILE IMAGEVIEW
    UIImageView * imgViewProfile = [UIImageView new];
    imgViewProfile.frame = CGRectMake(02,05,40,40);
    imgViewProfile.layer.cornerRadius = imgViewProfile.frame.size.height /2;
    imgViewProfile.image = [UIImage   imageNamed:@"profile_placeholder"];
    imgViewProfile.layer.masksToBounds = YES;
    imgViewProfile.layer.borderWidth = 1.0;
    imgViewProfile.layer.borderColor = [UIColor  whiteColor].CGColor;
    [objViewHeader  addSubview:imgViewProfile];
    
   
    
    //WELCOME
    int X = imgViewProfile.frame.origin.x +imgViewProfile.frame.size.width+02;
    
    UILabel * lblwelcome = [[UILabel    alloc]  initWithFrame:CGRectMake(X,05, SelfViewWidth-X-02, 20)];
    lblwelcome.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:17];
    lblwelcome.text = @"WELCOME";
    lblwelcome.textColor = [UIColor  whiteColor];
    [objViewHeader  addSubview:lblwelcome];
    
    
    //USER NAME
    UILabel * lbluser = [[UILabel    alloc]  initWithFrame:CGRectMake(X,lblwelcome.frame.origin.y+lblwelcome.frame.size.height+02, SelfViewWidth-X-02, 20)];
    lbluser.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:17];
    lbluser.text = [NSString  stringWithFormat:@"%@",objGlobalValues.strFirstName];
    lbluser.textColor = [UIColor  whiteColor];
    [objViewHeader  addSubview:lbluser];

    //CUSTUMER TYPE
    UILabel * lblcust = [[UILabel    alloc]  initWithFrame:CGRectMake(X,lbluser.frame.origin.y+lbluser.frame.size.height, SelfViewWidth-X-02, 20)];
    lblcust.font = [UIFont  fontWithName:kFontSFUIText_LightItalic size:15];
    NSString * strCustType = [NSString   stringWithFormat:@"%@",objGlobalValues.strCustomerGroupId];
    if (strCustType.integerValue == 1) {
     lblcust.text = [NSString  stringWithFormat:@"Retailer"];
        
    }
    else if (strCustType.integerValue == 2){
    lblcust.text = [NSString  stringWithFormat:@"Wholesaler"];
        
    }
    
    lblcust.textColor = [UIColor  whiteColor];
    [objViewHeader  addSubview:lblcust];

    return objViewHeader;
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat floatheight;
    if (section == 0) {
        floatheight = 70;
    }
    return floatheight;
}




-(void)logOutGoogleLogin
{
    //G+ LOG OUT
    GIDSignIn * sign  = [GIDSignIn sharedInstance];
    NSLog(@"userId = %@",sign.currentUser.userID);
    if (sign.currentUser.userID.length == 0) {
        NSLog(@"No need To Log Out No Google Session");
    }
    else
    {
         NSLog(@"Google Log Out Success Fully");
        [[GIDSignIn sharedInstance] signOut];
    }
}


-(void)logOutFaceBookLogin
{
    if ([FBSDKAccessToken currentAccessToken]) {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
        [[FBSDKLoginManager new] logOut];
    }
}





@end
