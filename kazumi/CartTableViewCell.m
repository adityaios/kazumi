//
//  CartTableViewCell.m
//  kazumi
//
//  Created by Yashvir on 10/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "CartTableViewCell.h"

@implementation CartTableViewCell

- (void)awakeFromNib {
    // Initialization code
    //Set min and max
    
    self.btnUpdateQuantity.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor,1.0);
    self.btnUpdateQuantity.layer.borderWidth = 2.0;
    self.btnUpdateQuantity.layer.borderColor = [UIColor  whiteColor].CGColor;
    self.btnUpdateQuantity.layer.cornerRadius = 2.0;
    
    
    self.SteperQuant.minimumValue = 1;
    self.SteperQuant.maximumValue = 1000;
    self.SteperQuant.continuous = NO;
    self.SteperQuant.wraps = NO;
    self.SteperQuant.tintColor = UIColorFromRGBWithAlpha(kazumiRedColor,1.0);

}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
