//
//  ProductDetailOptionViewController.m
//  kazumi
//
//  Created by Yashvir on 05/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "ProductDetailOptionViewController.h"

@interface ProductDetailOptionViewController ()

@end

@implementation ProductDetailOptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self  setUI];
    [self  intializerMethod];
    [self  customBackButton];
    
}


#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    arrOptionValue = [NSArray  new];
    arrOptionValue = [self.dictOptionPass    objectForKey:@"option_value"];
    
    
  //  NSLog(@"DICTIONARY = %@",self.dictOptionPass);
    self.strOption_id = [NSString   stringWithFormat:@"%@",[self.dictOptionPass objectForKey:@"option_id"]];
    self.strOption_name = [NSString   stringWithFormat:@"%@",[self.dictOptionPass objectForKey:@"option_name"]];
    
    NSLog(@"self.strOption_id = %@",self.strOption_id);
    NSLog(@"self.strOption_name = %@",self.strOption_name);
    self.title = [NSString   stringWithFormat:@"Select %@",self.strOption_name ];
}


#pragma mark
#pragma mark setUI
#pragma mark
-(void)setUI
{
    self.tblvProductDetail.frame = CGRectMake(0,64, SelfViewWidth, SelfViewHeight);
    //WEBVIEW
    self.automaticallyAdjustsScrollViewInsets = NO;
}




#pragma mark
#pragma mark  customBackButton
#pragma mark

-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
}

- (void)backButtonClicked:(id)sender {
   [self  performSegueWithIdentifier:kunwindsegue_ProductDetailOption sender:nil];
}



#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrOptionValue.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    
    static NSString *cellIdentifier = @"Cell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:17];
    NSDictionary * dict = [arrOptionValue  objectAtIndex:indexPath.row];
    NSString * stropt_val = [NSString stringWithFormat:@"%@",[dict objectForKey:@"opt_val"]];
    cell.textLabel.text = stropt_val;
    return cell;
    
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString * strSection;
    if (section == 0) {
      strSection = self.strOption_name;
    }
    return strSection;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return UITableViewAutomaticDimension;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    NSUInteger newRow = indexPath.row;
    NSUInteger oldRow = lastIndexPath.row;
    if (newRow != oldRow)
    {
        UITableViewCell *newCell = [tableView cellForRowAtIndexPath:
                                    indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:
                                    lastIndexPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        lastIndexPath = indexPath;
     }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //SELECT OPT VALUE ID
    NSDictionary * dict = [arrOptionValue  objectAtIndex:indexPath.row];
    self.strOpt_Val_ID = [dict  objectForKey:@"opt_val_id"];
}



//opt_id-opt_val_id

/*
 {
 "option_id" = 354;
 "option_name" = Size;
 "option_value" =     (
 {
 "opt_price" = "0.0000";
 "opt_quan" = 25;
 "opt_val" = L;
 "opt_val_id" = 785;
 },
 {
 "opt_price" = "0.0000";
 "opt_quan" = 25;
 "opt_val" = XL;
 "opt_val_id" = 786;
 },
 {
 "opt_price" = "0.0000";
 "opt_quan" = 25;
 "opt_val" = XXL;
 "opt_val_id" = 787;
 },
 {
 "opt_price" = "0.0000";
 "opt_quan" = 25;
 "opt_val" = 3XL;
 "opt_val_id" = 788;
 }
 );
 }
 */

@end
