//
//  WriteReviewViewController.m
//  kazumi
//
//  Created by Yashvir on 05/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "WriteReviewViewController.h"

@interface WriteReviewViewController ()

@end

@implementation WriteReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self  intializerMethod];
    [self  setUI];
    [self  customBackButton];
    
}

#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    self.title = @"Write a Review";
    objGlogalValues = [GlobalValues  sharedManager];
    
    
}

#pragma mark
#pragma mark setUI
#pragma mark
-(void)setUI
{
    //STAR VIEW
    self.starView.frame = CGRectMake(8,70,SelfViewWidth-16,50);
    self.starView.canEdit = YES;
    self.starView.maxRating = 05;
    self.starView.rating = 0;
    
    //LABEL
    self.lblReview.frame = CGRectMake(8,70+self.starView.frame.size.height+05,SelfViewWidth-16,20);
    self.lblReview.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:17];
    //TEXTVIEW
    self.txtViewReview.frame = CGRectMake(8,self.lblReview.frame.origin.y + self.lblReview.frame.size.height+05,SelfViewWidth-16,SelfViewHeight/4);
    self.txtViewReview.font = [UIFont  fontWithName:kFontSFUIText_Regular size:14];
    
    self.txtViewReview.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.txtViewReview.layer.borderWidth = 0.3;
    self.txtViewReview.layer.masksToBounds = true;
    self.txtViewReview.backgroundColor = [UIColor  whiteColor];
    
    
    //BUTTONS
    
    int YCancel = self.txtViewReview.frame.origin.y+self.txtViewReview.frame.size.height + 8;
    
    
    //CANCEL
    self.btnCancel.frame = CGRectMake(8,YCancel,100,45);
    self.btnCancel.backgroundColor = UIColorFromRGBWithAlpha(kazumiYellowColor,1.0);
    self.btnCancel.titleLabel.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:17];
    
    self.btnCancel.layer.borderWidth = 2.0;
    self.btnCancel.layer.borderColor = [UIColor  orangeColor].CGColor;
    self.btnCancel.layer.cornerRadius = 2.0;
    
    
    //SUBMIT
    self.btnSubmit.frame = CGRectMake(SelfViewWidth - 100- 8,YCancel,100,45);
    self.btnSubmit.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor,1.0);
    self.btnSubmit.titleLabel.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:17];
    self.btnSubmit.layer.borderWidth = 2.0;
    self.btnSubmit.layer.borderColor = [UIColor  redColor].CGColor;
    self.btnSubmit.layer.cornerRadius = 2.0;
    
    
    [self  textfieldToolBar:self.txtViewReview];
    
}

#pragma mark
#pragma mark customBackButton
#pragma mark
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
}

- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}





-(void)textfieldToolBar:(UITextView *)txfd
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,SelfViewWidth, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor  redColor];
    
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [numberToolbar sizeToFit];
    txfd.inputAccessoryView = numberToolbar;
}


-(void)cancelNumberPad{
    [self.view  endEditing:YES];
    
}


#pragma mark
#pragma mark IBACTION
#pragma mark


- (IBAction)btnCancelClicked:(UIButton *)sender {
    [self.navigationController  popViewControllerAnimated:YES];
}

- (IBAction)btnSubmitClicked:(UIButton *)sender {
    /*
     addreview
     customer_id
     product_id
     author
     review_text
     rating
     */
    
    NSString * strCustId = objGlogalValues.strCustomerId;
    NSString * strProdId = self.objProductModelWriteReview.strProduct_id;
    NSString * strAuthName = objGlogalValues.strFirstName;
    NSString * strRating = [NSString   stringWithFormat:@"%.01f",self.starView.rating];
    
    //REVIEW TEXT
    NSString * strReviewText = self.txtViewReview.text;
    strReviewText = [strReviewText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //remove double Space
    while ([strReviewText rangeOfString:@"  "].location != NSNotFound) {
        strReviewText = [strReviewText stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    if (strReviewText.length <= 4) {
        [TSMessage  showNotificationInViewController:self title:@"Review Error" subtitle:@"Please enter valid Review Text"  type:TSMessageNotificationTypeError];
        return;
    }
    
    
    
    if ([Reachability isConnected]) {
        [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
        NSDictionary *dictPara = @{@"action":@"addreview",@"customer_id":strCustId,@"product_id":strProdId,@"author":strAuthName,@"review_text":strReviewText,@"rating":strRating};
        // NSLog(@"dictPara = %@",dictPara);
        
        
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
            //REMOVE LOADER
            [DejalBezelActivityView  removeViewAnimated:YES];
            
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                //CHECK STATUS CODE
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    NSLog(@" %@",result);
                    //SUCESS MESSAGE
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Product Review" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
                    [self.navigationController  popViewControllerAnimated:YES];
                }
                else
                {
                    //ERROR MESSAGE
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Product Review" subtitle:strmsg  type:TSMessageNotificationTypeError];
                    [self.navigationController  popViewControllerAnimated:YES];
                    
                }
            }
        }];
    }
}
@end
