//
//  MyOrderViewController.h
//  kazumi
//
//  Created by NM8 on 01/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "Network.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "GlobalValues.h"
#import "Network.h"
#import "MyOrders.h"
#import "Constant.h"
#import "KazumiMacros.h"
#import "MyOrderTableViewCell.h"
#import "MyOrderDetailsViewController.h"

@interface MyOrderViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * marrOrderModelData;
    GlobalValues * objglobalValues;
    NSInteger  intPagination;
}

@property (weak, nonatomic) IBOutlet UITableView *tblvOrders;

//Methods
-(void)setUpUI;
-(void)intializerMethod;
-(void)customBackButton;
-(void)webserviceCallForMyOrders:(NSString*)strPagePara;
@end
