//
//  CartViewController.h
//  kazumi
//
//  Created by Yashvir on 10/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "DejalActivityView.h"
#import "GlobalValues.h"
#import "Network.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "TSMessage.h"
#import "KazumiMacros.h"
#import "CartTableViewCell.h"
#import "CartModel.h"
#import "ShippingDetailsViewController.h"
#import "UIImageView+AFNetworking.h"

@interface CartViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>{
    GlobalValues * objGlobalValue;
    NSMutableArray * marrDataCartModel;
    NSString * strCouponCodeApplied;
}




@property (weak, nonatomic) IBOutlet UITableView *tblvCart;

@property (weak, nonatomic) IBOutlet UIButton *btnHaveACoupon;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckOut;



@property (weak, nonatomic) IBOutlet UILabel *lbltotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscount;

@property (weak, nonatomic) IBOutlet UILabel *lblPayableAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblCouponCode;
@property (strong, nonatomic) IBOutlet UILabel *lblCouponValue;






- (IBAction)btnCheckOutClicked:(UIButton *)sender;
- (IBAction)btnHaveACouponClicked:(UIButton *)sender;






-(void)setUpUI;
-(void)intializerMethod;
-(void)customBackButton;
-(void)webserviceCallForCartList;
-(void)webserviceCallForHaveACoupon:(NSString *)strCoupon;
-(void)webserviceCallForDeleteProductFromCart:(CartModel*)objcart;
-(void)webserviceCallForUpdateProductQuantityFromCart:(CartModel*)objcart;
-(void)jsonParsingForCoupon:(NSDictionary*)dictResult;
-(void)webserviceCallForremovecoupon:(NSString*)CouponCode;

@end
