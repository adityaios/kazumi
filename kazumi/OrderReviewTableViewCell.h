//
//  OrderReviewTableViewCell.h
//  kazumi
//
//  Created by Yashvir on 19/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalValues.h"
#import "KazumiMacros.h"
#import "Constant.h"


@interface OrderReviewTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgViewProduct;
@property (strong, nonatomic) IBOutlet UILabel *lblProductName;
@property (strong, nonatomic) IBOutlet UILabel * lblOptions;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblQty;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIStepper *SteperQuant;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdateQuantity;


@end
