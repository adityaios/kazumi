//
//  ForgotPassValidationViewController.h
//  kazumi
//
//  Created by NM8 on 27/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "DejalActivityView.h"
#import "Network.h"
#import "GlobalValues.h"
#import "TSMessage.h"
#import "KazumiMacros.h"
#import "Constant.h"

@interface ForgotPassValidationViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txfdConfirmPswd;
@property (weak, nonatomic) IBOutlet UITextField *txfdNewPswd;
@property (weak, nonatomic) IBOutlet UITextField *txfdActivationCode;

@property (weak, nonatomic) IBOutlet UIButton *btnAdd;









- (IBAction)btnSubmit:(id)sender;


//Methods
-(void)viewSetUP;
-(void)IntializerMethod;
-(void)customBackButton;
-(void)webserviceCallPasswordValidation;


@end



