//
//  MyOrderDetailsViewController.m
//  kazumi
//
//  Created by Yashvir on 02/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "MyOrderDetailsViewController.h"

@interface MyOrderDetailsViewController ()

@end

@implementation MyOrderDetailsViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [self  intializerMethod];
    [self   setUI];
    [self  customBackButton];
    [self  webserviceCallForMyOrders];
}



#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    self.title =@"My Order  Details";
    objglobalValues=[GlobalValues sharedManager];
    
}


#pragma mark
#pragma mark setUpUI
#pragma mark
-(void)setUI
{
    
    self.tableView.frame = CGRectMake(0, 0, SelfViewWidth, SelfViewHeight);
    //HEADER
    self.viewHeader.frame = CGRectMake(0, 0, SelfViewWidth,100);
    
    //ORDER
    self.lblOrderTitle.frame = CGRectMake(8,10,75,21);
    self.lblOrderText.frame = CGRectMake(8,40,100,21);
    
    //ITEM
    self.lblItemTitle.frame = CGRectMake(SelfViewWidth/2 - 25,10,50,21);
    self.lblItemText.frame = CGRectMake(SelfViewWidth/2 - 25,40,50,21);
    
    //TOTAL
    self.lblTotalTitle.frame = CGRectMake(SelfViewWidth - 108,10,100,21);
    self.lblTotalText.frame = CGRectMake(SelfViewWidth - 108,40,100,21);
    
    //PRODUCT
    self.lblProductDetails.frame = CGRectMake(8,self.lblOrderText.frame.origin.y + self.lblOrderText.frame.size.height+8,SelfViewWidth - 16,21);
    
    
    //FOOTER
    
    //FOOTER
    self.viewFooter.frame = CGRectMake(0, 0, SelfViewWidth,500);
    
    
    
    //SHIPPING ADDRESS
    
    self.viewShippingAddress.frame = CGRectMake(8,8,SelfViewWidth-16,130);
    self.viewShippingAddress.layer.borderColor = UIColor.grayColor.CGColor;
    self.viewShippingAddress.layer.borderWidth = 0.3;
    self.viewShippingAddress.layer.masksToBounds = true;
    self.viewShippingAddress.backgroundColor = [UIColor  whiteColor];
    
    
    
    
    
    self.lblShippingAddressHead.frame = CGRectMake(8,10,self.viewShippingAddress.frame.size.width -16,21);
    self.lblShippingAdd1.frame = CGRectMake(8,31,self.viewShippingAddress.frame.size.width - 16,21);
    self.lblShippingAdd2.frame = CGRectMake(8,52,self.viewShippingAddress.frame.size.width -16,21);
    self.lblShippingAdd3.frame = CGRectMake(8,73,self.viewShippingAddress.frame.size.width -16,21);
    self.lblShippingAdd4.frame = CGRectMake(8,94,self.viewShippingAddress.frame.size.width -16,21);
    
    
    //PERMANENT ADDRESS
    self.viewPermanentAddress.frame = CGRectMake(8,self.viewShippingAddress.frame.origin.y+self.viewShippingAddress.frame.size.height+16,SelfViewWidth-16,130);
    self.viewPermanentAddress.layer.borderColor = UIColor.grayColor.CGColor;
    self.viewPermanentAddress.layer.borderWidth = 0.3;
    self.viewPermanentAddress.layer.masksToBounds = true;
    self.viewPermanentAddress.backgroundColor = [UIColor  whiteColor];
    
    
    
    
    self.lblPermanentAddressHead.frame = CGRectMake(8,10,self.viewPermanentAddress.frame.size.width -16,21);
    
    self.lblPermanentAdd1.frame = CGRectMake(8,31,self.viewPermanentAddress.frame.size.width - 16,21);
    self.lblPermanentAdd2.frame = CGRectMake(8,52,self.viewPermanentAddress.frame.size.width -16,21);
    self.lblPermanentAdd3.frame = CGRectMake(8,73,self.viewPermanentAddress.frame.size.width -16,21);
    self.lblPermanentAdd4.frame = CGRectMake(8,94,self.viewPermanentAddress.frame.size.width -16,21);
    
    
    //SHIPPING METHOD
    self.viewShippingMethod.frame = CGRectMake(8,self.viewPermanentAddress.frame.origin.y+self.viewPermanentAddress.frame.size.height+16,SelfViewWidth-16,80);
    self.viewShippingMethod.layer.borderColor = UIColor.grayColor.CGColor;
    self.viewShippingMethod.layer.borderWidth = 0.3;
    self.viewShippingMethod.layer.masksToBounds = true;
    self.viewShippingMethod.backgroundColor = [UIColor  whiteColor];
    
    
    
    self.lblShippingMethodHead.frame = CGRectMake(8,10,self.viewPermanentAddress.frame.size.width -16,21);
    self.lblShippingMethodtitle.frame = CGRectMake(8,31,self.viewPermanentAddress.frame.size.width - 16,21);
    self.lblShippingMethodValue.frame = CGRectMake(8,52,self.viewPermanentAddress.frame.size.width -16,21);
    
    
    //PAYMENT METHOD
    self.viewPaymentMethod.frame = CGRectMake(8,self.viewShippingMethod.frame.origin.y+self.viewShippingMethod.frame.size.height+16,SelfViewWidth-16,80);
    
    self.viewPaymentMethod.layer.borderColor = UIColor.grayColor.CGColor;
    self.viewPaymentMethod.layer.borderWidth = 0.3;
    self.viewPaymentMethod.layer.masksToBounds = true;
    self.viewPaymentMethod.backgroundColor = [UIColor  whiteColor];
    
    
    
    self.lblPaymentMethodHead.frame = CGRectMake(8,10,self.viewPaymentMethod.frame.size.width -16,21);
    self.lblPaymentMethodtitle.frame = CGRectMake(8,31,self.viewPaymentMethod.frame.size.width - 16,21);
    self.lblPaymentMethodValue.frame = CGRectMake(8,52,self.viewPaymentMethod.frame.size.width -16,21);
    
}



#pragma mark
#pragma mark webserviceCallForMyOrders
#pragma mark
-(void)webserviceCallForMyOrders
{
    //LOADER
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    
    //PARA DICT
    NSDictionary * dictParam = @{@"action":@"getorderdetails",@"customer_id":objglobalValues.strCustomerId,@"order_id":self.objOrderPass.strOrderID};
    
    
    //WEB CALL
    [Network   getWebserviceWithBaseUrl:kBaseURL withParameters:dictParam andCompletionHandler:^(id result, NSError *error) {
        
        //REMOVE LOADER
        [DejalBezelActivityView  removeViewAnimated:YES];
        
        //ERROR HANDLING
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            NSLog(@"result = %@",result);
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                
                //SUCESS MESSAGE
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"My Orders" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
                
                //JSON PARSING
                dictOrderDetails = [NSDictionary  new];
                dictOrderDetails = [result  objectForKey:@"order_details"];
                
                //ORDER ON
                self.lblOrderText.text = [dictOrderDetails  objectForKey:@"order_date"];
                //ITEM
                self.lblItemText.text = [NSString  stringWithFormat:@"(%lu)",(unsigned long)arrProductsList.count+1];
                //TOTAL
                NSString * strTotalPrice = [NSString  stringWithFormat:@"Rs %.02f",[[dictOrderDetails  objectForKey:@"total"] floatValue]];
                self.lblTotalText.text = strTotalPrice;
                
                
                self.lblPaymentMethodtitle.text = [dictOrderDetails  objectForKey:@"payment_method"];
                
                self.lblPaymentMethodValue.text = [NSString  stringWithFormat:@"Rs %.02f",[[dictOrderDetails  objectForKey:@"total"] floatValue]];
                
                
                //SHIPPING ADDRESS
                dictShippingAddress = [NSDictionary  new];
                dictShippingAddress = [result  objectForKey:@"shipping_address"];
                self.lblShippingAdd1.text = [NSString  stringWithFormat:@"%@ %@",[dictShippingAddress objectForKey:@"shipping_firstname"],[dictShippingAddress objectForKey:@"shipping_lastname"]];
                self.lblShippingAdd2.text = [NSString  stringWithFormat:@"%@,%@",[dictShippingAddress objectForKey:@"shipping_address_1"],[dictShippingAddress objectForKey:@"shipping_address_2"]];
                self.lblShippingAdd3.text = [NSString  stringWithFormat:@"%@,%@",[dictShippingAddress objectForKey:@"shipping_city"],[dictShippingAddress objectForKey:@"shipping_postcode"]];
                self.lblShippingAdd4.text = [NSString  stringWithFormat:@"%@,%@",[dictShippingAddress objectForKey:@"shipping_zone"],[dictShippingAddress objectForKey:@"shipping_country"]];
                
                
                
                //PAYMENT ADDRESS
                dictPaymentAddress = [NSDictionary  new];
                dictPaymentAddress = [result  objectForKey:@"payment_address"];
                
                self.lblPermanentAdd1.text = [NSString  stringWithFormat:@"%@ %@",[dictPaymentAddress objectForKey:@"payment_firstname"],[dictPaymentAddress objectForKey:@"payment_lastname"]];
                self.lblPermanentAdd2.text = [NSString  stringWithFormat:@"%@,%@",[dictPaymentAddress objectForKey:@"payment_address_1"],[dictPaymentAddress objectForKey:@"payment_address_2"]];
                self.lblPermanentAdd3.text = [NSString  stringWithFormat:@"%@,%@",[dictPaymentAddress objectForKey:@"payment_city"],[dictPaymentAddress objectForKey:@"payment_postcode"]];
                self.lblPermanentAdd4.text = [NSString  stringWithFormat:@"%@,%@",[dictPaymentAddress objectForKey:@"payment_zone"],[dictPaymentAddress objectForKey:@"payment_country"]];
                
                
                
                
                //SHIPPING
                dictShipping = [NSDictionary  new];
                dictShipping = [result  objectForKey:@"shipping"];
                self.lblShippingMethodtitle.text = [dictShipping  objectForKey:@"title"];
                self.lblShippingMethodValue.text = [dictShipping  objectForKey:@"value"];
                
                
                //PRODUCTS LIST
                arrProductsList = [NSArray  new];
                arrProductsList = [result  objectForKey:@"product"];
                
                
                
                
                //SUBTOTAL
                dictSubTotal = [NSDictionary  new];
                dictSubTotal = [result  objectForKey:@"subtotal"];
                
                
                [self.tableView  reloadData];
                
            }
            else
            {
                
                //ERROR MESSAGE
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"My Orders" subtitle:strmsg  type:TSMessageNotificationTypeError];
            }
        }
    }];
}



#pragma mark
#pragma mark customBackButton
#pragma mark
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}

- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}





#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrProductsList?arrProductsList.count:0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}




- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    
    static NSString *cellIdentifier = @"Cell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text =  [NSString  stringWithFormat:@"%ld - %ld",(long)indexPath.section,(long)indexPath.row];
    
    
    
    return cell;
}





-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView * viewProductImage;
    if (arrProductsList.count !=0) {
        NSDictionary * dictProduct = [arrProductsList   objectAtIndex:section];
        NSString * strImageUrl = [NSString  stringWithFormat:@"%@",[dictProduct  objectForKey:@"product_image"]];
        NSString * strProductName = [NSString  stringWithFormat:@"%@",[dictProduct  objectForKey:@"product_name"]];
        NSString * strModelName = [NSString  stringWithFormat:@"%@",[dictProduct  objectForKey:@"model"]];
        NSString * strQuantity = [NSString  stringWithFormat:@"%@",[dictProduct  objectForKey:@"quantity"]];
        
        
        NSString * strPrice = [NSString  stringWithFormat:@"Rs %.02f",[[dictProduct  objectForKey:@"price"] floatValue]];
        NSString * strTax = [NSString  stringWithFormat:@"Rs %.02f",[[dictProduct  objectForKey:@"tax"]   floatValue]];
        NSString * strTotal = [NSString  stringWithFormat:@"Rs %.02f",[[dictProduct  objectForKey:@"total"] floatValue]];
        
        
        
        
        
        UIView * viewProductImage = [[UIView alloc] initWithFrame:CGRectMake(0, 0,SelfViewWidth,210)];
        viewProductImage.backgroundColor = [UIColor  whiteColor];
        
        
        
        
        
        
        
        //IMAGEVIEW
        UIImageView * imgViewProduct = [[UIImageView   alloc] initWithFrame:CGRectMake(8,8,90,90)];
        [imgViewProduct setImageWithURL:[NSURL  URLWithString:strImageUrl] placeholderImage:[UIImage  imageNamed:@"placeholder"]];
        [viewProductImage  addSubview:imgViewProduct];
        
        
        //PRODUCT NAME
        UILabel * lblProductName = [[UILabel  alloc]  initWithFrame:CGRectMake(imgViewProduct.frame.origin.x + imgViewProduct.frame.size.width +8,10,SelfViewWidth - imgViewProduct.frame.origin.x - imgViewProduct.frame.size.width - 16,18)];
        lblProductName.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:14];
        strProductName = [strProductName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        lblProductName.text = strProductName;
        [viewProductImage  addSubview:lblProductName];
        
        
        int Ypadding = lblProductName.frame.origin.y + lblProductName.frame.size.height+05;
        
        //MODEL
        UILabel * lblModelName = [[UILabel  alloc]  initWithFrame:CGRectMake(imgViewProduct.frame.origin.x + imgViewProduct.frame.size.width +8,Ypadding,SelfViewWidth - imgViewProduct.frame.origin.x - imgViewProduct.frame.size.width - 16, 18)];
        lblModelName.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
        lblModelName.text = [NSString   stringWithFormat:@"Model - %@",strModelName];
        [viewProductImage  addSubview:lblModelName];
        
        
        
        Ypadding = Ypadding + lblModelName.frame.size.height+05;
        
        
        //QUANTITY
        UILabel * lblquantity = [[UILabel  alloc]  initWithFrame:CGRectMake(lblModelName.frame.origin.x,Ypadding,SelfViewWidth - imgViewProduct.frame.origin.x - imgViewProduct.frame.size.width - 16, 18)];
        lblquantity.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
        lblquantity.text = [NSString   stringWithFormat:@"Quantity - %@",strQuantity];
        [viewProductImage  addSubview:lblquantity];
        
        
        
        Ypadding = Ypadding + lblquantity.frame.size.height+05;
        
        
        
        //PRICE
        UILabel * lblPrice = [[UILabel  alloc]  initWithFrame:CGRectMake(lblquantity.frame.origin.x,Ypadding,SelfViewWidth - imgViewProduct.frame.origin.x - imgViewProduct.frame.size.width - 16, 18)];
        lblPrice.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
        lblPrice.text = [NSString   stringWithFormat:@"Price - %@",strPrice];
        [viewProductImage  addSubview:lblPrice];
        
        
        
        Ypadding = Ypadding + lblPrice.frame.size.height+05;
        
        
        //TAX
        UILabel * lblTax = [[UILabel  alloc]  initWithFrame:CGRectMake(lblPrice.frame.origin.x,Ypadding,SelfViewWidth - imgViewProduct.frame.origin.x - imgViewProduct.frame.size.width - 16, 18)];
        lblTax.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
        lblTax.text = [NSString   stringWithFormat:@"Tax - %@",strTax];
        [viewProductImage  addSubview:lblTax];
        
        
        
        Ypadding = Ypadding + lblTax.frame.size.height+05;
        
        
        //TOTAL
        UILabel * lblTotal = [[UILabel  alloc]  initWithFrame:CGRectMake(lblTax.frame.origin.x,Ypadding,SelfViewWidth - imgViewProduct.frame.origin.x - imgViewProduct.frame.size.width - 16, 18)];
        lblTotal.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
        lblTotal.text = [NSString   stringWithFormat:@"Total - %@",strTotal];
        [viewProductImage  addSubview:lblTotal];
        
        
        Ypadding = Ypadding + lblTotal.frame.size.height+05;
        
        
        //OPTIONS
        UILabel * lblOptions = [[UILabel  alloc]  initWithFrame:CGRectMake(8,Ypadding,SelfViewWidth - 16, 18)];
        lblOptions.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:14];
        lblOptions.text = [NSString   stringWithFormat:@"Options"];
        [viewProductImage  addSubview:lblOptions];
        
        
        
        
        Ypadding = Ypadding + lblOptions.frame.size.height+02;
        
        NSMutableString * strOptions = [[NSMutableString alloc]init];
        for (NSDictionary * dict in [dictProduct objectForKey:@"options"]) {
            [strOptions appendString:[dict objectForKey:@"opt_name"]];
            [strOptions appendString:@"\t"];
            [strOptions appendString:[dict objectForKey:@"opt_value"]];
            [strOptions appendString:@"\t"];
            NSString * strOptPrice = [NSString  stringWithFormat:@"Rs %.02f",[[dictProduct  objectForKey:@"opt_price"] floatValue]];
            [strOptions appendString:strOptPrice];
            [strOptions appendString:@"\n"];
        }
        
        
        //OPTIONS TEXTVIEW
        UITextView* txtOptions = [[UITextView  alloc]  initWithFrame:CGRectMake(8,Ypadding,SelfViewWidth - 16, 18*2)];
        txtOptions.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
        txtOptions.text = strOptions;
        [viewProductImage  addSubview:txtOptions];
        txtOptions.editable = NO;
        [txtOptions setContentInset:UIEdgeInsetsMake(-5, 0, 5,0)];
        txtOptions.layer.borderColor = UIColor.grayColor.CGColor;
        txtOptions.layer.borderWidth = 0.3;
        txtOptions.layer.masksToBounds = true;
        
        
        
        
        return viewProductImage;
    }
    
    return viewProductImage;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 210;
}









@end
