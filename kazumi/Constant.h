//
//  Constant.h
//  kazumi
//
//  Created by Yashvir on 14/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#ifndef Constant_h
#define Constant_h


//#BA302E RED
//0xffdf01

//COLOR HEXCODE
#define kazumiRedColor  0xBA302E
#define kazumiRed1Color 0x800000

#define kazumiYellowColor 0xF29212
#define kazumiYellow1Color 0xffdf01

#define kazumiSecondRedColor  0x800000
#define kazumiSecondYellowColor  0xffdf01





#define kazumiRedColorAlpha  0.5


//BUTTON CORNER RADIUS
#define kButtonCornerRadius 4


//FONTS
#define kfontDefault @"HelveticaNeue"
#define kfontDefaultBold @"HelveticaNeue-Bold"
#define kfontDefaultItalics @"HelveticaNeue-Italic"


#define kfontFutura_Medium @"Futura-Medium"
#define Futura_CondensedExtraBold @"Futura-CondensedExtraBold"
#define Futura_MediumItalic @"Futura-MediumItalic"


//APPLE FONTS
#define kFontSFUIText_LightItalic @"SFUIText-LightItalic"
#define kFontSFUIText_HeavyItalic @"SFUIText-HeavyItalic"
#define kFontSFUIText_Bold @"SFUIText-Bold"
#define kFontSFUIText_Regular @"SFUIText-Regular"


/*
SFUIText-Italic
SFUIText-Light
SFUIText-MediumItalic
SFUIText-Semibold
SFUIText-BoldItalic
SFUIText-SemiboldItalic
SFUIText-Medium
SFUIText-Heavy
*/


#define kFontSFUIDisplay_Light @"SFUIDisplay-Light"
#define kFontSFUIDisplay_Heavy @"SFUIDisplay-Heavy"
#define kFontSFUIDisplay_Regular @"SFUIDisplay-Regular"
#define kFontSFUIDisplay_Bold @"SFUIDisplay-Bold"

/*
SFUIDisplay-Medium
SFUIDisplay-Bold
SFUIDisplay-Black
SFUIDisplay-Ultralight
SFUIDisplay-Thin
SFUIDisplay-Semibold
*/










//SEGUES
#define ksegueHome  @"segue_Home"
#define ksegueCategory  @"segue_Category"
#define ksegueSignIn  @"segue_SignIn"
#define ksegueLoginSuccess @"segue_Login"
#define ksegueAbout  @"segue_about"
#define kseguePrivacy @"segue_privacy"
#define ksegueTerms @"segue_terms"
#define ksegueProfile @"segue_profile"
#define ksegueProductHome @"segue_ProductHome"
#define ksegueEditProfile @"segueEditProfile"
#define ksegueEditUserProfieInput @"segue_editingUserProfile"
#define kunwindSegue_EditProfile @"unwindsegue_editProfile"
#define ksegue_EditLocation  @"segue_editLocation"
#define kunwindsegue_EditLocationProfile  @"unwindsegue_EditLocationProfile"
#define ksegueEditProfileZone  @"segue_EditProfileZone"
#define kunwindsegue_EditProfileZone  @"unwindsegue_EditProfileZone"
#define ksegue_OrderDetails  @"segue_OrderDetails"
#define ksegue_ProducDetailOptions @"segue_producDetailOptions"
#define ksegue_writeReview @"segue_writeReview"
#define ksegue_ReviewList  @"segue_ReviewList"
#define kunwindsegue_ProductDetailOption  @"unwindsegue_ProductDetailOption"
#define ksegue_wishlist @"segue_wishlist"
#define ksegue_registrationActivationVC @"segue_registrationActivationVC"
#define ksegue_socialLogin @"segue_socialLogin"
#define ksegue_segue_OrderReview  @"segue_OrderReview"
#define ksegue_segue_FilterOptions  @"segue_FilterOptions"



//GOOGLE CREDENTIALS
#define kGoogleClientId @"417462267639-431uhe3h82g9ce37o435m7uucv6tc79l.apps.googleusercontent.com"

#define KFaceBookScheme @"fb425176917693351"





//ALERTVIEW
#define kAlertNoNetworkTitle @"No Network"
#define kAlertNoNetworkMsg @"No Network Available"



//STRINGS
#define kNoDiscountTypeApplied  @"No discount type applied"
#define kDiscountTypeAppliedRs  @"Rs."




//SOCIAL DICT KEY
#define kSocialType @"SocialType"
#define kSocialTypeGoggle @"SocialTypeGoggle"
#define kSocialTypeFaceBook @"SocialTypeFaceBook"


















#endif /* Constant_h */
