//
//  CartViewController.m
//  kazumi
//
//  Created by Yashvir on 10/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//





#import "CartViewController.h"

@interface CartViewController ()

@end

@implementation CartViewController
#define kRemoveCoupon @"Remove Coupon"
#define kHaveCoupon @"Have A Coupon?"



#define kDiscount_Download @"discount_on_download"
#define kCoupon_Code @"coupon_code"
#define kCoupon_Value @"coupon_value"
#define kDiscount_On_Refer @"discount_on_refer"
#define kPayableAmount @"grand_total"
#define kTotal @"total"



#pragma mark
#pragma mark VC LIFE CYCLE
#pragma mark

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  setUpUI];
    [self  intializerMethod];
    [self  customBackButton];
    
    if ([Reachability isConnected]) {
        [self  webserviceCallForCartList];
    }
}



#pragma mark
#pragma mark INTIALIZER
#pragma mark
-(void)intializerMethod
{
    self.title = @"Cart";
    objGlobalValue = [GlobalValues   sharedManager];
    
    UINib *nib = [UINib nibWithNibName:@"CartTableViewCell" bundle:nil];
    [self.tblvCart registerNib:nib forCellReuseIdentifier:@"CartCell"];
    
    self.tblvCart.estimatedRowHeight = 130.0;
    self.tblvCart.rowHeight = UITableViewAutomaticDimension;
    self.tblvCart.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.view.backgroundColor =  UIColorFromRGBWithAlpha(kazumiRed1Color,1.0);
    
}






#pragma mark
#pragma mark setUpUI
#pragma mark
-(void)setUpUI
{
    self.tblvCart.frame = CGRectMake(0,0,SelfViewWidth, SelfViewHeight - 100);
    // self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    //Btns Have a Coupon
    self.btnHaveACoupon.frame = CGRectMake(SelfViewWidth-125,self.tblvCart.frame.origin.y+self.tblvCart.frame.size.height+02,120,35);
    //self.btnHaveACoupon.backgroundColor = UIColorFromRGBWithAlpha(kazumiYellowColor,1.0);
    //self.btnHaveACoupon.layer.borderWidth =0.2;
    // self.btnHaveACoupon.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnHaveACoupon.layer.cornerRadius =  2.0;
    self.btnHaveACoupon.titleLabel.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:16];
    
    //Btn CHECKOUT
    self.btnCheckOut.frame = CGRectMake(SelfViewWidth-125,self.btnHaveACoupon.frame.origin.y+self.btnHaveACoupon.frame.size.height+02,120,35);
    //self.btnCheckOut.backgroundColor = UIColorFromRGBWithAlpha(kazumiYellowColor,1.0);
    //self.btnCheckOut.layer.borderWidth = 0.2;
    //self.btnCheckOut.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnCheckOut.layer.cornerRadius =  2.0;
    self.btnCheckOut.titleLabel.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:16];
    
    
    //Total Price
    self.lbltotalPrice.frame = CGRectMake(02,self.btnHaveACoupon.frame.origin.y,SelfViewWidth - self.btnHaveACoupon.frame.size.width-8,14);
    self.lbltotalPrice.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
    
    //Dicount Price
    self.lblDiscount.frame = CGRectMake(02,self.lbltotalPrice.frame.origin.y+self.lbltotalPrice.frame.size.height+04,SelfViewWidth - self.btnHaveACoupon.frame.size.width-8,14);
    self.lblDiscount.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
    
    //Coupon Code
    self.lblCouponCode.frame = CGRectMake(02,self.lblDiscount.frame.origin.y+self.lblDiscount.frame.size.height+04,SelfViewWidth - self.btnHaveACoupon.frame.size.width-8,14);
    self.lblCouponCode.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
    
    //Coupon Value
    self.lblCouponValue.frame = CGRectMake(02,self.lblCouponCode.frame.origin.y+self.lblCouponCode.frame.size.height+04,SelfViewWidth - self.btnHaveACoupon.frame.size.width-8,14);
    self.lblCouponValue.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
    
    //Payable Amount
    self.lblPayableAmount.frame = CGRectMake(02,self.lblCouponValue.frame.origin.y+self.lblCouponValue.frame.size.height+04,SelfViewWidth - self.btnHaveACoupon.frame.size.width-8,14);
    self.lblPayableAmount.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
    
}


#pragma mark custom Back Button
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark
#pragma mark IBACTIONS
#pragma mark

#pragma mark btnDeleteClicked
-(void)btnDeleteClicked:(UIButton*)sender{
    UIButton *button = (UIButton *)sender;
    CGPoint buttonOriginInTableView = [button convertPoint:CGPointZero toView:self.tblvCart];
    NSIndexPath *indexPath = [self.tblvCart indexPathForRowAtPoint:buttonOriginInTableView];
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    CartModel * objCart = [marrDataCartModel  objectAtIndex:indexPath.row];
    
    
    
    //ALERT FOR DELETE
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Cart Item Delete"
                                  message:@"Remove item from Cart?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             [self  webserviceCallForDeleteProductFromCart:objCart];
                             
                             
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}



#pragma mark btnUpdateQuantityClicked
-(void)btnUpdateQuantityClicked:(UIButton*)sender{
    UIButton *button = (UIButton *)sender;
    CGPoint buttonOriginInTableView = [button convertPoint:CGPointZero toView:self.tblvCart];
    NSIndexPath *indexPath = [self.tblvCart indexPathForRowAtPoint:buttonOriginInTableView];
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    CartModel * objCart = [marrDataCartModel  objectAtIndex:indexPath.row];
    [self  webserviceCallForUpdateProductQuantityFromCart:objCart];
    
    
}



#pragma mark stepperChanged
- (void)stepperChanged:(UIStepper*)sender {
    UIStepper * stepper = (UIStepper *)sender;
    CGPoint buttonOriginInTableView = [stepper convertPoint:CGPointZero toView:self.tblvCart];
    
    NSIndexPath *indexPath = [self.tblvCart indexPathForRowAtPoint:buttonOriginInTableView];
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    CartTableViewCell * tcell = [self.tblvCart cellForRowAtIndexPath:indexPath];
    NSLog(@"stepper value = %ld",(long)tcell.SteperQuant.value);
    
    
    //WHOLESALER Check
    if (objGlobalValue.strCustomerGroupId.integerValue == 2 && tcell.SteperQuant.value < 20) {
        tcell.SteperQuant.value = 20;
        [TSMessage  showNotificationInViewController:self title:@"For WholeSaler Customer Quantity cannot be lesser then 20" subtitle:nil  type:TSMessageNotificationTypeMessage];
        return;
        
    }
    
    //Insert Value In Cart Models Objects
    CartModel * objcart = [CartModel  new];
    objcart  =    [marrDataCartModel objectAtIndex:indexPath.row];
    objcart.strProductQuant = [NSString  stringWithFormat:@"%f",tcell.SteperQuant.value];
    
    NSString * strQuant = [NSString  stringWithFormat:@"Qty : %.f",tcell.SteperQuant.value];
    tcell.lblQty.text = strQuant;
    
    //    [TSMessage  showNotificationInViewController:self title:@"Update Quatity"  subtitle:@"Please press Update Qty button,unless Updated Quantity displayed will not be saved"  type:TSMessageNotificationTypeMessage];
    [TSMessage  showNotificationInViewController:self title:@"Update Quatity" subtitle:@"Please press Update Qty button,unless Updated Quantity displayed will not be saved" type:TSMessageNotificationTypeMessage duration:TSMessageNotificationDurationEndless];
    
}



#pragma mark btnCheckOutClicked
- (IBAction)btnCheckOutClicked:(UIButton *)sender {
    ShippingDetailsViewController * objShipping = [self.storyboard instantiateViewControllerWithIdentifier:@"ShippingDetailsViewController"];
    [self.navigationController pushViewController:objShipping animated:YES];
}


#pragma mark btnHaveACouponClicked
- (IBAction)btnHaveACouponClicked:(UIButton *)sender {
    if ([sender.titleLabel.text  isEqualToString:kRemoveCoupon]) {
        if ([Reachability isConnected]) {
            if (strCouponCodeApplied.length >2) {
                [self webserviceCallForremovecoupon:strCouponCodeApplied];
            }
            else
            {
                [TSMessage  showNotificationInViewController:self title:@"No Coupon Code Applied" subtitle:nil  type:TSMessageNotificationTypeError];
                return;
                
            }
        }
    }
    else
    {
        //ALERT FOR DELETE
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Have a Coupon?" message:@"Please enter Coupon code" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        alert.tag = 100;
        
        [alert addButtonWithTitle:@"Apply"];
        [alert show];
    }
}



#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark


#pragma mark numberOfSectionsInTableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



#pragma mark numberOfRowsInSection
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return marrDataCartModel?marrDataCartModel.count:0;
}



#pragma mark cellForRowAtIndexPath
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CartTableViewCell * tcell =  [tableView dequeueReusableCellWithIdentifier:@"CartCell"];
    
    //CELL BORDER
    tcell.contentView.layer.borderColor =  [UIColor   lightGrayColor].CGColor;
    tcell.contentView.layer.borderWidth = 1.0;
    
    
    
    //Display Data
    
    CartModel * objcart = [CartModel  new];
    objcart  =    [marrDataCartModel objectAtIndex:indexPath.row];
    
    //Img
    [tcell.imgViewProduct setImageWithURL:[NSURL  URLWithString:objcart.strProductImgUrl] placeholderImage:[UIImage  imageNamed:@"placeholder"]];
    
    //Name
    objcart.strProductName = [objcart.strProductName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    tcell.lblProductName.text = objcart.strProductName;
    //Price
    objcart.strProductPrice = [objcart.strProductPrice  stringByReplacingOccurrencesOfString:@"," withString:@""];
    NSString * strPrice = [NSString  stringWithFormat:@"Rs. %.02f",[objcart.strProductPrice floatValue]];
    
    
    tcell.lblTotalAmount.text = strPrice;
    //Quant
    NSString * strQuant = [NSString  stringWithFormat:@"Qty : %ld",(long)objcart.strProductQuant.integerValue];
    tcell.lblQty.text = strQuant;
    //Options
    NSMutableString * mstrOpt = [NSMutableString   new];
    NSArray * arrOpt = [[NSArray  alloc]  init];
    arrOpt = [objcart.arrOptions  mutableCopy];
    for (NSDictionary * dict in arrOpt) {
        [mstrOpt   appendString:[dict  objectForKey:@"opt_name"]];
        [mstrOpt   appendString:@" : "];
        [mstrOpt   appendString:[dict  objectForKey:@"opt_value"]];
        [mstrOpt   appendString:@"\n"];
    }
    tcell.lblOptions.text = mstrOpt;
    
    
    
    
    
    tcell.SteperQuant.value = objcart.strProductQuant.floatValue;
    [tcell.btnDelete   addTarget:self action:@selector(btnDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    [tcell.SteperQuant addTarget:self action:@selector(stepperChanged:) forControlEvents:UIControlEventValueChanged];
    [tcell.btnUpdateQuantity   addTarget:self action:@selector(btnUpdateQuantityClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    //
    //    NSString * strTest = [NSString   stringWithFormat:@"Q = %@ - N = %@",objcart.strProductQuant,objcart.strProductName];
    //    NSLog(@"strTest = %@",strTest);
    //    NSLog(@"objcart.strProductName = %@",objcart.strProductName);
    
    
    return tcell;
}



//#pragma mark titleForHeaderInSection
//-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    
//    NSString * strTitle = @"Cart List";
//    return strTitle;
//}

#pragma mark heightForHeaderInSection
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}



#pragma mark
#pragma mark ALERTVIEW DELEGATE
#pragma mark

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {
        if (buttonIndex == 1) {
            UITextField *textfield = [alertView textFieldAtIndex:0];
            NSLog(@"username: %@", textfield.text);
            textfield.text = [textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            while ([textfield.text rangeOfString:@"  "].location != NSNotFound) {
                textfield.text = [textfield.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
            }
            
            if (textfield.text.length <=1 ) {
                [TSMessage  showNotificationInViewController:self title:@"Please Enter Coupon Code" subtitle:nil  type:TSMessageNotificationTypeSuccess];
                return;
            }
            
            [self   webserviceCallForHaveACoupon:textfield.text];
        }
    }
}







#pragma mark
#pragma mark WEBSERVICE CALL
#pragma mark


#pragma mark webserviceCallForCartList
-(void)webserviceCallForCartList
{
    
    
    //LOADER
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    //This value also changed after every new installation of application
    
    NSString  *strDeviceID = objGlobalValue.strDeviceID;
    
    //PARAMETER DICT
    NSDictionary *dictPara = @{@"action":@"cartlist",@"customer_id":objGlobalValue.strCustomerId,@"device_id":strDeviceID,@"cart_login_type":@"L"};
    NSLog(@"dictPara = %@",dictPara);
    
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
        //REMOVE LOADER
        [DejalBezelActivityView  removeViewAnimated:YES];
        
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                
                
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                
                //RESPONSE
                NSLog(@"%@",result);
                
                
                //TOTAL PRICE
                if ([[result  allKeys]  containsObject:kTotal]) {
                    NSString * strtotalPrice = [NSString   stringWithFormat:@"Total Price Rs. %@",[result  objectForKey:kTotal]];
                    self.lbltotalPrice.text = strtotalPrice;
                    
                }
                else
                {
                    self.lbltotalPrice.text = @"";
                }
                
                
                
                
                
                //DISCOUNT
                if ([[result  allKeys]  containsObject:kDiscount_On_Refer]) {
                    NSString * strDicount = [result  objectForKey:@"discount_on_refer"];
                    if (strDicount.intValue == 0) {
                        self.lblDiscount.text = @"";
                    }
                    else
                    {
                        strDicount = [NSString   stringWithFormat:@"Discount on Refer Rs. %@",strDicount];
                        self.lblDiscount.text = strDicount;
                        
                    }
                }
                else
                {
                    //DISCOUNT ON DOWNLOAD
                    if ([[result  allKeys]  containsObject:kDiscount_Download]) {
                        NSString * strDiscountOnDownload = [result  objectForKey:kDiscount_Download];
                        if (strDiscountOnDownload.intValue == 0) {
                            self.lblDiscount.text = @"";
                        }
                        else
                        {
                            self.lblDiscount.text = strDiscountOnDownload;
                        }
                    }
                    self.lblDiscount.text = @"";
                    
                }
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                //PAYABLE AMOUNT
                if ([[result  allKeys]  containsObject:kPayableAmount]) {
                    NSString * strPayableAmount = [result  objectForKey:@"grand_total"];
                    if (strPayableAmount.intValue == 0) {
                        self.lblPayableAmount.text = @"";
                    }
                    else
                    {
                        
                        strPayableAmount = [NSString   stringWithFormat:@"Payable Amount Rs. %@",[result  objectForKey:@"grand_total"]];
                        self.lblPayableAmount.text = strPayableAmount;
                    }
                }
                else
                {
                    self.lblPayableAmount.text = @"";
                }
                
                
                
                
                //Coupon Code
                if ([[result  allKeys]  containsObject:kCoupon_Code]) {
                    NSString *  CouponCode = [NSString   stringWithFormat:@"%@",[result  objectForKey:@"coupon_code"]];
                    if (CouponCode.intValue == 0) {
                        self.lblCouponCode.text = @"";
                    }
                    else
                    {
                        
                        
                        strCouponCodeApplied = CouponCode;
                        CouponCode = [NSString   stringWithFormat:@"Coupon code %@",[result  objectForKey:@"coupon_code"]];
                        self.lblCouponCode.text = CouponCode;
                        [self.btnHaveACoupon   setTitle:kRemoveCoupon forState:UIControlStateNormal];

                    }
                    
                    
                }
                else
                {
                    self.lblCouponCode.text = @"";
                }
                
                
                
                
                //COUPON VALUE
                if ([[result  allKeys]  containsObject:kCoupon_Value]) {
                    NSString *  strCouponValue = [NSString   stringWithFormat:@"%@",[result  objectForKey:@"coupon_value"]];
                    if (strCouponValue.intValue == 0) {
                        self.lblCouponValue.text = @"";
                    }
                    else
                    {
                        strCouponValue = [NSString   stringWithFormat:@"Coupon Value Rs. %@",[result  objectForKey:@"coupon_value"]];
                        self.lblCouponValue.text = strCouponValue;
                    }
                    
                }
                else
                {
                    self.lblCouponValue.text = @"";
                }
                
                
                
                
                
                
                NSArray * arrJsonResponse = [result  objectForKey:@"product"];
                
                marrDataCartModel = [NSMutableArray  new];
                for (NSDictionary * dictProduct in arrJsonResponse) {
                    CartModel * objcart = [CartModel  new];
                    objcart.strProductName = [dictProduct objectForKey:@"product_name"];
                    objcart.strCartId = [dictProduct objectForKey:@"cart_id"];
                    objcart.strProductId = [dictProduct objectForKey:@"product_id"];
                    objcart.arrOptions = [dictProduct objectForKey:@"options"];
                    objcart.strProductImgUrl = [dictProduct objectForKey:@"product_image"];
                    objcart.strProductQuant = [dictProduct objectForKey:@"quantity"];
                    objcart.strProductPrice = [dictProduct objectForKey:@"price"];
                    [marrDataCartModel   addObject:objcart];
                }
                [self.tblvCart   reloadData];
            }
            else
            {
                
                
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage showNotificationWithTitle:@"Error"
                                            subtitle:strmsg
                                                type:TSMessageNotificationTypeError];
                
            }
        }
    }];
    
    
    
    
    
    /*
     //FOR DEBUGGing
     NSString *filePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"CartListProducts" ofType:@"json"];
     NSData *data = [NSData dataWithContentsOfFile:filePath];
     
     NSError *error = nil;
     NSDictionary * result  = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
     
     if (error) {
     NSLog(@"error = %@",error.localizedDescription);
     }
     else
     {
     NSLog(@"result = %@",result);
     }
     
     
     //TOTAL PRICE
     NSString * strtotalPrice = [NSString   stringWithFormat:@"Total Price Rs %@",[result  objectForKey:@"total"]];
     self.lbltotalPrice.text = strtotalPrice;
     
     //PAYABLE AMOUNT
     NSString * strPayableAmount = [NSString   stringWithFormat:@"Payable Amount Rs %@",[result  objectForKey:@"grand_total"]];
     self.lblPayableAmount.text = strPayableAmount;
     
     //DISCOUNT
     NSString * strDicount = [NSString   stringWithFormat:@"Discount on Refer Rs %@",[result  objectForKey:@"discount_on_refer"]];
     self.lblDiscount.text = strDicount;
     
     NSArray * arrJsonResponse = [result  objectForKey:@"product"];
     
     marrDataCartModel = [NSMutableArray  new];
     for (NSDictionary * dictProduct in arrJsonResponse) {
     CartModel * objcart = [CartModel  new];
     objcart.strProductName = [dictProduct objectForKey:@"product_name"];
     objcart.strCartId = [dictProduct objectForKey:@"cart_id"];
     objcart.strProductId = [dictProduct objectForKey:@"product_id"];
     objcart.arrOptions = [dictProduct objectForKey:@"options"];
     objcart.strProductImgUrl = [dictProduct objectForKey:@"product_image"];
     objcart.strProductQuant = [dictProduct objectForKey:@"quantity"];
     objcart.strProductPrice = [dictProduct objectForKey:@"price"];
     [marrDataCartModel   addObject:objcart];
     }
     [self.tblvCart   reloadData];
     */
    
}



#pragma mark webserviceCallForDeleteProductFromCart
-(void)webserviceCallForDeleteProductFromCart:(CartModel *)objcart
{
    /*
     deletefromcart
     cart_id
     customer_id
     device_id
     cart_login_type		L - for login users, G- guest login
     */
    
    
    
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSMutableDictionary * mdict = [NSMutableDictionary   new];
    [mdict  setObject:@"deletefromcart" forKey:@"action"];
    [mdict  setObject:objcart.strCartId forKey:@"cart_id"];
    [mdict  setObject:objGlobalValue.strCustomerId forKey:@"customer_id"];
    [mdict  setObject:objGlobalValue.strDeviceID forKey:@"device_id"];
    [mdict  setObject:@"L" forKey:@"cart_login_type"];
    NSLog(@"mdict = %@",mdict);
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:mdict andCompletionHandler:^(id result, NSError *error) {
        //REMOVE LOADER
        [DejalBezelActivityView  removeViewAnimated:YES];
        
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                objGlobalValue.strCartCount=[result objectForKey:@"product_count"];
                
                //RESPONSE
                NSLog(@"%@",result);
                [self  webserviceCallForCartList];
                
            }
            else
            {
                
                //                responseObject = {
                //                    msg = "No product in the cart";
                //                    status = 200;
                //                    statusCode = 0;
                //                }
                
                
                //When Only One Product Item is in Cart
                NSString * strmsg = [result   objectForKey:@"msg"];
                if ([strmsg  isEqualToString:@"No product in the cart"]) {
                    objGlobalValue.strCartCount = @"0";
                    [self.navigationController  popViewControllerAnimated:YES];
                }
                [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg  type:TSMessageNotificationTypeError];
                
            }
        }
    }];
}


#pragma mark webserviceCallForUpdateProductQuantityFromCart
-(void)webserviceCallForUpdateProductQuantityFromCart:(CartModel *)objcart
{
    //UPDATE Quatity Webservice Call
    /*
     updatecart
     cart_id
     customer_id
     device_id
     cart_login_type		L - for login users, G- guest login
     quantity
     */
    
    
    NSString * strQuant = [NSString   stringWithFormat:@"%d",objcart.strProductQuant.intValue];
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSMutableDictionary * mdict = [NSMutableDictionary   new];
    [mdict  setObject:@"updatecart" forKey:@"action"];
    [mdict  setObject:objcart.strCartId forKey:@"cart_id"];
    [mdict  setObject:objGlobalValue.strCustomerId forKey:@"customer_id"];
    [mdict  setObject:objGlobalValue.strDeviceID forKey:@"device_id"];
    [mdict  setObject:@"L" forKey:@"cart_login_type"];
    [mdict  setObject:strQuant forKey:@"quantity"];
    
    NSLog(@"mdict = %@",mdict);
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:mdict andCompletionHandler:^(id result, NSError *error) {
        //REMOVE LOADER
        [DejalBezelActivityView  removeViewAnimated:YES];
        
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                
                //RESPONSE
                NSLog(@"%@",result);
                [self  webserviceCallForCartList];
                
            }
            else
            {
                NSLog(@"%@",result);
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg  type:TSMessageNotificationTypeError];
                
                [self  webserviceCallForCartList];
                
            }
        }
    }];
}





#pragma mark webserviceCallForHaveACoupon
-(void)webserviceCallForHaveACoupon:(NSString *)strCoupon
{
    //    applycoupon
    //    customer_id
    //    device_id
    //    coupon_code
    
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSMutableDictionary * mdict = [NSMutableDictionary   new];
    [mdict  setObject:@"applycoupon" forKey:@"action"];
    [mdict  setObject:objGlobalValue.strCustomerId forKey:@"customer_id"];
    [mdict  setObject:objGlobalValue.strDeviceID forKey:@"device_id"];
    [mdict  setObject:strCoupon forKey:@"coupon_code"];
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:mdict andCompletionHandler:^(id result, NSError *error) {
        //REMOVE LOADER
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                //RESPONSE
                NSLog(@"%@",result);
                [self   jsonParsingForCoupon:result];
                
                
                
            }
            else
            {
                NSLog(@"%@",result);
                [self webserviceCallForCartList];
                
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg  type:TSMessageNotificationTypeError];
                
            }
        }
    }];
}




#pragma mark jsonParsingForCoupon
-(void)jsonParsingForCoupon:(NSDictionary *)dictResult
{
    /*
     "cart_detail_id" = 228;
     "coupon_code" = 3333;
     "coupon_id" = 5;
     "coupon_value" = "109.5";
     "grand_total" = "2,080.50";
     msg = "Coupon Applied Successfully";
     status = 200;
     statusCode = 1;
     tax = "0.00";
     total = "2,190.00";
     */
    
    //TOTAL PRICE
    if ([[dictResult  allKeys]  containsObject:kTotal]) {
        NSString * strtotalPrice = [NSString   stringWithFormat:@"Total Price Rs. %@",[dictResult  objectForKey:kTotal]];
        self.lbltotalPrice.text = strtotalPrice;
        
    }
    else
    {
        self.lbltotalPrice.text = @"";
    }
    
    
    
    
    
    
    //DISCOUNT
    if ([[dictResult  allKeys]  containsObject:kDiscount_On_Refer]) {
        NSString * strDicount = [dictResult  objectForKey:@"discount_on_refer"];
        if (strDicount.intValue == 0) {
            self.lblDiscount.text = @"";
        }
        else
        {
            strDicount = [NSString   stringWithFormat:@"Discount on Refer Rs. %@",strDicount];
            self.lblDiscount.text = strDicount;
            
        }
    }
    else
    {
        //DISCOUNT ON DOWNLOAD
        if ([[dictResult  allKeys]  containsObject:kDiscount_Download]) {
            NSString * strDiscountOnDownload = [dictResult  objectForKey:kDiscount_Download];
            if (strDiscountOnDownload.intValue == 0) {
                self.lblDiscount.text = @"";
            }
            else
            {
                self.lblDiscount.text = strDiscountOnDownload;
            }
        }
        self.lblDiscount.text = @"";
        
    }
    
    

    
    
    
    
    //PAYABLE AMOUNT
    if ([[dictResult  allKeys]  containsObject:kPayableAmount]) {
        NSString * strPayableAmount = [dictResult  objectForKey:@"grand_total"];
        if (strPayableAmount.intValue == 0) {
            self.lblPayableAmount.text = @"";
        }
        else
        {
            
            strPayableAmount = [NSString   stringWithFormat:@"Payable Amount Rs. %@",[dictResult  objectForKey:@"grand_total"]];
            self.lblPayableAmount.text = strPayableAmount;
        }
    }
    else
    {
        self.lblPayableAmount.text = @"";
    }
    
    
    
    
    //Coupon Code
    if ([[dictResult  allKeys]  containsObject:kCoupon_Code]) {
        NSString *  strCouponCde = [NSString   stringWithFormat:@"%@",[dictResult  objectForKey:@"coupon_code"]];
        strCouponCodeApplied = strCouponCde;
        if (strCouponCde.intValue == 0) {
            self.lblCouponCode.text = @"";
        }
        else
        {
            strCouponCde = [NSString   stringWithFormat:@"Coupon code %@",[dictResult  objectForKey:@"coupon_code"]];
            self.lblCouponCode.text = strCouponCde;
            [self.btnHaveACoupon   setTitle:kRemoveCoupon forState:UIControlStateNormal];
        }
        
        
    }
    else
    {
        self.lblCouponCode.text = @"";
    }
    
    
    
    
    //Coupon Value
    if ([[dictResult  allKeys]  containsObject:kCoupon_Value]) {
        NSString *  strCouponValue = [NSString   stringWithFormat:@"%@",[dictResult  objectForKey:kCoupon_Value]];
        if (strCouponValue.intValue == 0) {
            self.lblCouponValue.text = @"";
        }
        else
        {
            strCouponValue = [NSString   stringWithFormat:@"Coupon Value %@",[dictResult  objectForKey:kCoupon_Value]];
            self.lblCouponValue.text = strCouponValue;
            
        }
        
        
    }
    else
    {
        self.lblCouponValue.text = @"";
    }
    
}




/*
 2015-12-19 09:34:32.122 kazumi[230:11007] {
 "cart_detail_id" = 228;
 "coupon_code" = 3333;
 "coupon_id" = 5;
 "coupon_value" = "109.5";
 "grand_total" = "2,080.50";
 msg = "Coupon Applied Successfully";
 status = 200;
 statusCode = 1;
 tax = "0.00";
 total = "2,190.00";
 }
 
 */



/*
 
 
 @property (weak, nonatomic) IBOutlet UILabel *lbltotalPrice;
 @property (strong, nonatomic) IBOutlet UILabel *lblCouponCode;
 @property (strong, nonatomic) IBOutlet UILabel *lblCouponValue;
 @property (weak, nonatomic) IBOutlet UILabel *lblDiscount;
 @property (weak, nonatomic) IBOutlet UILabel *lblPayableAmount;
 
 
 
 
 */








#pragma mark webservice Call For Remove Coupon
-(void)webserviceCallForremovecoupon:(NSString *)CouponCode
{
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    //PARAMETER DICT
    NSDictionary *dict = @{@"action":@"removecoupon",@"customer_id":objGlobalValue.strCustomerId,@"coupon_code":CouponCode};
    NSLog(@"dictPara = %@",dict);
    
    [Network   getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView  removeViewAnimated:YES];
        
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                [self.btnHaveACoupon setTitle:kHaveCoupon forState:UIControlStateNormal];
                
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                
                
                
                //TOTAL PRICE
                if ([[result  allKeys]  containsObject:kTotal]) {
                    NSString * strtotalPrice = [NSString   stringWithFormat:@"Total Price Rs. %@",[result  objectForKey:kTotal]];
                    self.lbltotalPrice.text = strtotalPrice;
                    
                }
                else
                {
                    self.lbltotalPrice.text = @"";
                }
                
                
                
                
                
                
                //DISCOUNT
                if ([[result  allKeys]  containsObject:kDiscount_On_Refer]) {
                    NSString * strDicount = [result  objectForKey:@"discount_on_refer"];
                    if (strDicount.intValue == 0) {
                        self.lblDiscount.text = @"";
                    }
                    else
                    {
                        strDicount = [NSString   stringWithFormat:@"Discount on Refer Rs. %@",strDicount];
                        self.lblDiscount.text = strDicount;
                        
                    }
                }
                else
                {
                    //DISCOUNT ON DOWNLOAD
                    if ([[result  allKeys]  containsObject:kDiscount_Download]) {
                        NSString * strDiscountOnDownload = [result  objectForKey:kDiscount_Download];
                        if (strDiscountOnDownload.intValue == 0) {
                            self.lblDiscount.text = @"";
                        }
                        else
                        {
                            self.lblDiscount.text = strDiscountOnDownload;
                        }
                    }
                    self.lblDiscount.text = @"";
                    
                }
                
                
                
                
                
                //PAYABLE AMOUNT
                if ([[result  allKeys]  containsObject:kPayableAmount]) {
                    NSString * strPayableAmount = [result  objectForKey:@"grand_total"];
                    if (strPayableAmount.intValue == 0) {
                        self.lblPayableAmount.text = @"";
                    }
                    else
                    {
                        
                        strPayableAmount = [NSString   stringWithFormat:@"Payable Amount Rs. %@",[result  objectForKey:@"grand_total"]];
                        self.lblPayableAmount.text = strPayableAmount;
                    }
                }
                else
                {
                    self.lblPayableAmount.text = @"";
                }
                
                
                
                //Coupon Code
                if ([[result  allKeys]  containsObject:kCoupon_Code]) {
                    NSString *  strCouponCde = [NSString   stringWithFormat:@"%@",[result  objectForKey:@"coupon_code"]];
                    if (strCouponCde.intValue == 0) {
                        self.lblCouponCode.text = @"";
                    }
                    else
                    {
                        strCouponCde = [NSString   stringWithFormat:@"Coupon code %@",[result  objectForKey:@"coupon_code"]];
                        self.lblCouponCode.text = strCouponCde;
                        [self.btnHaveACoupon   setTitle:kRemoveCoupon forState:UIControlStateNormal];
                    }
                    
                    
                }
                else
                {
                    self.lblCouponCode.text = @"";
                }
                
                
                
                
                //Coupon Value
                if ([[result  allKeys]  containsObject:kCoupon_Value]) {
                    NSString *  strCouponValue = [NSString   stringWithFormat:@"%@",[result  objectForKey:kCoupon_Value]];
                    if (strCouponValue.intValue == 0) {
                        self.lblCouponValue.text = @"";
                    }
                    else
                    {
                        strCouponValue = [NSString   stringWithFormat:@"Coupon Value %@",[result  objectForKey:kCoupon_Value]];
                        self.lblCouponValue.text = strCouponValue;
                        
                    }
                    
                    
                }
                else
                {
                    self.lblCouponValue.text = @"";
                }
                
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage showNotificationWithTitle:@"Error"
                                            subtitle:strmsg
                                                type:TSMessageNotificationTypeError];
                
            }
        }
        
        
    }];
}




@end
