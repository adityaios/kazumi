//
//  HomeViewController.m
//  kazumi
//
//  Created by Yashvir on 14/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController



#pragma mark
#pragma mark VIEW CONTROLLER LIFE CYCLE
#pragma mark

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self  intializerMethod];
    [self  setUpUI];
    //WEBSERVICE CALL
    [self  getCategoryListForHome];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //CART BUTTON
    [self   cartBarButton];
    
    // Code to run once
    static dispatch_once_t once;
    dispatch_once(&once, ^ {
        NSString * strSuccess = [NSString   stringWithFormat:@"WELCOME %@",objglobalValues.strFirstName];
        [TSMessage  showNotificationInViewController:self title:strSuccess subtitle:@"Login Successfull" type:TSMessageNotificationTypeSuccess];
    });
}



#pragma mark
#pragma mark intializerMethod
#pragma mark

-(void)intializerMethod
{
    //REVEAL VIEW CONTROLLER
    self.barButton.target = self.revealViewController;
    self.barButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    //NAVIGATION BAR
    self.navigationController.navigationBar.barTintColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    self.title = @"Home";
    
    //GLOBAL VALUES
    objglobalValues = [GlobalValues  sharedManager];
    
    //SEARCH BAR
    self.serachBar.frame = CGRectMake(0,-44, self.view.frame.size.width, 44);
    [self.tblvHomeCat setContentOffset:CGPointMake(0, 44)];
    self.serachBar.delegate = self;
    
}



-(void)cartBarButton
{
    
    
    MKNumberBadgeView *number = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(25, -10,20,20)];
    number.value = objglobalValues.strCartCount.integerValue;
    number.shadow = NO;
    number.shine = YES;
    number.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:14];
    number.strokeColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);;
    number.layer.cornerRadius = 2.0;
    
    
    
    UIImage * imgcart = [UIImage  imageNamed:@"cart"];
    // Allocate UIButton
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0,0,30,30);
    [btn setImage:imgcart forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:number]; //Add NKNumberBadgeView as a subview on UIButton
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
    
}



-(IBAction)cartButtonClicked:(id)sender
{
    NSLog(@"cartButtonClicked");
    
    if (objglobalValues.strCartCount.integerValue == 0) {
        [TSMessage  showNotificationInViewController:self title:@"Cart" subtitle:@"No Product Available in Cart" type:TSMessageNotificationTypeMessage];
    }
    else
    {
        CartViewController * objCartVC = [self.storyboard   instantiateViewControllerWithIdentifier:@"CartViewController"];
        [self.navigationController pushViewController:objCartVC animated:YES];
        
    }
    
}



#pragma mark
#pragma mark setUpUI
#pragma mark

-(void)setUpUI
{
    
}


#pragma mark
#pragma mark - Table view data source
#pragma mark

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger intReturn;
    if (marrCatModelData.count == 0){
        intReturn = 0;
    }
    else
    {
        intReturn = marrCatModelData.count;
    }
    
    return intReturn;
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"strCellIdentifierHome" forIndexPath:indexPath];
    
    CatModel * objCatModel = [marrCatModelData objectAtIndex:indexPath.row];
    UILabel * lblText = (UILabel*)[cell   viewWithTag:200];
    lblText.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:17];
    lblText.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    lblText.text = objCatModel.catName;
    
    UIImageView * imgViewMenu = (UIImageView*)[cell   viewWithTag:100];
    [imgViewMenu setImageWithURL:[NSURL  URLWithString:objCatModel.catImgUrl] placeholderImage:[UIImage  imageNamed:@"placeholder"]];
    
    return cell;
}



#pragma mark
#pragma mark - TABLEVIEW DELEGATE
#pragma mark
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self  performSegueWithIdentifier:ksegueProductHome sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ProductListViewController * objPLVc = segue.destinationViewController;
    NSIndexPath *indexPath = [self.tblvHomeCat indexPathForSelectedRow];
    CatModel * objcatmodel = [marrCatModelData  objectAtIndex:indexPath.row];
    objPLVc.objCatModelPass = objcatmodel;
}





#pragma mark
#pragma mark - SERACH BAR DELEGATE
#pragma mark
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"Cancel");
    [self.serachBar  resignFirstResponder];
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"GO");
    [self.serachBar  resignFirstResponder];
    
}



- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"%@",searchText);
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.serachBar resignFirstResponder];
}


-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
}



@end
