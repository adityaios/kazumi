//
//  RegistrationViewController.m
//  kazumi
//
//  Created by Yashvir on 19/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "RegistrationViewController.h"

@interface RegistrationViewController ()

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  setUpUI];
    [self  CustomBackButton];
    [self  intializerMethod];
    [self.btnActivateAccount setHidden:YES];
}

#pragma mark
#pragma mark  CustomBackButton
#pragma mark
-(void)CustomBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}


- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}





#pragma mark
#pragma mark  intializerMethod
#pragma mark
-(void)intializerMethod
{
    NSLog(@"Dictionary = %d",self.dictSocialData.count);
    if (self.dictSocialData.count == 0) {
    }
    else
    {
        NSLog(@"Dictionary = %@",self.dictSocialData);
        if ([[self.dictSocialData  objectForKey:kSocialType ] isEqualToString:kSocialTypeGoggle]) {
            
            //NAME
            NSArray *arrNames = [[self.dictSocialData  objectForKey:@"name"]componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (arrNames.count == 0) {
                
            }
            else if (arrNames.count == 2)
            {
                self.txfdfirstName.text = [arrNames  objectAtIndex:0];
                self.txfdlastName.text = [arrNames  objectAtIndex:1];
                
            }
            else
            {
                self.txfdfirstName.text = [arrNames  objectAtIndex:0];
            }
            
            
            //EMAIL
            self.txfdemail.text = [self.dictSocialData  objectForKey:@"email"];
            
            //MESSAGE
            [TSMessage  showNotificationInViewController:self title:@"Social Login"  subtitle:@"Please fill rest of details for Registration with G+" type:TSMessageNotificationTypeSuccess];
            
            
            
        }
        else if ([[self.dictSocialData  objectForKey:kSocialType] isEqualToString:kSocialTypeFaceBook])
        {
            
            
            //NAME
            NSArray *arrNames = [[self.dictSocialData  objectForKey:@"name"]componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (arrNames.count == 0) {
                
            }
            else if (arrNames.count == 2)
            {
                self.txfdfirstName.text = [arrNames  objectAtIndex:0];
                self.txfdlastName.text = [arrNames  objectAtIndex:1];
                
            }
            else
            {
                self.txfdfirstName.text = [arrNames  objectAtIndex:0];
            }
            
            
            //EMAIL
            self.txfdemail.text = [self.dictSocialData  objectForKey:@"email"];
            
            //MESSAGE
            [TSMessage  showNotificationInViewController:self title:@"Social Login"  subtitle:@"Please fill rest of details for Registration with FaceBook" type:TSMessageNotificationTypeSuccess];
        }
    }
}




#pragma mark
#pragma mark  setUpUI
#pragma mark

-(void)setUpUI
{
    
    self.title = @"Registration";
    
    
    //BG IMAGE VIEW
    self.bgImageView.frame = CGRectMake(0,64,SelfViewWidth,SelfViewHeight);
    //SCROLLVIEW
    self.bgScrollView.frame = CGRectMake(0,64,SelfViewWidth,SelfViewHeight);
    self.bgScrollView.contentSize = CGSizeMake(SelfViewWidth, SelfViewHeight + SelfViewHeight/4);
    
    
    
    int Xpadding = 10;
    int Ypadding = 15;
    int Y = Ypadding;
    
    
    //TYPE SELECTION
    self.btnRetailer.frame = CGRectMake(Xpadding,Y, 30, 30);
    self.lblRetailer.frame = CGRectMake(Xpadding+35,Y+02,95,25);
    
    self.btnWholesaler.frame = CGRectMake(self.lblRetailer.frame.origin.x+self.lblRetailer.frame.size.width+05,Y, 30, 30);
    self.lblWholesaler.frame = CGRectMake(self.btnWholesaler.frame.origin.x+35, Y+02, 95, 25);
    
    Y = Y + Ypadding + 30;
    
    
    
    
    //TEXTFIELD FIRST NAME
    self.txfdfirstName.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewfirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    //paddingViewfirstName.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    self.txfdfirstName.leftView = paddingViewfirstName;
    self.txfdfirstName.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    Y = Y + Ypadding + 40;
    
    
    //TEXTFIELD LAST NAME
    self.txfdlastName.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewlastName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    //paddingViewlastName.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    self.txfdlastName.leftView = paddingViewlastName;
    self.txfdlastName.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    Y = Y + Ypadding +40;
    
    //TEXTFIELD EMAIL
    self.txfdemail.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView * paddingViewEmail = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
   // paddingViewEmail.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    
    
    
    self.txfdemail.leftView = paddingViewEmail;
    self.txfdemail.leftViewMode = UITextFieldViewModeAlways;
    
    
    Y = Y + Ypadding +40;
    
    //TEXTFIELD PHONE
    self.txfdPhoneNo.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView * paddingViewPhone = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
   // paddingViewPhone.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    
    self.txfdPhoneNo.leftView = paddingViewPhone;
    self.txfdPhoneNo.leftViewMode = UITextFieldViewModeAlways;
    [self  textfieldToolBar:self.txfdPhoneNo];
    
    
    
    Y = Y + Ypadding +40;
    
    //TEXTFIELD USER NAME
    self.txfdUserName.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView * paddingViewUser = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    //paddingViewUser.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    
    
    
    self.txfdUserName.leftView = paddingViewUser;
    self.txfdUserName.leftViewMode = UITextFieldViewModeAlways;
    
    
    Y = Y + Ypadding +40;
    
    //TEXTFIELD PASSWORD
    self.txfdpswd.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView * paddingViewPswd = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    //paddingViewPswd.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    
    
    self.txfdpswd.leftView = paddingViewPswd;
    self.txfdpswd.leftViewMode = UITextFieldViewModeAlways;
    
    
    Y = Y + Ypadding +40;
    
    //TEXTFIELD CONFIRM PASSWORD
    self.txfdConfirmPswd.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView * paddingViewConfirmPswd = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
   // paddingViewConfirmPswd.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    
    
    
    self.txfdConfirmPswd.leftView = paddingViewConfirmPswd;
    self.txfdConfirmPswd.leftViewMode = UITextFieldViewModeAlways;
    
    
    Y = Y+Ypadding+40;
    
    
    //TEXTFIELD REFRAL CODE
    self.txfdReferalCode.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView * paddingViewRefralCode = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
   // paddingViewRefralCode.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    
    self.txfdReferalCode.leftView = paddingViewRefralCode;
    self.txfdReferalCode.leftViewMode = UITextFieldViewModeAlways;
    
    
    Y = Y + Ypadding +40;
    
    //BUTTON REGISTER
    
    self.btnRegister.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    //self.btnRegister.layer.borderWidth = 2.0;
    self.btnRegister.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    //self.btnRegister.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnRegister.layer.cornerRadius = kButtonCornerRadius;
    self.btnRegister.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
    
    self.imgViewArrowRegister.frame = CGRectMake(self.btnRegister.frame.origin.x+self.btnRegister.frame.size.width - 40, Y+02, 38, 38);
    
    
    Y = Y + Ypadding +40;
    
    self.btnActivateAccount.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    //self.btnActivateAccount.layer.borderWidth = 2.0;
    self.btnActivateAccount.backgroundColor = UIColorFromRGBWithAlpha(kazumiYellowColor, 1.0);
    //self.btnActivateAccount.layer.borderColor =  [UIColor  orangeColor].CGColor;
    self.btnActivateAccount.layer.cornerRadius = kButtonCornerRadius;
    self.btnActivateAccount.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
    
    
    
}



-(void)textfieldToolBar:(UITextField *)txfd
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,SelfViewWidth, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor  redColor];
    
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    txfd.inputAccessoryView = numberToolbar;
}


-(void)cancelNumberPad{
    [self.txfdPhoneNo resignFirstResponder];
    self.txfdPhoneNo.text = @"";
}

-(void)doneWithNumberPad{
    NSString *numberFromTheKeyboard = self.txfdPhoneNo.text;
    [self.txfdPhoneNo resignFirstResponder];
}




#pragma mark
#pragma mark  TEXTFILED DELEGATE
#pragma mark

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    //reset offset of content
    if (textField.tag == 6) {
        [self.bgScrollView setContentOffset:
         CGPointMake(0, -self.bgScrollView.contentInset.top) animated:YES];
        [textField  resignFirstResponder];
        return YES;
    }
    
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    
    if (nextResponder) {
        [self.bgScrollView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        [self.bgScrollView setContentOffset:CGPointMake(0,0) animated:YES];
        [textField resignFirstResponder];
        return YES;
    }
    return NO;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view  endEditing:YES];
}




// called when textField start editting.
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    [self.bgScrollView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
}





#pragma mark
#pragma mark  IBACTION
#pragma mark

- (IBAction)btnRetailerClicked:(UIButton *)sender {
    if ([self.btnWholesaler  isSelected]) {
        self.btnWholesaler.selected = NO;
    }
    [self.btnRetailer  setSelected:YES];
    strUserType = @"1";//RETAILER
    
}

- (IBAction)btnActivateAccountClicked:(UIButton *)sender {
    [self  performSegueWithIdentifier:ksegue_registrationActivationVC sender:nil];
}


- (IBAction)btnWholesalerClicked:(UIButton *)sender {
    if ([self.btnRetailer  isSelected]) {
        self.btnRetailer.selected = NO;
    }
    [self.btnWholesaler  setSelected:YES];
    strUserType = @"2";//WHOLESALER
    
}





- (IBAction)btnRegisterClicked:(UIButton *)sender {
    
    self.txfdfirstName.text = [self.txfdfirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.txfdlastName.text = [self.txfdlastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.txfdemail.text = [self.txfdemail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.txfdPhoneNo.text = [self.txfdPhoneNo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.txfdUserName.text = [self.txfdUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.txfdpswd.text = [self.txfdpswd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.txfdConfirmPswd.text = [self.txfdConfirmPswd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.txfdReferalCode.text = [self.txfdReferalCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    
    
    //VALIDATIONS DOUBLE SPACE
    //FIRST NAME
    while ([self.txfdfirstName.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdfirstName.text = [self.txfdfirstName.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    //LAST NAME
    while ([self.txfdlastName.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdlastName.text = [self.txfdlastName.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    
    //EMAIL
    while ([self.txfdemail.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdemail.text = [self.txfdemail.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    //PHONE
    while ([self.txfdPhoneNo.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdPhoneNo.text = [self.txfdPhoneNo.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    
    //USER NAME
    while ([self.txfdUserName.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdUserName.text = [self.txfdUserName.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    
    //PSWD
    while ([self.txfdpswd.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdpswd.text = [self.txfdpswd.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    //CONFIRM PSWD
    while ([self.txfdConfirmPswd.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdConfirmPswd.text = [self.txfdConfirmPswd.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    
    if (self.txfdfirstName.text.length == 0 || self.txfdlastName.text.length == 0 || self.txfdemail.text.length == 0 || self.txfdPhoneNo.text.length == 0 || self.txfdUserName.text.length == 0 || self.txfdpswd.text.length == 0 || self.txfdConfirmPswd.text.length == 0 ) {
        [TSMessage showNotificationWithTitle:@"Validation Error"
                                    subtitle:@"Please Fill all Fields"
                                        type:TSMessageNotificationTypeError];
        return;
        
    }
    
    
    
    //MOBILE NO VALIDATION
    if (self.txfdPhoneNo.text.length <= 8 || self.txfdPhoneNo.text.length >= 15) {
        [TSMessage showNotificationWithTitle:@"PHONE NO ERROR"
                                    subtitle:@"Please enter valid mobile number"
                                        type:TSMessageNotificationTypeError];
        return;
        
        
    }
    
    
    //EMAIL VALIDATION
    if (![self  NSStringIsValidEmail:self.txfdemail.text]) {
        [TSMessage showNotificationWithTitle:@"EMAIL ERROR"
                                    subtitle:@"Please enter valid email"
                                        type:TSMessageNotificationTypeError];
        
        return;
        
    }
    
    
    
    //PSWD
    if (self.txfdpswd.text.length <=5) {
        [TSMessage showNotificationWithTitle:@"PASSWORD ERROR"
                                    subtitle:@"Please enter password greater then 5 characters"
                                        type:TSMessageNotificationTypeError];
        return;
        
    }
    
    
    
    //CONFIRM PSWD
    if (![self.txfdpswd.text  isEqualToString:self.txfdConfirmPswd.text]) {
        [TSMessage showNotificationWithTitle:@"CONFIRM PASSWORD ERROR"
                                    subtitle:@"Please re confirm Password"
                                        type:TSMessageNotificationTypeError];
        return;
        
    }
    
    
    
    //USER TYPE
    NSLog(@"strUserType =%@",strUserType);
    if (strUserType.integerValue ==1 || strUserType.integerValue ==2  ) {
    }
    else
    {
        [TSMessage showNotificationWithTitle:@"USER TYPE ERROR"
                                    subtitle:@"Please select User Type"
                                        type:TSMessageNotificationTypeError];
        return;
        
    }
    
    
    
    
    
    
    
    
    
    if ([Reachability isConnected]) {
        [self  webserviceCallForRegistration];
    }
}




-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}





/*
 user_name
 first_name
 last_name
 email_id
 password
 phone
 user_type
 fb_id
 gplus_id
 */



#pragma mark
#pragma mark  webserviceCallForRegistration
#pragma mark
-(void)webserviceCallForRegistration
{
    //WEBSERVICE CALL
    
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    
    NSDictionary *dict;
    if (self.txfdReferalCode.text.length == 0) {
        dict = @{@"action" : @"registration",@"user_name" : self.txfdUserName.text,@"first_name" : self.txfdfirstName.text,@"last_name" : self.txfdlastName.text,@"email_id" : self.txfdemail.text,@"password" : self.txfdpswd.text,@"phone" : self.txfdPhoneNo.text,@"user_type" : strUserType};
        
        
        //SOCIAL SIGN IN
        if ([[self.dictSocialData allKeys] containsObject:kSocialType]){
            
            //GOGGLE SIGN IN
            if ([[self.dictSocialData  objectForKey:kSocialType] isEqualToString:kSocialTypeGoggle]) {
                NSString * strGoogleID = [self.dictSocialData  objectForKey:@"userId"];
                dict = @{@"action" : @"registration",@"user_name" : self.txfdUserName.text,@"first_name" : self.txfdfirstName.text,@"last_name" : self.txfdlastName.text,@"email_id" : self.txfdemail.text,@"password" : self.txfdpswd.text,@"phone" : self.txfdPhoneNo.text,@"user_type" : strUserType,@"gplus_id":strGoogleID};
            }
            else
            {
                //FACEBOOK
                NSString * strFbID = [self.dictSocialData  objectForKey:@"userId"];
                dict = @{@"action" : @"registration",@"user_name" : self.txfdUserName.text,@"first_name" : self.txfdfirstName.text,@"last_name" : self.txfdlastName.text,@"email_id" : self.txfdemail.text,@"password" : self.txfdpswd.text,@"phone" : self.txfdPhoneNo.text,@"user_type" : strUserType,@"fb_id":strFbID};
                
                
                
            }
        }
    }
    else
    {
        dict = @{@"action" : @"registration",@"user_name" : self.txfdUserName.text,@"first_name" : self.txfdfirstName.text,@"last_name" : self.txfdlastName.text,@"email_id" : self.txfdemail.text,@"password" : self.txfdpswd.text,@"phone" : self.txfdPhoneNo.text,@"user_type" : strUserType,@"referral_code":self.txfdReferalCode.text};
        
        //SOCIAL SIGN IN
        if ([[self.dictSocialData allKeys] containsObject:kSocialTypeGoggle]){
            //GOGGLE SIGN IN
            if ([[self.dictSocialData  objectForKey:kSocialType] isEqualToString:kSocialTypeGoggle]) {
                NSString * strGoogleID = [self.dictSocialData  objectForKey:@"userId"];
                dict = @{@"action" : @"registration",@"user_name" : self.txfdUserName.text,@"first_name" : self.txfdfirstName.text,@"last_name" : self.txfdlastName.text,@"email_id" : self.txfdemail.text,@"password" : self.txfdpswd.text,@"phone" : self.txfdPhoneNo.text,@"user_type" : strUserType,@"referral_code":self.txfdReferalCode.text,@"gplus_id":strGoogleID};
            }
            else
            {
                //FACEBOOK
                NSString * strFBID = [self.dictSocialData  objectForKey:@"userId"];
                dict = @{@"action" : @"registration",@"user_name" : self.txfdUserName.text,@"first_name" : self.txfdfirstName.text,@"last_name" : self.txfdlastName.text,@"email_id" : self.txfdemail.text,@"password" : self.txfdpswd.text,@"phone" : self.txfdPhoneNo.text,@"user_type" : strUserType,@"referral_code":self.txfdReferalCode.text,@"fb_id":strFBID};
            }
        }
    }
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
        if (error) {
            [DejalBezelActivityView  removeViewAnimated:YES];
            NSLog(@"%@",error);
        }
        else
        {
            [DejalBezelActivityView  removeViewAnimated:YES];
            NSLog(@"%@",result);
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [self  performSegueWithIdentifier:ksegue_registrationActivationVC sender:nil];
                [TSMessage  showNotificationInViewController:self title:@"Registeration Successful" subtitle:strmsg type:TSMessageNotificationTypeSuccess];
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Registeration Failed" subtitle:strmsg type:TSMessageNotificationTypeError];
            }
        }
    }];
    
}




@end
