//
//  Location.h
//  kazumi
//
//  Created by Yashvir on 01/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject
@property(nonatomic,strong)  NSString * strLocationId;
@property(nonatomic,strong)  NSString * strLocationName;


@end
