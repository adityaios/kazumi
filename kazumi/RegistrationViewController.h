//
//  RegistrationViewController.h
//  kazumi
//
//  Created by Yashvir on 19/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KazumiMacros.h"
#import "TSMessage.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#import "Network.h"
#import "Constant.h"


@interface RegistrationViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate>{
    UITextField * activeField;
    NSString * strUserType;
    NSString * strsocialId;
   
}



@property (strong, nonatomic) NSDictionary  * dictSocialData;


@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *bgScrollView;
@property (weak, nonatomic) IBOutlet UITextField *txfdfirstName;
@property (weak, nonatomic) IBOutlet UITextField *txfdlastName;
@property (weak, nonatomic) IBOutlet UITextField *txfdemail;
@property (weak, nonatomic) IBOutlet UITextField *txfdPhoneNo;
@property (weak, nonatomic) IBOutlet UITextField *txfdUserName;
@property (weak, nonatomic) IBOutlet UITextField *txfdpswd;
@property (weak, nonatomic) IBOutlet UITextField *txfdConfirmPswd;
@property (weak, nonatomic) IBOutlet UITextField *txfdReferalCode;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewArrowRegister;

@property (weak, nonatomic) IBOutlet UILabel *lblRetailer;
@property (weak, nonatomic) IBOutlet UILabel *lblWholesaler;

@property (weak, nonatomic) IBOutlet UIButton *btnRetailer;
@property (weak, nonatomic) IBOutlet UIButton *btnWholesaler;
@property (weak, nonatomic) IBOutlet UIButton *btnActivateAccount;





- (IBAction)btnWholesalerClicked:(UIButton *)sender;
- (IBAction)btnRetailerClicked:(UIButton *)sender;
- (IBAction)btnActivateAccountClicked:(UIButton *)sender;











//METHODS
-(void)setUpUI;
-(void)CustomBackButton;
-(void)intializerMethod;
-(void)textfieldToolBar:(UITextField *)txfd;
-(void)webserviceCallForRegistration;
-(BOOL) NSStringIsValidEmail:(NSString *)checkString;


- (IBAction)btnRegisterClicked:(UIButton *)sender;


@end


