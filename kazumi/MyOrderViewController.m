//
//  MyOrderViewController.m
//  kazumi
//
//  Created by NM8 on 01/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "MyOrderViewController.h"

@interface MyOrderViewController ()

@end

@implementation MyOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  setUpUI];
    [self  intializerMethod];
    [self  customBackButton];
    
    if ([Reachability  isConnected]) {
         intPagination = 1;
         NSString  * strPage = [NSString   stringWithFormat:@"%ld",(long)intPagination];
         [self  webserviceCallForMyOrders:strPage];
    }
}


#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    self.title =@"My Order";
    objglobalValues=[GlobalValues sharedManager];
    
    UINib *nib = [UINib nibWithNibName:@"MyOrderTableViewCell" bundle:nil];
    [self.tblvOrders registerNib:nib forCellReuseIdentifier:@"MyOrderCell"];
    
}


#pragma mark
#pragma mark setUpUI
#pragma mark
-(void)setUpUI
{
    self.tblvOrders.frame = CGRectMake(0, 0,SelfViewWidth, SelfViewHeight);
}



#pragma mark
#pragma mark CustomBackButton
#pragma mark
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}

- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark
#pragma mark webserviceCallForMyOrders
#pragma mark
-(void)webserviceCallForMyOrders:(NSString *)strPagePara
{
  
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
     NSDictionary * dictParam = @{@"action":@"getallorders",@"customer_id":objglobalValues.strCustomerId,@"start":strPagePara};
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictParam andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            
            NSLog(@"result = %@",result);
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"My Orders" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
                
                
                marrOrderModelData = [NSMutableArray  new];
                
                //PARSING RESPONSE
                for (NSDictionary * dictOrder in [result  objectForKey:@"Result"]) {
                    MyOrders * objOrder = [MyOrders  new];
                    objOrder.strOrderID = [dictOrder  objectForKey:@"order_id"];
                    objOrder.strOrderStatus = [dictOrder  objectForKey:@"order_status"];
                    objOrder.strOrderDate = [dictOrder  objectForKey:@"order_date"];
                    objOrder.strOrderTotal = [dictOrder  objectForKey:@"total"];
                    [marrOrderModelData  addObject:objOrder];
                    
                    
              }
                [self.tblvOrders  reloadData];
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"My Orders" subtitle:strmsg  type:TSMessageNotificationTypeError];
            }
        }
    }];
    
    
    
    
}




#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return marrOrderModelData?marrOrderModelData.count:0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyOrderTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"MyOrderCell"];
    
    cell.layer.borderColor = [UIColor  lightGrayColor].CGColor;
    cell.layer.borderWidth = 0.3;
    cell.layer.masksToBounds = true;
    
    
    MyOrders * objMyOrder = [marrOrderModelData  objectAtIndex:indexPath.row];
    cell.lblOrderID.text = objMyOrder.strOrderID;
    cell.lblStatus.text =  objMyOrder.strOrderStatus;
    NSString * strTotal = [NSString  stringWithFormat:@"Rs %0.2f",[objMyOrder.strOrderTotal floatValue]];
    cell.lblTotal.text = strTotal;
    cell.lblDate.text =  objMyOrder.strOrderDate;
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self   performSegueWithIdentifier:ksegue_OrderDetails sender:nil];
}


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:ksegue_OrderDetails])
    {
        MyOrderDetailsViewController * objOrderDetailVC = [segue destinationViewController];
        NSIndexPath * objindexPath = [self.tblvOrders   indexPathForSelectedRow];
        objOrderDetailVC.objOrderPass = [marrOrderModelData  objectAtIndex:objindexPath.row];
    }
}





@end
