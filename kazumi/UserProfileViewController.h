//
//  UserProfileViewController.h
//  kazumi
//
//  Created by Yashvir on 20/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Reachability.h"
#import "Network.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "GlobalValues.h"
#import "KazumiMacros.h"
#import "Constant.h"
#import "WishListViewController.h"
#import "MyOrderViewController.h"
#import "ChangePasswordViewController.h"
#import "InviteViewController.h"
#import "InviteViewController.h"
#import "MKNumberBadgeView.h"
#import "CartViewController.h"


@interface UserProfileViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    GlobalValues * objglobalValues;
    NSMutableArray * marrAboutUsOptions;
}
@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButtonProfile;


@property (weak, nonatomic) IBOutlet UIImageView *imgViewBg;
@property (weak, nonatomic) IBOutlet UITableView *tblvAbout;
@property (weak, nonatomic) IBOutlet UIView *viewtblvHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblgmail;
@property (strong, nonatomic) IBOutlet UILabel *lblCustomerType;




//METHODS
-(void)setUpUI;
-(void)intializerMethod;
-(void)webserviceCallForProfile;

@end
