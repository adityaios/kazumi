//
//  PaymentMethodTableViewController.m
//  kazumi
//
//  Created by Yashvir on 19/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "PaymentMethodTableViewController.h"

@interface PaymentMethodTableViewController ()

@end

@implementation PaymentMethodTableViewController


#define CashOnDelivery @"Cash On Delivery"
#define BankTransfer @"Bank Transfer"




- (void)viewDidLoad {
    [super viewDidLoad];
    [self intializerMethod];
    [self customBackButton];
    NSLog(@"dictOrderPass = %@",self.dictOrderPass);
}


#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    self.title =@"Payment Method";
    objGlobalValues = [GlobalValues  sharedManager];
    
    //BTN PLACE ORDER
    //self.btnPlaceOrder.layer.borderWidth = 0.1;
    self.btnPlaceOrder.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    //self.btnPlaceOrder.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnPlaceOrder.layer.cornerRadius = kButtonCornerRadius;
    self.btnPlaceOrder.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
    
    
}


#pragma mark
#pragma mark customBackButton
#pragma mark
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil )
    {
        cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    }
    
    
    if ([indexPath compare:self.lastIndexPath] == NSOrderedSame)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    if (indexPath.section==0) {
        cell.textLabel.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:17];
        cell.textLabel.text = @"Cash On Delivery";
    }
    if (indexPath.section==1) {
        cell.textLabel.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:17];
        cell.textLabel.text = @" Bank Transfer";
    }
    
    return cell;
}

// UITableView Delegate Method
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.lastIndexPath = indexPath;
    strType = [NSString  stringWithFormat:@"%ld",(long)self.lastIndexPath.section + 1];
    [tableView reloadData];
}



#pragma mark
#pragma mark IBACTION
#pragma mark
- (IBAction)btnPlaceOrderClicked:(UIButton *)sender {
    NSLog(@"Selected = %ld",(long)self.lastIndexPath.section);
    /*
     addorder
     customer_id		to place order after selecting payment method and shipping
     device_id
     shipping_method
     payment_method
     total
     tax
     coupon_id
     coupon_value
     */
    
    
    /*
     url = Constant.API_HOST_URL + "?action=addorder"+ "&customer_id=" + Customer_id + "&device_id=" + android_id
     + "&shipping_method=" + "free" + "&payment_method="+paymentM + ""
     + "&coupon_id=" + coupenId +"&coupon_code=" + coupenCode + "&coupon_value="+coupenVaule+"&grand_total=" +gtotal+"&total=" +total
     + "&tax=" + Taxvalue ;
     */
    
    NSLog(@"StrType = %@",strType);
    
    
    
    NSMutableDictionary * mdict = [NSMutableDictionary   new];
    [mdict  setObject:@"addorder" forKey:@"action"];
    [mdict  setObject:objGlobalValues.strCustomerId forKey:@"customer_id"];
    [mdict  setObject:objGlobalValues.strDeviceID forKey:@"device_id"];
    [mdict  setObject:@"free" forKey:@"shipping_method"];
    
    
    if (strType.integerValue == 1) {
        [mdict  setObject:CashOnDelivery forKey:@"payment_method"];
    }
    else if (strType.integerValue == 2)
    {
        [mdict  setObject:BankTransfer forKey:@"payment_method"];
    }
    else
    {
        [TSMessage  showNotificationInViewController:self title:@"Please Select Payment Type" subtitle:nil  type:TSMessageNotificationTypeError];
        return;
    }
    
    
    
    
    //    "coupon_code" = "";
    //    "coupon_id" = "<null>";
    //    "coupon_value" = "";
    
    
    
    //COUPON ID
    if ([[self.dictOrderPass  allKeys]  containsObject:@"coupon_id"]) {
        NSString * strCouponID = [NSString   stringWithFormat:@"%@",[self.dictOrderPass  objectForKey:@"coupon_id"]];
        if (strCouponID == (id)[NSNull null] || [strCouponID isEqualToString:@"<null>"])
        {
            strCouponID = @"";
        }
        [mdict  setObject:strCouponID forKey:@"coupon_id"];
        
        
    }
    else
    {
        [mdict  setObject:@"" forKey:@"coupon_id"];
    }
    
    
    
    
    //COUPON CODE
    if ([[self.dictOrderPass  allKeys]  containsObject:@"coupon_code"]) {
        NSString * strCouponCode = [NSString   stringWithFormat:@"%@",[self.dictOrderPass  objectForKey:@"coupon_code"]];
        if (strCouponCode == (id)[NSNull null] || strCouponCode.length == 0 )
        {
            strCouponCode = @"";
        }
        [mdict  setObject:strCouponCode forKey:@"coupon_code"];
        
        
    }
    else
    {
        [mdict  setObject:@"" forKey:@"coupon_code"];
    }
    
    
    
    
    //COUPON VALUE
    if ([[self.dictOrderPass  allKeys]  containsObject:@"coupon_value"]) {
        NSString * strCouponValue = [NSString   stringWithFormat:@"%@",[self.dictOrderPass  objectForKey:@"coupon_value"]];
        if (strCouponValue == (id)[NSNull null] || strCouponValue.length == 0 )
        {
            strCouponValue = @"";
        }
        [mdict  setObject:strCouponValue forKey:@"coupon_value"];
        
        
    }
    else
    {
        [mdict  setObject:@"" forKey:@"coupon_value"];
    }
    
    
    //Grand Total
    if ([[self.dictOrderPass  allKeys]  containsObject:@"grand_total"]) {
        NSString * strGrandTotal = [NSString   stringWithFormat:@"%@",[self.dictOrderPass  objectForKey:@"grand_total"]];
        if (strGrandTotal == (id)[NSNull null] || strGrandTotal.length == 0 )
        {
            strGrandTotal = @"";
        }
        [mdict  setObject:strGrandTotal forKey:@"grand_total"];
        
        
    }
    else
    {
        [mdict  setObject:@"" forKey:@"grand_total"];
    }
    
    
    //Total
    if ([[self.dictOrderPass  allKeys]  containsObject:@"total"]) {
        NSString * strGrandTotal = [NSString   stringWithFormat:@"%@",[self.dictOrderPass  objectForKey:@"total"]];
        if (strGrandTotal == (id)[NSNull null] || strGrandTotal.length == 0 )
        {
            strGrandTotal = @"";
        }
        [mdict  setObject:strGrandTotal forKey:@"total"];
        
        
    }
    else
    {
        [mdict  setObject:@"" forKey:@"total"];
    }
    
    
    
    //Tax
    if ([[self.dictOrderPass  allKeys]  containsObject:@"tax"]) {
        NSString * strTax = [NSString   stringWithFormat:@"%@",[self.dictOrderPass  objectForKey:@"total"]];
        if (strTax == (id)[NSNull null] || strTax.length == 0 )
        {
            strTax = @"";
        }
        [mdict  setObject:strTax forKey:@"tax"];
    }
    else
    {
        [mdict  setObject:@"" forKey:@"tax"];
    }
    
    NSLog(@"mdict = %@",mdict);
    if ([Reachability  isConnected]) {
        [self  webserviceCallForPlaceOrder:mdict];
    }
    
    
    
}


#pragma mark webserviceCallForPlaceOrder
-(void)webserviceCallForPlaceOrder:(NSDictionary *)dict
{
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
        //REMOVE LOADER
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                
                //SUCCESS MSG
                NSString  * strmsg = [NSString  stringWithFormat:@"%@\n Your Order Id is - %@",[result   objectForKey:@"msg"],[[result   objectForKey:@"Result"] objectForKey:@"order_id"]];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                
                //RESPONSE
                NSLog(@"%@",result);
                
                
                //ALERT
                UIAlertController * alert = [UIAlertController   alertControllerWithTitle:@"Thank You" message:strmsg preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction  actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    objGlobalValues.strCartCount = @"0";
                    [self.navigationController  popToRootViewControllerAnimated:YES];
                }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                
                
            }
            else
            {
                
                /*
                 {
                    Result =     {
                        "order_id" = 107;
                    };
                    msg = "Error In Sending Mail";
                    status = 1;
                    statusCode = 0;
                }
              */
                
                if ([[result  allKeys] containsObject:@"Result"]) {
                    NSDictionary * dict = [result  objectForKey:@"Result"];
                    if ([[dict  allKeys] containsObject:@"order_id"]){
                        NSString  * strmsg = [NSString  stringWithFormat:@"%@\n Your Order Id is - %@",[result   objectForKey:@"msg"],[[result   objectForKey:@"Result"] objectForKey:@"order_id"]];
                        [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg  type:TSMessageNotificationTypeError];
                        objGlobalValues.strCartCount = @"0";
                        [self.navigationController  popToRootViewControllerAnimated:YES];
                        return ;
                        
                        
                    }
                }
                
                NSLog(@"%@",result);
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg  type:TSMessageNotificationTypeError];
            }
        }
    }];
}



/*
 {
 Result =     {
 "order_id" = 87;
 };
 msg = "Your order has been received successfully";
 status = 200;
 statusCode = 1;
 }
 */














@end
