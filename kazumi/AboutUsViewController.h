//
//  AboutUsViewController.h
//  kazumi
//
//  Created by Yashvir on 20/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Reachability.h"
#import "Network.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "MKNumberBadgeView.h"
#import "CartViewController.h"



@interface AboutUsViewController : UIViewController
{
 GlobalValues * objglobalValues;

}
@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButtonAboutUs;
@property (weak, nonatomic) IBOutlet UIWebView *webViewAbout;





//METHODS
-(void)setUpUI;
-(void)intializerMethod;
-(void)webserviceCallAbout;
-(void)setWebViewUsingHtlml:(NSMutableString*)strhtml;

@end
