//
//  MyOrderTableViewCell.h
//  kazumi
//
//  Created by Yashvir on 02/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewOrderCell;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderID;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;





@end
