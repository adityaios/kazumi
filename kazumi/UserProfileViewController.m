//
//  UserProfileViewController.m
//  kazumi
//
//  Created by Yashvir on 20/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "UserProfileViewController.h"

@interface UserProfileViewController ()

@end

@implementation UserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self   intializerMethod];
    [self   setUpUI];
    [self   webserviceCallForProfile];
}



#pragma mark
#pragma mark intializerMethod
#pragma mark

-(void)intializerMethod
{
    objglobalValues = [GlobalValues   sharedManager];
    
    //REVEAL VIEW CONTROLLER
    self.barButtonProfile.target = self.revealViewController;
    self.barButtonProfile.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    //GLOBAL VALUES
    objglobalValues = [GlobalValues   sharedManager];
    
    //TITLE
    self.title = @"My Account";
    
    //ABOUT US OPTIONS
     marrAboutUsOptions = [[NSMutableArray   alloc] initWithObjects:@"Edit Profile",@"Change Password",@"My Wish List",@"My Orders",@"Invite Friends", nil];
    
}




#pragma mark
#pragma mark setUpUI
#pragma mark

-(void)setUpUI
{
    
    //BG IMAGE VIEW
    self.imgViewBg.frame = CGRectMake(0,64,SelfViewWidth,SelfViewHeight);
    
    //TBLV
    self.tblvAbout.frame = CGRectMake(0,64,SelfViewWidth,SelfViewHeight);
    
    //TBLV HEADER
    self.viewtblvHeader.frame = CGRectMake(0,0,SelfViewWidth,95);
    self.viewtblvHeader.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    
    //PROFILE IMAGEVIEW
    self.imgViewProfile.frame = CGRectMake(SelfViewWidth/2 - 20,02,40,40);
    self.imgViewProfile.layer.cornerRadius = self.imgViewProfile.frame.size.height /2;
    self.imgViewProfile.layer.masksToBounds = YES;
    self.imgViewProfile.layer.borderWidth = 1.0;
    self.imgViewProfile.layer.borderColor = [UIColor  whiteColor].CGColor;
    
    
    //CUSTOMER TYPE
    self.lblCustomerType.frame = CGRectMake(10,self.imgViewProfile.frame.origin.y+self.imgViewProfile.frame.size.height+01,SelfViewWidth-20,14);
    self.lblCustomerType.font = [UIFont  fontWithName:kFontSFUIText_LightItalic size:15];
    
    if (objglobalValues.strCustomerGroupId.intValue == 1) {
        self.lblCustomerType.text = @"Retailer";
    }
    else
    {
       // WHOLESALER
       self.lblCustomerType.text = @"Wholesaler";
    }
    
    
    

    //NAME
    self.lblName.frame = CGRectMake(10,self.lblCustomerType.frame.origin.y+self.lblCustomerType.frame.size.height,SelfViewWidth-20,18);
    self.lblName.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:15];
    
    //GMAIL LABEL
    self.lblgmail.frame = CGRectMake(10,self.lblName.frame.origin.y +self.lblName.frame.size.height,SelfViewWidth - 20,18);
    self.lblgmail.font = [UIFont  fontWithName:kFontSFUIDisplay_Light size:15];
    
    
    //INTIALIALLY HIDDEN
    self.viewtblvHeader.hidden = YES;
    
    
    [self cartBarButton];
    
}




-(void)cartBarButton
{
    
    MKNumberBadgeView *number = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(25, -10,20,20)];
    number.value = objglobalValues.strCartCount.integerValue;
    number.shadow = NO;
    number.shine = YES;
    number.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:14];
    number.strokeColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);;
    number.layer.cornerRadius = 2.0;
    
    
    
    
    UIImage * imgcart = [UIImage  imageNamed:@"cart"];
    // Allocate UIButton
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0,0,30,30);
    [btn setImage:imgcart forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:number]; //Add NKNumberBadgeView as a subview on UIButton
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
    
}



-(IBAction)cartButtonClicked:(id)sender
{
    
    if (objglobalValues.strCartCount.integerValue == 0) {
        [TSMessage  showNotificationInViewController:self title:@"Cart" subtitle:@"No Product Available in Cart" type:TSMessageNotificationTypeMessage];
    }
    else
    {
        CartViewController * objCartVC = [self.storyboard   instantiateViewControllerWithIdentifier:@"CartViewController"];
        [self.navigationController pushViewController:objCartVC animated:YES];
        
    }
    }



#pragma mark
#pragma mark webserviceCallForProfile
#pragma mark
-(void)webserviceCallForProfile
{
    if ([Reachability isConnected]) {
        [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
        NSDictionary * dict = @{@"action":@"getprofiledetails",@"user_id":objglobalValues.strCustomerId};
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
            
            [DejalBezelActivityView  removeViewAnimated:YES];
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage showNotificationWithTitle:@"Success" subtitle:strmsg type:TSMessageNotificationTypeSuccess];
                    NSDictionary * dict = [result  objectForKey:@"Result"];
                    
                    //Full Name
                    NSString * strname = [NSString  stringWithFormat:@"%@ %@",[dict  objectForKey:@"first_name"],[dict  objectForKey:@"last_name"]];
                    self.lblName.text = strname;
                    
                    //Email
                    NSString * stremail = [NSString  stringWithFormat:@"%@",[dict  objectForKey:@"email"]];
                    self.lblgmail.text = stremail;
                    
                    
                    //TBLV HEADER
                    self.viewtblvHeader.hidden = NO;
                }
                else
                {
                    
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage showNotificationWithTitle:@"Error"
                                                subtitle:strmsg
                                                    type:TSMessageNotificationTypeError];
                    
                }
            }
        }];
        
        
    }
    
}


/*
{
    Result =     {
        "customer_group_id" = 1;
        "customer_id" = 70;
        email = "dharmenderk@nishkrant.com";
        "first_name" = sahil;
        "last_name" = "N Media";
        loginname = test12;
    };
    msg = "User Profile Details";
    status = 200;
    statusCode = 1;
}
*/







#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return marrAboutUsOptions.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    
    static NSString *cellIdentifier = @"Cell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        
        //Selected Cell bg
        UIView * viewcellbg = [[UIView alloc] init];
        viewcellbg.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
        cell.selectedBackgroundView = viewcellbg;
    }
    
    
    
    //Text label
    cell.textLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:18];
    cell.textLabel.textColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    

    //Accessory Type
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    //SetText
    cell.textLabel.text = [marrAboutUsOptions   objectAtIndex:indexPath.section];
    
    return cell;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger intReturn;
    intReturn = 40;
    return intReturn;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].textLabel.textColor = [UIColor whiteColor];
  //  [tableView  deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section==0) {
        [self  performSegueWithIdentifier:ksegueEditProfile sender:nil];
    }
    else if(indexPath.section==1)
    {
        
        ChangePasswordViewController * objChangePassword = [[ChangePasswordViewController alloc] initWithNibName:@"ChangePasswordViewController" bundle:nil];
        [self.navigationController pushViewController:objChangePassword animated:YES];
        
    }
    else if (indexPath.section==2)
    {
        
        [self   performSegueWithIdentifier:ksegue_wishlist sender:nil];
        
        
    }
    else if (indexPath.section==3)
    {
        
        MyOrderViewController *myOrder = [self.storyboard   instantiateViewControllerWithIdentifier:@"MyOrderViewController"];
        [self.navigationController pushViewController:myOrder animated:YES];
        
    }
    
    else if (indexPath.section==4)
    {
        InviteViewController * objInviteVC = [[InviteViewController alloc] initWithNibName:@"InviteViewController" bundle:nil];
         [self.navigationController pushViewController:objInviteVC animated:YES];
        
    }
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
  return [marrAboutUsOptions  objectAtIndex:section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}



- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].textLabel.textColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
}

















@end
