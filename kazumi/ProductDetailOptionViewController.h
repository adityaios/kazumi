//
//  ProductDetailOptionViewController.h
//  kazumi
//
//  Created by Yashvir on 05/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Network.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "TSMessage.h"
#import "KazumiMacros.h"


@interface ProductDetailOptionViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSInteger intSectionCount;
    NSArray * arrOptionValue;
    NSIndexPath * lastIndexPath;

}


-(void)customBackButton;
-(void)setUI;
-(void)intializerMethod;



@property (strong, nonatomic) NSString * strOption_name;
@property (strong, nonatomic) NSString * strOption_id;
@property (strong, nonatomic) NSString * strOpt_Val_ID;


@property (weak, nonatomic) IBOutlet UITableView *tblvProductDetail;
@property (nonatomic ,strong) NSDictionary * dictOptionPass;
@end
