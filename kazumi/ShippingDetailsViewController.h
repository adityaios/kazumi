//
//  ShippingDetailsViewController.h
//  kazumi
//
//  Created by Yashvir on 15/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "DejalActivityView.h"
#import "GlobalValues.h"
#import "Network.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "TSMessage.h"
#import "KazumiMacros.h"
#import "Location.h"
#import "WebserviceConstant.h"

@interface ShippingDetailsViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{
    GlobalValues * objglobalValues;
    NSMutableArray * marrCountry;
    NSMutableArray * marrZone;
    NSString * strAddressId;
    
    Location * objlocSelectedCountry;
    Location * objlocSelectedZone;
    
}
@property (weak, nonatomic) IBOutlet UIScrollView *bgScrollView;

@property (weak, nonatomic) IBOutlet UITextField *txfdFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txfdLastName;
@property (weak, nonatomic) IBOutlet UITextField *txfdContactNo;
@property (weak, nonatomic) IBOutlet UITextField *txfdEmail;

@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectCountry;
@property (weak, nonatomic) IBOutlet UITextField *txfdcountry;

@property (weak, nonatomic) IBOutlet UILabel *lblState;
@property (weak, nonatomic) IBOutlet UITextField *txfdState;

@property (weak, nonatomic) IBOutlet UITextField *txfdadd1;
@property (weak, nonatomic) IBOutlet UITextField *txfdAdd2;
@property (weak, nonatomic) IBOutlet UITextField *txfdCity;
@property (weak, nonatomic) IBOutlet UITextField *txfdPostCode;
@property (weak, nonatomic) IBOutlet UIButton *btnProceedToPayment;



- (IBAction)btnProceedToPayment:(UIButton *)sender;



//METHODS
-(void)viewSetUP;
-(void)intializerMethod;
-(void)customBackButton;
-(void)webserviceCallForAddressDetails;
-(void)textfieldToolBar:(UITextField *)txfd;
-(void)pickerViewAsInputView:(UITextField*)txfd;




@end
