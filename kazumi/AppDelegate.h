//
//  AppDelegate.h
//  kazumi
//
//  Created by Yashvir on 14/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "Constant.h"
#import "KazumiMacros.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;

-(void)customNavigationBar;
-(void)getAllFontnames;
@end

