//
//  FilterOptionsViewController.m
//  kazumi
//
//  Created by Yashvir on 17/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//




#import "FilterOptionsViewController.h"

@interface FilterOptionsViewController ()

@end

@implementation FilterOptionsViewController



#pragma mark
#pragma mark VIEW CONTROLLER LIFE CYCLE
#pragma mark
- (void)viewDidLoad {
    [super viewDidLoad];
    [self  viewSetUP];
    [self  intializerMethod];
    [self  customBackButton];
    
    if ([Reachability  isConnected]) {
        [self  webserviceCallForFilterOptions];
    }
    
}


#pragma mark
#pragma mark INTIALIZER
#pragma mark
-(void)intializerMethod
{
    objGlobalValues = [GlobalValues  sharedManager];
    marrSelectedRows = [NSMutableArray  new];
    self.title = @"Product Filter";
    
    
}


#pragma mark
#pragma mark VIEW UI
#pragma mark
-(void)viewSetUP
{
    
    int XPadding = 4;
    int Ypadding = 10;
    
    //Table View
    self.tblvOption.frame = CGRectMake(0, 0,SelfViewWidth,SelfViewHeight);
    
    //TBLV HEADER
    self.tblvHeader.frame = CGRectMake(0, 0,SelfViewWidth,60);
    self.lblPricehead.frame = CGRectMake(XPadding*4,Ypadding,SelfViewWidth-8*XPadding,20);
    self.sliderPrice.frame = CGRectMake(XPadding,Ypadding+30,SelfViewWidth-2*XPadding,31);
    self.lblMax.frame = CGRectMake(SelfViewWidth-120,Ypadding,120,17);
    
    //Find Bar Button
    
    UIImage * imgFind = [UIImage  imageNamed:@"find"];
    UIButton *btnFilter = [UIButton  buttonWithType:UIButtonTypeCustom];
    btnFilter.frame = CGRectMake(0,0,40,40);
    //    btnFilter.backgroundColor = UIColorFromRGBWithAlpha(kazumiYellowColor,1.0);
    //    btnFilter.layer.borderWidth = 2.0;
    //    btnFilter.layer.borderColor = [UIColor  orangeColor].CGColor;
    //    btnFilter.layer.cornerRadius = 2.0;
    [btnFilter addTarget:self action:@selector(filterButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    btnFilter.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
    [btnFilter  setImage:imgFind forState:UIControlStateNormal];
    
    
    
    UIBarButtonItem * barbtnFilter = [[UIBarButtonItem alloc] initWithCustomView:btnFilter];
    self.navigationItem.rightBarButtonItem = barbtnFilter;
    
}




-(void)customizeSliderMin:(NSString *)min AndWithMax:(NSString *)max
{
    
    NSNumberFormatter *tempFormatter = [[NSNumberFormatter alloc] init];
    [tempFormatter  setPositivePrefix:@"Rs"];
    [self.sliderPrice setNumberFormatter:tempFormatter];
    
    // customize slider 1
    self.sliderPrice.maximumValue = max.floatValue;
    self.sliderPrice.minimumValue = min.floatValue;
    self.sliderPrice.value = max.floatValue;
    self.sliderPrice.popUpViewCornerRadius = 05;
    [self.sliderPrice setMaxFractionDigitsDisplayed:0];;
    self.sliderPrice.popUpViewColor =  UIColorFromRGB(kazumiRedColor);
    self.sliderPrice.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:15];
    // self.sliderMap.textColor = [UIColor colorWithHue:0.55 saturation:1.0 brightness:0.5 alpha:1];
    self.sliderPrice.textColor = [UIColor whiteColor];
    self.sliderPrice.popUpViewWidthPaddingFactor = 1.7;
    self.sliderPrice.tintColor = UIColorFromRGB(kazumiRedColor);
    
}



#pragma mark  custom Back Button
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}






#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark


#pragma mark number Of Sections

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger intreturn;
    if (arrOption.count == 0) {
        intreturn = 0;
    }
    else
    {
        intreturn = arrOption.count;
    }
    return intreturn;
}


#pragma mark number Of Rows In Section
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger intreturn;
    if (arrOption.count == 0) {
        intreturn = 0;
    }
    else
    {
        NSDictionary * dictOptions = [arrOption   objectAtIndex:section];
        NSArray * arrOptionValues = [dictOptions   objectForKey:@"options"];
        intreturn = arrOptionValues.count;
    }
    return intreturn;
}



#pragma mark cell For Row At IndexPath
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    
    static NSString *cellIdentifier = @"Cell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    
    //Check Mark
    if (marrSelectedRows.count == 0) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        if ([marrSelectedRows  containsObject:indexPath]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
    }
    
    
    
    NSDictionary * dictOptions = [arrOption   objectAtIndex:indexPath.section];
    NSArray * arrOptionValues = [dictOptions   objectForKey:@"options"];
    NSDictionary * dictOptionValue = [arrOptionValues   objectAtIndex:indexPath.row];
    NSString * strOptValue = [NSString   stringWithFormat:@"%@",[dictOptionValue  objectForKey:@"option_value"]];
    cell.textLabel.text =  strOptValue;
    return cell;
}




#pragma mark height For Row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger intReturn;
    intReturn = 40;
    return intReturn;
}


#pragma mark title For Header
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    NSDictionary * dictOptions = [arrOption   objectAtIndex:section];
    NSString * strtitle = [dictOptions  objectForKey:@"name"];
    return strtitle;
}



#pragma mark height For Header
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}


#pragma mark did Select Row
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([marrSelectedRows  containsObject:indexPath]) {
        [marrSelectedRows   removeObject:indexPath];
        NSLog(@"marrSelectedRows = %@",marrSelectedRows);
        [self.tblvOption  reloadData];
    }
    
    else
    {
        
        for (NSIndexPath * selectedIndexPath in marrSelectedRows) {
            if ( selectedIndexPath.section == indexPath.section ){
                [marrSelectedRows   removeObject:selectedIndexPath];
                break;
            }
        }
        
        [marrSelectedRows   addObject:indexPath];
        NSLog(@"marrSelectedRows = %@",marrSelectedRows);
        [self.tblvOption reloadData];
    }
}




#pragma mark did Deselect Row At IndexPath
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}






#pragma mark
#pragma mark IBACTION
#pragma mark

#pragma mark back btn clicked
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}





#pragma mark filter btn clicked
-(IBAction)filterButtonClicked:(id)sender
{
    /*
     optionfilter
     cat_id
     options		            attrbuteid-valuename,attrbuteid-valuename eg: 1-XL, 2-White
     customer_group_id
     min_price
     max_price
     */
    
    
    NSString * strOptWeb;
    
    if (marrSelectedRows.count == 0) {
        [TSMessage  showNotificationInViewController:self title:@"No option selected" subtitle:nil  type:TSMessageNotificationTypeMessage];
        
    }
    else
    {
        
        NSMutableString * strOpt = [[NSMutableString alloc]init];
        for (NSIndexPath * objIndexPath in marrSelectedRows) {
            NSDictionary * dictOptions = [arrOption   objectAtIndex:objIndexPath.section];
            NSString * strAttributeID = [NSString   stringWithFormat:@"%@",[dictOptions  objectForKey:@"attribute_id"]];
            
            NSArray * arrOptionValues = [dictOptions   objectForKey:@"options"];
            NSDictionary * dictOptionValue = [arrOptionValues   objectAtIndex:objIndexPath.row];
            NSString * strOptValue = [NSString   stringWithFormat:@"%@",[dictOptionValue  objectForKey:@"option_value"]];
            
            [strOpt appendString:@","];
            [strOpt appendString:[NSString  stringWithFormat:@"%@-%@",strAttributeID,strOptValue]];
            
            
        }
        
        strOptWeb  = strOpt;
        if ([strOptWeb hasPrefix:@","] && [strOptWeb length] > 1) {
            strOptWeb  = [strOpt substringFromIndex:1];
        }
    }
    
    
    
    
    //PARAMETERS
    NSMutableDictionary * mdict = [NSMutableDictionary   new];
    [mdict  setObject:@"optionfilter" forKey:@"action"];
    [mdict  setObject:self.objCatModelFilter.catId forKey:@"cat_id"];
    [mdict  setObject:objGlobalValues.strCustomerGroupId forKey:@"customer_group_id"];
    
    
    
    
    //Min
    [mdict  setObject:strmin forKey:@"min_price"];
    
    
    
    //Max
    strmax = [NSString   stringWithFormat:@"%d",(NSInteger)self.sliderPrice.value];
    [mdict  setObject:strmax forKey:@"max_price"];
    
    
    
    
    //OPT
    if (strOptWeb.length >1) {
        [mdict  setObject:strOptWeb forKey:@"options"];
    }
    NSLog(@"mdict = %@",mdict);
    
    
    if ([Reachability  isConnected]) {
        [self  webserviceCallToFindFilterProducts:mdict];
    }
    
    
}





#pragma mark
#pragma mark - WEBSERVICE CALL
#pragma mark


#pragma mark - webservice Call For Filter Options
-(void)webserviceCallForFilterOptions
{
    
    [DejalBezelActivityView  activityViewForView:self.view withLabel:@"Please Wait"];
    
    NSDictionary *dictPara = @{@"action" : @"getfilteroptions"};
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara    andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView   removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSLog(@"result = %@",result);
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                
                
                
                //Option Array
                arrOption = [NSArray   new];
                arrOption = [result  objectForKey:@"Result"];
                
                
                
                /*
                 //FOR DEBUGGing
                 NSString *filePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"Options" ofType:@"json"];
                 NSData *data = [NSData dataWithContentsOfFile:filePath];
                 
                 NSError *error = nil;
                 arrOption = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 if (error) {
                 NSLog(@"error = %@",error.localizedDescription);
                 }
                 else
                 {
                 NSLog(@"arrOption = %@",arrOption);
                 }
                 */
                
                
                //Min
                strmin = [NSString   stringWithFormat:@"%@",[result  objectForKey:@"min_price"]];
                NSString * strMinfloat= [NSString   stringWithFormat:@"%.02f",[[result  objectForKey:@"min_price"] floatValue]];
                
                
                //Max
                strmax = [NSString   stringWithFormat:@"%@",[result  objectForKey:@"max_price"]];
                NSString * strMaxfloat = [NSString   stringWithFormat:@"%.02f",[[result  objectForKey:@"max_price"] floatValue]];
                self.lblMax.text = [NSString   stringWithFormat:@"Max Rs %@",strMaxfloat];
                
                
                //Cutomize Slider
                [self   customizeSliderMin:strMinfloat AndWithMax:strMaxfloat];
                [self.tblvOption  reloadData];
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage showNotificationWithTitle:@"Error"
                                            subtitle:strmsg
                                                type:TSMessageNotificationTypeError];
                
            }
        }
        
    }];
    
}





#pragma mark webservice Call To Find Filter Products
-(void)webserviceCallToFindFilterProducts:(NSDictionary *)dict
{
    
    if ([Reachability isConnected]) {
        [DejalBezelActivityView  activityViewForView:self.view withLabel:@"Please Wait"];
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
            [DejalBezelActivityView   removeViewAnimated:YES];
            if (error) {
                NSLog(@"Error = %@",error);
            }
            else
            {
                //CHECK STATUS CODE
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    //SUCCESS MSG
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                    NSLog(@"result = %@",result);
                    
                    //JSON PARSING
                    NSArray * arrJsonResponse = [result  objectForKey:@"Result"];
                    marrFilterProducts = [NSMutableArray   new];
                    if (arrJsonResponse.count >= 1) {
                        for (NSDictionary * dict in arrJsonResponse) {
                            NSLog(@"dict = %@",dict);
                            ProductModelClass * objModelProduct = [ProductModelClass  new];
                            objModelProduct.strActual_price = [dict  objectForKey:@"actual_price"];
                            objModelProduct.strDiscount_type = [dict  objectForKey:@"discount_type"];
                            objModelProduct.strDiscount_value = [dict  objectForKey:@"discount_value"];
                            objModelProduct.strDiscounted_amount = [dict  objectForKey:@"discounted_amount"];
                            objModelProduct.strProduct_id = [dict  objectForKey:@"product_id"];
                            objModelProduct.strProduct_image = [dict  objectForKey:@"product_image"];
                            objModelProduct.strProduct_Name = [dict  objectForKey:@"product_name"];
                            [marrFilterProducts     addObject:objModelProduct];
                        }
                        
                        if (marrFilterProducts.count >= 1) {
                            //DELEGATE METHOD
                            if ([self.FilterDelegateObject respondsToSelector:@selector(sendFilteredProductsBackToProductListController:)]) {
                                [self.FilterDelegateObject  sendFilteredProductsBackToProductListController:marrFilterProducts];
                            }
                            
                            
                        }
                    }
                }
                else
                {
                    
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage showNotificationWithTitle:@"Error"
                                                subtitle:strmsg
                                                    type:TSMessageNotificationTypeError];
                }
            }
        }];
    }
}






#pragma mark
#pragma mark - SCROLLVIEW DELEGATE
#pragma mark

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view  endEditing:YES];
}





@end
