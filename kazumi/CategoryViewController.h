//
//  CategoryViewController.h
//  kazumi
//
//  Created by Yashvir on 14/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Network.h"
#import "WebserviceConstant.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "Constant.h"
#import "CatModel.h"
#import "KazumiMacros.h"
#import "ProductListViewController.h"
#import "MKNumberBadgeView.h"
#import "CartViewController.h"



@interface CategoryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
   NSMutableArray * marrCatModelData;
   GlobalValues * objglobalValues;

}
@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButtonCat;

@property (weak, nonatomic) IBOutlet UITableView *tblvCatList;




//METHODS
-(void)getAllCategoriesList;

@end
