//
//  MainMenuTableViewController.h
//  kazumi
//
//  Created by Yashvir on 14/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Constant.h"
#import "KazumiMacros.h"
#import "GlobalValues.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface MainMenuTableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray * marr_menu;
    NSMutableArray * marr_menuImg;
    GlobalValues * objGlobalValues;
}

-(void)setUpUI;
-(void)intializerMethod;
-(void)logOutGoogleLogin;
-(void)logOutFaceBookLogin;



@property (weak, nonatomic) IBOutlet UILabel *lblKazumi;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewKazumi;
@property (weak, nonatomic) IBOutlet UIView *tblvHeader;







@end
