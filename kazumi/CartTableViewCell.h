//
//  CartTableViewCell.h
//  kazumi
//
//  Created by Yashvir on 10/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalValues.h"
#import "KazumiMacros.h"
#import "Constant.h"

@interface CartTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *imgViewProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel * lblOptions;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet UIStepper *SteperQuant;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdateQuantity;








@end
