//
//  PaymentMethodTableViewController.h
//  kazumi
//
//  Created by Yashvir on 19/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KazumiMacros.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "DejalActivityView.h"
#import "Network.h"
#import "GlobalValues.h"
#import "TSMessage.h"
#import "Reachability.h"

@interface PaymentMethodTableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>
{
    GlobalValues * objGlobalValues;
    NSString * strType;
}

@property(strong,nonatomic) NSDictionary* dictOrderPass;
@property(strong) NSIndexPath* lastIndexPath;
@property (strong, nonatomic) IBOutlet UIButton *btnPlaceOrder;


- (IBAction)btnPlaceOrderClicked:(UIButton *)sender;


-(void)customBackButton;
-(void)intializerMethod;
-(void)webserviceCallForPlaceOrder:(NSDictionary*)dict;

@end
