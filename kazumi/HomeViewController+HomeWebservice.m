//
//  HomeViewController+HomeWebservice.m
//  kazumi
//
//  Created by Yashvir on 19/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "HomeViewController+HomeWebservice.h"

@implementation HomeViewController (HomeWebservice)


-(void)getCategoryListForHome
{
    if ([Reachability isConnected]) {
        //WEBSERVICE CALL
        [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
        NSDictionary *dict = @{@"action" : @"gethomecategories"};
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
            if (error) {
                [DejalBezelActivityView  removeViewAnimated:YES];
                NSLog(@"%@",error);
            }
            else
            {
                [DejalBezelActivityView  removeViewAnimated:YES];
                NSLog(@"%@",result);
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    marrCatModelData = [[NSMutableArray  alloc] init];
                    for (NSDictionary * dict in [result  objectForKey:@"Result"]) {
                        CatModel * objCatModel = [CatModel  new];
                        objCatModel.catId = [dict  objectForKey:@"cat_id"];
                        objCatModel.catImgUrl = [dict  objectForKey:@"category_image"];
                        objCatModel.catName = [dict  objectForKey:@"category_name"];
                        [marrCatModelData   addObject:objCatModel];
                    }
                    [self.tblvHomeCat   reloadData];
                }
                else
                {
                    
                }
             }
        }];
     }
    else
    {
        [TSMessage showNotificationWithTitle:kAlertNoNetworkTitle
                                    subtitle:kAlertNoNetworkMsg
                                        type:TSMessageNotificationTypeError];

    }
}



















@end
