//
//  RegistrationActivationViewController.m
//  kazumi
//
//  Created by Yashvir on 14/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "RegistrationActivationViewController.h"

@interface RegistrationActivationViewController ()

@end

@implementation RegistrationActivationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  customBackButton];
    [self  viewSetUP];
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.txfdActivation    becomeFirstResponder];
}


#pragma mark
#pragma mark  viewSetUP
#pragma mark
-(void)viewSetUP
{
    self.title = @"Registration Activation";
    
    
    
    int Xpadding = 10;
    int Y = 64;
    int Ypadding = 8;
    
    //BG IMAGE VIEW
    self.bgImageView.frame = CGRectMake(0,Y,SelfViewWidth,SelfViewHeight);
    
    Y = Y+ Ypadding;
    
    //LABEL
    self.lblActivation.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding,45);
    
    Y= Y+self.lblActivation.frame.size.height+Ypadding;
    
    //TEXTFIELD
    self.txfdActivation.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding,40);
    UIView * paddingViewEmail = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewEmail.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewEmail.backgroundColor = [UIColor   whiteColor];

    self.txfdActivation.leftView = paddingViewEmail;
    self.txfdActivation.leftViewMode = UITextFieldViewModeAlways;
    
    Y= Y+self.txfdActivation.frame.size.height+Ypadding*2;
    
    
    self.btnSubmit.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2* Xpadding, 40);
    //self.btnSubmit.layer.borderWidth = 2.0;
    self.btnSubmit.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    //self.btnSubmit.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnSubmit.layer.cornerRadius = kButtonCornerRadius;
    self.btnSubmit.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
    
    
}



#pragma mark
#pragma mark  customBackButton
#pragma mark
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}


- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark
#pragma mark  TEXTFILED DELEGATE
#pragma mark
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField   resignFirstResponder];
    return YES;
}


#pragma mark
#pragma mark  IBACTION
#pragma mark

- (IBAction)btnsubmitclicked:(UIButton *)sender {
    self.txfdActivation.text = [self.txfdActivation.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    while ([self.txfdActivation.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdActivation.text = [self.txfdActivation.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    if (self.txfdActivation.text.length == 0 ) {
        [TSMessage  showNotificationInViewController:self title:@"Please enter Activation Code" subtitle:nil type:TSMessageNotificationTypeError];
        return;
    }
    
    if ([Reachability  isConnected]) {
        [self   webserviceCallForRegistrationActivationCode];
    }
    
}


-(void)webserviceCallForRegistrationActivationCode
{
    //verifyaccount	activation_code
    //WEBSERVICE CALL
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary *dict = @{@"action" : @"verifyaccount",@"activation_code" : self.txfdActivation.text};
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            NSLog(@"%@",result);
            
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Registeration Activation Suceesfull" subtitle:strmsg type:TSMessageNotificationTypeSuccess];
                
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Registeration Activation Failed" subtitle:strmsg type:TSMessageNotificationTypeError];
            }
            [self.navigationController  popToRootViewControllerAnimated:YES];
        }
        
    }
     ];
    
}








@end
