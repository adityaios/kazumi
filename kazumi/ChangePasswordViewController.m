//
//  ChangePasswordViewController.m
//  kazumi
//
//  Created by NM8 on 02/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *newpass;
@property (weak, nonatomic) IBOutlet UITextField *oldpass;
@property (weak, nonatomic) IBOutlet UITextField *cnfmpass;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self  setUpUI];
    [self  intializerMethod];
    [self  CustomBackButton];
    
}


#pragma mark
#pragma mark setUpUI
#pragma mark
-(void)setUpUI
{
    
    
    self.oldpass.layer.borderColor = UIColor.grayColor.CGColor;
    self.oldpass.layer.borderWidth = 0.3;
    self.oldpass.layer.masksToBounds = true;
    UIView * paddingViewoldpass = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewoldpass.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.oldpass.leftView = paddingViewoldpass;
    self.oldpass.secureTextEntry = YES;

    self.oldpass.leftViewMode = UITextFieldViewModeAlways;
    
    

    self.newpass.layer.borderColor = UIColor.grayColor.CGColor;
    self.newpass.layer.borderWidth = 0.3;
    self.newpass.layer.masksToBounds = true;
    self.newpass.secureTextEntry = YES;

    UIView * paddingViewnewpass = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewnewpass.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.newpass.leftView = paddingViewnewpass;
    self.newpass.leftViewMode = UITextFieldViewModeAlways;
    
    
    self.cnfmpass.layer.borderColor = UIColor.grayColor.CGColor;
    self.cnfmpass.layer.borderWidth = 0.3;
     self.cnfmpass.secureTextEntry = YES;
    self.cnfmpass.layer.masksToBounds = true;
    UIView * paddingViewcnfmpass = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewcnfmpass.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.cnfmpass.leftView = paddingViewcnfmpass;
    self.cnfmpass.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    //self.btnSubmit.layer.borderWidth = 2.0;
    self.btnSubmit.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    //self.btnSubmit.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnSubmit.layer.cornerRadius = kButtonCornerRadius;
    self.btnSubmit.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Bold size:17];
}


#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    self.title =@"Change Password";
    objglobalValues=[GlobalValues sharedManager];
    
    
    
}



#pragma mark
#pragma mark CustomBackButton
#pragma mark
-(void)CustomBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    
}


/*
 changepassword
 user_id
 old_pwd
 new_pwd
 */

- (IBAction)btnSubmitClicked:(UIButton *)sender {
    
    self.oldpass.text = [self.oldpass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.newpass.text = [self.newpass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.cnfmpass.text = [self.cnfmpass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    
    //VALIDATIONS DOUBLE SPACE
    //OLD PASSWORD
    while ([self.oldpass.text rangeOfString:@"  "].location != NSNotFound) {
        self.oldpass.text = [self.oldpass.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    
    //NEW PASSWORD
    while ([self.newpass.text rangeOfString:@"  "].location != NSNotFound) {
        self.newpass.text = [self.newpass.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    
    //CONFIRM PASSWORD
    while ([self.cnfmpass.text rangeOfString:@"  "].location != NSNotFound) {
        self.cnfmpass.text = [self.cnfmpass.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    if (self.oldpass.text.length == 0 || self.newpass.text.length == 0 || self.cnfmpass.text.length == 0 ) {
        [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please Fill all Fields"  type:TSMessageNotificationTypeError];
        return;
        
    }
    
    
    //PSWD LENGTH
    if (self.newpass.text.length <=5) {
        [TSMessage  showNotificationInViewController:self title:@"Password Error" subtitle:@"Please enter password greater then 5 characters"  type:TSMessageNotificationTypeError];
        return;
    }
    
    
    //CONFIRM PASSWORD
    if (![self.newpass.text  isEqualToString:self.cnfmpass.text]) {
        [TSMessage  showNotificationInViewController:self title:@"Password Error" subtitle:@"Confirm Password and New Password is not same"  type:TSMessageNotificationTypeError];
        return;
    }
    
    if ([Reachability isConnected]) {
        [self  webserviceCallForchangepassword];
    }
}





#pragma mark
#pragma mark webserviceCallForchangepassword
#pragma mark
-(void)webserviceCallForchangepassword
{
    
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary * dict = @{@"action":@"changepassword",@"user_id":objglobalValues.strCustomerId,@"old_pwd":self.oldpass.text,@"new_pwd":self.newpass.text};
    NSLog(@"dict %@",dict);
    
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Change Password" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Change Password" subtitle:strmsg  type:TSMessageNotificationTypeError];
            }
        }
    }];
}





-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField  resignFirstResponder];
    return YES;
}





@end
