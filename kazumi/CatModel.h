//
//  CatModel.h
//  kazumi
//
//  Created by Yashvir on 19/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CatModel : NSObject
@property(nonatomic,strong) NSString * catId;
@property(nonatomic,strong) NSString * catImgUrl;
@property(nonatomic,strong) NSString * catName;
@property(nonatomic,strong) NSString * strsubcount;

@end
