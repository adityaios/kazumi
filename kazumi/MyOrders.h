//
//  MyOrders.h
//  kazumi
//
//  Created by Yashvir on 02/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyOrders : NSObject

@property(nonatomic,strong) NSString  * strOrderID;
@property(nonatomic,strong) NSString  * strOrderDate;
@property(nonatomic,strong) NSString  * strOrderStatus;
@property(nonatomic,strong) NSString  * strOrderTotal;

@end
