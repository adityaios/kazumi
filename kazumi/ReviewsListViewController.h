//
//  ReviewsListViewController.h
//  kazumi
//
//  Created by Yashvir on 08/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "DejalActivityView.h"
#import "GlobalValues.h"
#import "Network.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "TSMessage.h"
#import "KazumiMacros.h"
#import "Reachability.h"
#import "ProductModelClass.h"
#import "ReviewTableViewCell.h"

@interface ReviewsListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * marrReviewData;
    
}
@property(nonatomic,strong) ProductModelClass * objProductModeleReview;
@property (weak, nonatomic) IBOutlet UITableView *tblvReview;


-(void)customBackButton;
-(void)viewSetUp;
-(void)intializerMethod;
-(void)webserviceCallForProductReviews;

@end
