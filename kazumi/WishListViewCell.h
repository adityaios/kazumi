//
//  WishListViewCell.h
//  kazumi
//
//  Created by NM8 on 24/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WishListViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblDisAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblDiscount;
@property (strong, nonatomic) IBOutlet UILabel *lblActualPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblProName;
@property (strong, nonatomic) IBOutlet UIImageView *imgProduct;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;





@end
