//
//  ProductDetailsViewController.m
//  kazumi
//
//  Created by Yashvir on 27/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "ProductDetailsViewController.h"

@interface ProductDetailsViewController ()

@end

@implementation ProductDetailsViewController

#define Ksection0HeaderViewHeight 200



#pragma mark
#pragma mark VIEW CONTROLLER LIFE CYCLE
#pragma mark

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  intializerMethod];
    [self  setUpUI];
    
    
    //WEBSERVICE CALL FOR PRODUCTS
    [self  webserviceCallForProductsDetails];
}



#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    objglobalValues = [GlobalValues  sharedManager];
    self.title = self.objProductModelPass.strProduct_Name;
    
    
}


#pragma mark
#pragma mark setUpUI
#pragma mark
-(void)setUpUI
{
    //BACK BUTTON
    [self   customBackButton];
    
    int  Xpadding = 8;
    
    //BTNS ADD TO WISHLIST
    self.btnAddToWishlist.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor,1.0);
    self.btnAddToWishlist.frame = CGRectMake(Xpadding,70,120, 40);
   // self.btnAddToWishlist.layer.borderWidth = 2.0;
    //self.btnAddToWishlist.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnAddToWishlist.layer.cornerRadius = 2.0;
    
    
    NSLog(@"SCREEN_WIDTH = %f",SCREEN_WIDTH);
    
    //BTNS ADD TO CART
    self.btnAddToCart.frame = CGRectMake(SelfViewWidth-Xpadding-120,70,120,40);
    self.btnAddToCart.backgroundColor = UIColorFromRGBWithAlpha(kazumiYellow1Color,1.0);
    //self.btnAddToCart.layer.borderWidth = 2.0;
    //self.btnAddToCart.layer.borderColor = [UIColor  orangeColor].CGColor;
    self.btnAddToCart.layer.cornerRadius = 2.0;
    
    
    //TABLEVIEW PRODUCT DETAILS
    int Ytblv = self.btnAddToWishlist.frame.origin.y+self.btnAddToWishlist.frame.size.height+05;
    self.tblvProductDetails.frame = CGRectMake(0,Ytblv ,SelfViewWidth,SelfViewHeight - Ytblv);
    self.headerView.frame = CGRectMake(0, 0, SelfViewWidth,SelfViewHeight/2);
    
    //TABLEVIEW PRODUCT IMAGE PAGER
    self.viewProductImgPager.frame = CGRectMake(16,0, SelfViewWidth-32, self.headerView.frame.size.height - 75);
    
    // [self  setImageScrollingView];
    
    //Product Name
    self.lblProductName.frame = CGRectMake(16, self.viewProductImgPager.frame.origin.y+self.viewProductImgPager.frame.size.height+02, self.viewProductImgPager.frame.size.width, 21);
    
    //Product Count
    self.lblProductCount.frame = CGRectMake(self.viewProductImgPager.frame.origin.x,self.lblProductName.frame.origin.y+self.lblProductName.frame.size.height+8,50, 21);
    
    //COUNTER
    self.stepperCount.frame = CGRectMake(self.lblProductCount.frame.origin.x +self.lblProductCount.frame.size.width+05 ,self.lblProductCount.frame.origin.y-4,94, 29);
    self.stepperCount.tintColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);;
    
    
    
    /*
     actual_price" = "1445.0000";
     "discount_type" = "Rs.";
     "discount_value" = "100.00";
     "discounted_amount" = "1345.00";
     */
    
    /*
     "actual_price" = "845.0000";
     "discount_type" = "No discount type applied";
     "discount_value" = "0.00";
     "discounted_amount" = "0.00";
     */
    
    //Price
    self.lblPrice.frame = CGRectMake(self.viewProductImgPager.frame.origin.x +self.viewProductImgPager.frame.size.width - 150 ,self.lblProductCount.frame.origin.y-05,150,16);
    
    //Discount Value
    self.lbldiscountValue.frame = CGRectMake(self.viewProductImgPager.frame.origin.x +self.viewProductImgPager.frame.size.width - 150 ,self.lblPrice.frame.origin.y+self.lblPrice.frame.size.height,150,16);
    
    
    //Actual Value(Discounted value)
    self.lbldiscountedValue.frame = CGRectMake(self.viewProductImgPager.frame.origin.x +self.viewProductImgPager.frame.size.width - 150 ,self.lbldiscountValue.frame.origin.y+self.lbldiscountValue.frame.size.height,150,16);
    
    
    
    //HIDE BUTTONS
    self.btnAddToCart.hidden = YES;
    self.btnAddToWishlist.hidden = YES;
    self.stepperCount.hidden = YES;
    
    
    [self cartBarButton];
    
    
    
}


-(void)cartBarButton
{
    
    
    MKNumberBadgeView *number = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(25, -10,20,20)];
    number.value = objglobalValues.strCartCount.integerValue;
    number.shadow = NO;
    number.shine = YES;
    number.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:14];
    number.strokeColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);;
    number.layer.cornerRadius = 2.0;
    
    
    UIImage * imgcart = [UIImage  imageNamed:@"cart"];
    // Allocate UIButton
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0,0,30,30);
    [btn setImage:imgcart forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:number]; //Add NKNumberBadgeView as a subview on UIButton
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}



-(IBAction)cartButtonClicked:(id)sender
{
    if (objglobalValues.strCartCount.integerValue == 0) {
        [TSMessage  showNotificationInViewController:self title:@"Cart" subtitle:@"No Product Available in Cart" type:TSMessageNotificationTypeMessage];
    }
    else
    {
        CartViewController * objCartVC = [self.storyboard   instantiateViewControllerWithIdentifier:@"CartViewController"];
        [self.navigationController pushViewController:objCartVC animated:YES];
        
    }
}





#pragma mark
#pragma mark  customBackButton
#pragma mark

-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}

- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}




#pragma mark
#pragma mark - webserviceCallForProductsDetails
#pragma mark
-(void)webserviceCallForProductsDetails
{
    
    NSString * strProductid = self.objProductModelPass.strProduct_id;
    NSString * strProductName = self.objProductModelPass.strProduct_Name;
    NSLog(@"strProductid = %@",strProductid);
    NSLog(@"strProductName = %@",strProductName);
    
    
    if ([Reachability isConnected]) {
        //LOADER
        [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
        
        //PARAMETER DICT
        NSDictionary *dictPara = @{@"action" : @"getproductdetails",@"product_id":strProductid,@"customer_group_id":objglobalValues.strCustomerGroupId};
        NSLog(@"dictPara = %@",dictPara);
        
        
        //NETWORK REQUEST
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
            
            //REMOVE LOADER
            [DejalBezelActivityView  removeViewAnimated:YES];
            
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                //CHECK STATUS CODE
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    NSLog(@" %@",result);
                    //SUCESS MESSAGE
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Product Details" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
                    
                    
                    //PRODUCT DICTIONARY
                    dictProductDetail = [NSDictionary new];
                    dictProductDetail = [[result objectForKey:@"Result"] objectAtIndex:0];
                    NSLog(@"dictProductDetail %@",dictProductDetail);
                    
                    
                    //SET TEXT ON PRODUCT DETAIL SCREEN
                    [self  setTextOnProductDetailScreen:dictProductDetail];
                    
                    
                    //ARRAY OPTIONS
                    arrOptions = [NSArray  new];
                    arrOptions = [result  objectForKey:@"options"];
                    NSLog(@"arrOptions %@",arrOptions);
                    
                    //FOR OPTION VALUES
                    mdictOptionValuesPass = [[NSMutableDictionary  alloc] initWithCapacity:arrOptions.count];
                    
                    
                    //ARRAY PRODUCT IMAGES
                    arrProductImages = [NSArray  new];
                    //NUL VALUE CHECK
                    arrProductImages = [result objectForKey:@"product_images"];
                    NSLog(@"arrProductImages %@",arrProductImages);
                    
                    
                    
                    //UNHIDE BUTTONS
                    self.btnAddToCart.hidden = NO;
                    self.btnAddToWishlist.hidden = NO;
                    self.stepperCount.hidden = NO;
                    
                    
                    
                    //IMAGES ON SCROLLVIEW
                    [self  setImageScrollingView:arrProductImages];
                    [self.tblvProductDetails  reloadData];
                    
                    
                }
                else
                {
                    //ERROR MESSAGE
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Product Details" subtitle:strmsg  type:TSMessageNotificationTypeError];
                    
                }
            }
            
        }];
    }
}





#pragma mark
#pragma mark setImageScrollingView
#pragma mark
-(void)setImageScrollingView:(NSArray *)arrProduct
{
    UIScrollView *scr=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,self.viewProductImgPager.frame.size.width, self.viewProductImgPager.frame.size.height-20)];
    scr.tag = 1;
    scr.autoresizingMask=UIViewAutoresizingNone;
    [self.viewProductImgPager addSubview:scr];
    
    
    [self setupScrollView:scr];
    
    UIPageControl *pgCtr = [[UIPageControl alloc] initWithFrame:CGRectMake(0,self.viewProductImgPager.frame.origin.y +self.viewProductImgPager.frame.size.height - 20,self.viewProductImgPager.frame.size.width,20)];
    [pgCtr setTag:12];
    pgCtr.numberOfPages=arrProduct.count;
    
    pgCtr.pageIndicatorTintColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    pgCtr.currentPageIndicatorTintColor = [UIColor  whiteColor];
    
    pgCtr.autoresizingMask=UIViewAutoresizingNone;
    [self.viewProductImgPager addSubview:pgCtr];
    
}

#pragma mark
#pragma mark setupScrollView
#pragma mark
- (void)setupScrollView:(UIScrollView*)scrMain {
    
    if (arrProductImages.count !=0) {
        for (int i=1;i<=arrProductImages.count;i++) {
            NSDictionary * dict = [arrProductImages  objectAtIndex:i - 1];
            NSLog(@"dict = %@",dict);
            NSString * strthumbnailImg = [NSString  stringWithFormat:@"%@",[dict  objectForKey:@"small_image"]];
            NSLog(@"strthumbnailImg = %@",strthumbnailImg);
            
            
            UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((i-1)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height)];
            imgV.layer.cornerRadius = 5.0;
            imgV.layer.masksToBounds = YES;
            
            
            imgV.contentMode=UIViewContentModeScaleAspectFit;
            [imgV setImageWithURL:[NSURL  URLWithString:strthumbnailImg] placeholderImage:[UIImage  imageNamed:@"placeholder"]];
            
            imgV.tag=i;
            [scrMain addSubview:imgV];
            
            
            //TAP GESTURE ON IMAGEVIEW
            UITapGestureRecognizer * imgViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgViewTapGesture:)];
            imgViewTap.numberOfTapsRequired = 1;
            [imgV addGestureRecognizer:imgViewTap];
            imgV.userInteractionEnabled = YES;
            
        }
        [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width*arrProductImages.count, scrMain.frame.size.height)];
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
    }
}



#pragma mark
#pragma mark scrollingTimer
#pragma mark

- (void)scrollingTimer {
    UIScrollView *scrMain = (UIScrollView*) [self.viewProductImgPager viewWithTag:1];
    UIPageControl *pgCtr = (UIPageControl*) [self.viewProductImgPager viewWithTag:12];
    CGFloat contentOffset = scrMain.contentOffset.x;
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    if( nextPage!=arrProductImages.count )  {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=nextPage;
    } else {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=0;
    }
}


- (void) imgViewTapGesture:(UITapGestureRecognizer*)sender
{
    UIImageView * imgview = (UIImageView*)sender.view;
    NSLog(@"%d",imgview.tag);
    //UIImage * img = imgview.image;
    
    
    NSDictionary * dictImg = [arrProductImages objectAtIndex:imgview.tag - 1];
    NSString * strimgUrl = [dictImg   objectForKey:@"original_image"];
    
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    //imageInfo.image = img;
    
    imageInfo.imageURL = [NSURL  URLWithString:strimgUrl];
    
    imageInfo.referenceRect = imgview.frame;
    imageInfo.referenceView = imgview.superview;
    imageInfo.referenceContentMode = imgview.contentMode;
    imageInfo.referenceCornerRadius = imgview.layer.cornerRadius;
    
    // Setup view controller
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode:JTSImageViewControllerMode_Image
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
    
    // Present the view controller.
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
    
}



#pragma mark
#pragma mark setTextOnProductDetailScreen
#pragma mark

-(void)setTextOnProductDetailScreen:(NSDictionary *)dict
{
    
    //PRODUCT NAME
    self.lblProductName.text = [NSString  stringWithFormat:@"%@",[dict  objectForKey:@"product_name"]];
    
    
    //ACTUAL PRICE
    NSString  * strPrice = [NSString  stringWithFormat:@"Rs %.02f",[[dict  objectForKey:@"actual_price"] floatValue]];
    self.lblPrice.text = strPrice;
    
    
    
    /*
     actual_price" = "1445.0000";
     "discount_type" = "Rs.";
     "discount_value" = "100.00";
     "discounted_amount" = "1345.00";
     */
    
    /*
     "actual_price" = "845.0000";
     "discount_type" = "No discount type applied";
     "discount_value" = "0.00";
     "discounted_amount" = "0.00";
     */
    
    //DISCOUNT PRICE
    NSString * strdiscount_type = [NSString  stringWithFormat:@"%@",[dict  objectForKey:@"discount_type"]];
    
    if( [strdiscount_type caseInsensitiveCompare:kNoDiscountTypeApplied] == NSOrderedSame ) {
        self.lbldiscountValue.hidden = YES;
        self.lbldiscountedValue.hidden = YES;
    }
    
    
    else if ([strdiscount_type caseInsensitiveCompare:kDiscountTypeAppliedRs] == NSOrderedSame)
    {
        
        NSMutableAttributedString  * astrPrice=[[NSMutableAttributedString alloc] initWithString:strPrice];
        [astrPrice addAttribute:NSStrikethroughStyleAttributeName
                          value:@2
                          range:NSMakeRange(0, [astrPrice length])];
        
        self.lbldiscountValue.textColor = [UIColor  grayColor];
        self.lbldiscountValue.font = [UIFont  fontWithName:kfontDefaultItalics size:14];
        self.lblPrice.attributedText = astrPrice;
        self.lbldiscountValue.text = [NSString  stringWithFormat:@"Rs %.02f off",[[dict  objectForKey:@"discount_value"] floatValue]];
    }
    else
    {
        
        NSMutableAttributedString  * astrPrice=[[NSMutableAttributedString alloc] initWithString:strPrice];
        [astrPrice addAttribute:NSStrikethroughStyleAttributeName
                          value:@2
                          range:NSMakeRange(0, [astrPrice length])];
        self.lblPrice.attributedText = astrPrice;
        
        self.lbldiscountValue.font = [UIFont  fontWithName:kfontDefaultItalics size:14];
        self.lbldiscountValue.textColor = [UIColor  blackColor];
        self.lbldiscountValue.text = [NSString  stringWithFormat:@"%@ %% off",[dict  objectForKey:@"discount_value"]];
    }
    
    
    //DISCOUNTED VALUE
    NSString * strdiscounted_value = [NSString  stringWithFormat:@"%@",[dict  objectForKey:@"discounted_amount"]];
    
    self.lbldiscountedValue.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
    self.lbldiscountedValue.textColor = [UIColor blackColor];
    self.lbldiscountedValue.text = [NSString  stringWithFormat:@"Rs %.02f",[strdiscounted_value floatValue]];
    
    
    //STEPPER
    if (objglobalValues.strCustomerGroupId.intValue == 1) {
        //RETAILER
        self.stepperCount.minimumValue = 1.0;
        self.stepperCount.stepValue = 1.0;
        self.lblProductCount.text = [NSString  stringWithFormat:@"%.00f",self.stepperCount.minimumValue];
    }
    else
    {
        //WHOLESAILER
        self.stepperCount.minimumValue = 20.0;
        self.stepperCount.stepValue = 1.0;
        self.lblProductCount.text = [NSString  stringWithFormat:@"%.00f",self.stepperCount.minimumValue];
    }
    
}




#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger intReturn;
    if (section==0) {
        intReturn = 0;
    }
    else if (section==1)
    {
        intReturn = arrOptions?arrOptions.count:0;
    }
    else if (section==2)
    {
        intReturn = 2;
    }
    
    return intReturn;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    
    static NSString *cellIdentifier = @"Cell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:17];
    
    if (indexPath.section == 1) {
        //OPTIONS
        NSDictionary * dictOptions = [arrOptions  objectAtIndex:indexPath.row];
        NSString * strOptName = [NSString  stringWithFormat:@"%@",[dictOptions  objectForKey:@"option_name"]];
        cell.textLabel.text = strOptName;
        
        //DETAIL TEXT LABEL
        if ([[mdictOptionValuesPass  allKeys] containsObject:strOptName]){
            NSString * strOpt = [mdictOptionValuesPass  objectForKey:strOptName];
            NSArray *subStrings = [strOpt componentsSeparatedByString:@"-"];
            NSString * strOptValId = [subStrings objectAtIndex:1];
            NSArray * arrOptionValue = [dictOptions    objectForKey:@"option_value"];
            for (NSDictionary * dict  in arrOptionValue) {
                if ([[dict  objectForKey:@"opt_val_id"] isEqualToString:strOptValId]) {
                    cell.detailTextLabel.text = [dict  objectForKey:@"opt_val"];
                    break;
                    
                }
            }
        }
    }
    
    
    
    else if (indexPath.section == 2) {
        //REVIEWS
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Reviews";
        }
        else if (indexPath.row == 1) {
            cell.textLabel.text = @"Write a Review";
        }
    }
    
    return cell;
}




- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return @"Options";
    }
    else if (section == 2)
    {
        return @"Reviews";
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return Ksection0HeaderViewHeight;
    }
    return UITableViewAutomaticDimension;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //UIView * viewProductImage;
    if (section == 0){
        if ([dictProductDetail  objectForKey:@"product_description"] != nil) {
            UIView * viewProductImage = [[UIView alloc] initWithFrame:CGRectMake(0, 0,SelfViewWidth,Ksection0HeaderViewHeight)];
            viewProductImage.backgroundColor = [UIColor  clearColor];
            
            //DESCRIPTION LABEL
            UILabel * lblDescription = [[UILabel  alloc]  initWithFrame:CGRectMake(8,5,SelfViewWidth -  16,20)];
            lblDescription.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:18];
            lblDescription.backgroundColor = [UIColor  clearColor];
            lblDescription.text = @"Description";
            [viewProductImage  addSubview:lblDescription];
            
            
            
            //WEBVIEW
            int Ywebview = lblDescription.frame.origin.y + lblDescription.frame.size.height +02;
            UIWebView * webViewText = [[UIWebView  alloc]  initWithFrame:CGRectMake(8,Ywebview,SelfViewWidth -  16,Ksection0HeaderViewHeight - Ywebview - 02)];
            NSMutableString * mstrhtmlContent = [dictProductDetail  objectForKey:@"product_description"];
            NSMutableString *html = [NSMutableString stringWithString: @"<html><head><title></title></head><body style=\"background:transparent;\">"];
            [html appendString:mstrhtmlContent];
            [html appendString:@"</body></html>"];
            [webViewText loadHTMLString:[html description] baseURL:nil];
            webViewText.backgroundColor = [UIColor  clearColor];
            [viewProductImage  addSubview:webViewText];
            
            
           
            /*
            //TEXTVIEW
            int Ytextview = lblDescription.frame.origin.y + lblDescription.frame.size.height +02;
            UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(8,Ytextview,SelfViewWidth -  16,Ksection0HeaderViewHeight - Ytextview - 02)];
            NSMutableString * mstrhtmlContent = [dictProductDetail  objectForKey:@"product_description"];
            NSMutableString *html = [NSMutableString stringWithString: @"<html><head><title></title></head><body style=\"background:transparent;\">"];
            [html appendString:mstrhtmlContent];
            [html appendString:@"</body></html>"];
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSFontAttributeName:[UIFont fontWithName:kFontSFUIDisplay_Bold size:18]} documentAttributes:nil error:nil];
            [textView setEditable:NO];
            [textView.textStorage appendAttributedString:attributedString];
            textView.backgroundColor = [UIColor  clearColor];
            [viewProductImage  addSubview:textView];
            */
            
            return viewProductImage;
            
        }
    }
    
    return nil;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSLog(@"indexPath.section = %ld",(long)indexPath.section);
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    
    if (indexPath.section == 1) {
        [self  performSegueWithIdentifier:ksegue_ProducDetailOptions sender:nil];
    }
    
    else if (indexPath.section == 2)
    {
        if (indexPath.row == 0) {
            [self  performSegueWithIdentifier:ksegue_ReviewList sender:nil];
            
        }
        else
        {
            [self  performSegueWithIdentifier:ksegue_writeReview sender:nil];
        }
    }
}



#pragma mark
#pragma mark - PrepareForSegue
#pragma mark
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:ksegue_ProducDetailOptions])
    {
        ProductDetailOptionViewController * objOptionDetailVC = [segue destinationViewController];
        NSIndexPath * objIndexPath = [self.tblvProductDetails  indexPathForSelectedRow];
        objOptionDetailVC.dictOptionPass = [arrOptions  objectAtIndex:objIndexPath.row];
    }
    
    // segue_writeReview
    else if ([segue.identifier  isEqualToString:ksegue_writeReview])
    {
        WriteReviewViewController * objReviewWrite = [segue  destinationViewController];
        objReviewWrite.objProductModelWriteReview = self.objProductModelPass;
        
    }
    else if ([segue.identifier  isEqualToString:ksegue_ReviewList])
    {
        
        ReviewsListViewController * objReviewList = [segue  destinationViewController];
        objReviewList.objProductModeleReview = self.objProductModelPass;
    }
}






- (IBAction)unwindFromProductDetailOptionVC:(UIStoryboardSegue *)segue
{
    
    if ([segue.identifier  isEqualToString:kunwindsegue_ProductDetailOption]) {
        ProductDetailOptionViewController * objDetailOptionVC = (ProductDetailOptionViewController *)segue.sourceViewController;
        NSString * strOptId = objDetailOptionVC.strOption_id;
        NSString * strOptValId = objDetailOptionVC.strOpt_Val_ID;
        
        
        
        NSString * strOptName = objDetailOptionVC.strOption_name;
        
        if (strOptValId.integerValue == 0) {
            [TSMessage  showNotificationInViewController:self title:@"Product Options" subtitle:@"Please Select Product Option"  type:TSMessageNotificationTypeMessage];
            
        }
        else
        {
            
            if ([[mdictOptionValuesPass allKeys]  containsObject:strOptName]) {
                //WEBSERVICE
                NSString * strdictobject = [NSString  stringWithFormat:@"%@-%@",strOptId,strOptValId];
                [mdictOptionValuesPass  removeObjectForKey:strOptName];
                [mdictOptionValuesPass  setObject:strdictobject forKey:strOptName];
                
            }
            else
            {
                //WEBSERVICE
                NSString * strdictobject = [NSString  stringWithFormat:@"%@-%@",strOptId,strOptValId];
                [mdictOptionValuesPass  setObject:strdictobject forKey:strOptName];
            }
            NSLog(@"mdictOptionValuesPass = %@",mdictOptionValuesPass);
            [self.tblvProductDetails  reloadData];
        }
        
    }
}








#pragma mark
#pragma mark - SHOW PHOTO BROWSER
#pragma mark
-(void)showPhotoBrowser:(NSArray *)photoArray
{
    
}




#pragma mark
#pragma mark - IBACTIONS
#pragma mark

- (IBAction)stepperValueChanged:(UIStepper *)sender {
    self.lblProductCount.text = [NSString  stringWithFormat:@"%.00f",sender.value];
    
}


- (IBAction)btnAddToCartClicked:(UIButton *)sender {
    //    addtocart
    //    product_id
    //    quantity
    //    device_id
    //    cart_login_type		L - for login users, G- guest login
    //    product_options		opt_id-opt_val_id,opt_id-opt_val_id eg 19-10,12-11
    //    user_id
    
    
    
    NSString * strProductId = self.objProductModelPass.strProduct_id;
    NSString * strQuantity = [NSString   stringWithFormat:@"%d",self.lblProductCount.text.integerValue];
    
    //This value also changed after every new installation of application
    NSString  *strDeviceID = objglobalValues.strDeviceID;
    
    
    
    //PRODUCT OPTIONS
    NSString * strOptWeb;
    if (mdictOptionValuesPass.count == 0) {
        [TSMessage  showNotificationInViewController:self title:@"Product Options Error" subtitle:@"Please Select Product Options"  type:TSMessageNotificationTypeError];
        return;
    }
    else
    {
        
        NSMutableString * strOpt = [[NSMutableString alloc]init];
        NSArray * dictKeys = [mdictOptionValuesPass allKeys];
        for (NSString* str in dictKeys) {
            NSString * strObj = [mdictOptionValuesPass   objectForKey:str];
            [strOpt appendString:@","];
            [strOpt appendString:strObj];
            
        }
        
        
        strOptWeb  = strOpt;
        if ([strOptWeb hasPrefix:@","] && [strOptWeb length] > 1) {
            strOptWeb  = [strOpt substringFromIndex:1];
        }
    }
    
    
    if ([Reachability isConnected]) {
        [DejalBezelActivityView  activityViewForView:self.view withLabel:@"Please Wait"];
        
        //PARAMETER DICT
        NSDictionary *dictPara = @{@"action" : @"addtocart",@"product_id":strProductId,@"quantity":strQuantity,@"device_id":strDeviceID,@"cart_login_type":@"L",@"product_options":strOptWeb,@"user_id":objglobalValues.strCustomerId};
        NSLog(@"dictPara = %@",dictPara);
        
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
            //REMOVE LOADER
            [DejalBezelActivityView  removeViewAnimated:YES];
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                //CHECK STATUS CODE
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    NSLog(@" %@",result);
                    //SUCESS MESSAGE
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Add to Cart" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
                    objglobalValues.strCartCount = [NSString  stringWithFormat:@"%d",[[result   objectForKey:@"product_count"] integerValue]];
                    [self.navigationController  popViewControllerAnimated:YES];
                }
                else
                {
                    //ERROR MESSAGE
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Add to Cart" subtitle:strmsg  type:TSMessageNotificationTypeError];
                    [self.navigationController  popViewControllerAnimated:YES];
                    
                }
            }
        }];
    }
}




- (IBAction)btnAddToWishListClicked:(UIButton *)sender {
    /* addtowishlist
     product_id
     customer_id
     */
    
    
    NSString * strProductId = self.objProductModelPass.strProduct_id;
    if ([Reachability isConnected]) {
        [DejalBezelActivityView  activityViewForView:self.view withLabel:@"Please Wait"];
        //PARAMETER DICT
        NSDictionary *dictPara = @{@"action":@"addtowishlist",@"product_id":strProductId,@"customer_id":objglobalValues.strCustomerId};
        NSLog(@"dictPara = %@",dictPara);
        
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
            //REMOVE LOADER
            [DejalBezelActivityView  removeViewAnimated:YES];
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                //CHECK STATUS CODE
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    NSLog(@" %@",result);
                    //SUCCESS MESSAGE
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Add to WishList" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
                    
                }
                else
                {
                    //ERROR MESSAGE
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Add to WishList" subtitle:strmsg  type:TSMessageNotificationTypeError];
                    
                }
            }
        }];
        
    }
    
}



@end
