//
//  ProfileEditViewController.h
//  kazumi
//
//  Created by Yashvir on 22/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "Network.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "GlobalValues.h"
#import "KazumiMacros.h"
#import "Constant.h"
#import "Location.h"

@interface ProfileEditViewController : UIViewController<UIScrollViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{
    GlobalValues * objglobalValues;
    NSString * strAddressId;
    Location * objlocSelectedCountry;
    Location * objlocSelectedZone;
    NSMutableArray * marrCountry;
    NSMutableArray * marrZone;
}


@property (weak, nonatomic) IBOutlet UIScrollView *bgScrollView;

@property (weak, nonatomic) IBOutlet UITextField *txfdFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txfdLastName;
@property (weak, nonatomic) IBOutlet UITextField *txfdContactNo;
@property (weak, nonatomic) IBOutlet UITextField *txfdEmail;

@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectCountry;
@property (weak, nonatomic) IBOutlet UITextField *txfdcountry;

@property (weak, nonatomic) IBOutlet UILabel *lblState;
@property (weak, nonatomic) IBOutlet UITextField *txfdState;

@property (weak, nonatomic) IBOutlet UITextField *txfdadd1;
@property (weak, nonatomic) IBOutlet UITextField *txfdAdd2;
@property (weak, nonatomic) IBOutlet UITextField *txfdCity;
@property (weak, nonatomic) IBOutlet UITextField *txfdPostCode;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;



- (IBAction)btnSaveClicked:(UIButton *)sender;





//METHODS
-(void)setUpUI;
-(void)intializerMethod;
-(void)customBackButton;
-(void)webserviceCallForEditProfile;
-(void)textfieldToolBar:(UITextField *)txfd;
-(void)webserviceCallForEditProfileSave:(NSDictionary *)dictPara;
-(void)webserviceCallForZones:(NSString*)strCountryId;

@end
