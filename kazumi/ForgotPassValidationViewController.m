//
//  ForgotPassValidationViewController.m
//  kazumi
//
//  Created by NM8 on 27/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "ForgotPassValidationViewController.h"

@interface ForgotPassValidationViewController ()

@end

@implementation ForgotPassValidationViewController

- (void)viewDidLoad {
   [super viewDidLoad];
    
    [self  viewSetUP];
    [self   IntializerMethod];
    [self  customBackButton];
    
    self.title = @"Password Validation";

    
}



#pragma mark
#pragma mark INTIALIZER
#pragma mark
-(void)IntializerMethod
{
    
}


#pragma mark
#pragma mark viewSetUP
#pragma mark
-(void)viewSetUP
{
  
    //ACTIVATION CODE
    self.txfdActivationCode.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdActivationCode.layer.borderWidth = 0.3;
    self.txfdActivationCode.layer.masksToBounds = true;
    
    UIView * viewActivationCode = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    viewActivationCode.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdActivationCode.leftView = viewActivationCode;
    self.txfdActivationCode.leftViewMode = UITextFieldViewModeAlways;
    


    //NEW PSWD
    self.txfdNewPswd.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdNewPswd.layer.borderWidth = 0.3;
    self.txfdNewPswd.secureTextEntry = YES;

    self.txfdNewPswd.layer.masksToBounds = true;
    
    
    UIView * viewNewPswd = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    viewNewPswd.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdNewPswd.leftView = viewNewPswd;
    self.txfdNewPswd.leftViewMode = UITextFieldViewModeAlways;

    
    
    //CONFIRM PSWD
    self.txfdConfirmPswd.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdConfirmPswd.layer.borderWidth = 0.3;
    self.txfdConfirmPswd.secureTextEntry = YES;
   // int integer = [self.txfdConfirmPswd.text intValue];

    self.txfdConfirmPswd.layer.masksToBounds = true;
    
    
    UIView * viewConfirmPswd = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    viewConfirmPswd.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdConfirmPswd.leftView = viewConfirmPswd;
    self.txfdConfirmPswd.leftViewMode = UITextFieldViewModeAlways;
    
    //BTN SUBMIT
    //self.btnAdd.layer.borderWidth = 2.0;
    self.btnAdd.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    //self.btnAdd.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnAdd.layer.cornerRadius = kButtonCornerRadius;
    self.btnAdd.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
}



//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString: self.txfdConfirmPswd.text];
//   
//    
//    BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
//       return stringIsValid;
//}
//





#pragma mark
#pragma mark customBackButton
#pragma mark
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
































- (IBAction)btnSubmit:(id)sender {
    
    //[self webserviceCallPasswordValidation];
    
    //Validation
    
    self.txfdNewPswd.text = [self.txfdNewPswd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.txfdConfirmPswd.text = [self.txfdConfirmPswd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    //PSWD
    while ([self.txfdNewPswd.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdNewPswd.text = [self.txfdNewPswd.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    //CONFIRM PSWD
    while ([self.txfdConfirmPswd.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdConfirmPswd.text = [self.txfdConfirmPswd.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    //PSWD
    if (self.txfdNewPswd.text.length <=5) {
        [TSMessage showNotificationWithTitle:@"PASSWORD ERROR"
                                    subtitle:@"Please enter password greater then 5 characters"
                                        type:TSMessageNotificationTypeError];
        return;
        
    }
    
    
    
    //CONFIRM PSWD
    if (![self.txfdNewPswd.text  isEqualToString:self.txfdConfirmPswd.text]) {
        [TSMessage showNotificationWithTitle:@"CONFIRM PASSWORD ERROR"
                                    subtitle:@"Please re confirm Password"
                                        type:TSMessageNotificationTypeError];
        return;
        
    }
    if ([Reachability  isConnected]) {
        [self  webserviceCallPasswordValidation];
    }
}


    


#pragma mark
#pragma mark webserviceCallPasswordValidation
#pragma mark+
-(void)webserviceCallPasswordValidation
{
    
    //WEBSERVICE CALL
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary *dict = @{@"action" : @"getforgotpwdverify",@"activation_code" : self.txfdActivationCode.text,@"new_password":self.txfdNewPswd.text};
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
        if (error) {
            [DejalBezelActivityView  removeViewAnimated:YES];
            NSLog(@"%@",error);
        }
        else
        {
            [DejalBezelActivityView  removeViewAnimated:YES];
             NSLog(@"%@",result);
            
        }
        
    }
     ];
    
}

#pragma mark
#pragma mark  TEXTFILED DELEGATE
#pragma mark
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField   resignFirstResponder];
    return YES;
}


@end
