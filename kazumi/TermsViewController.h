//
//  TermsViewController.h
//  kazumi
//
//  Created by Yashvir on 20/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Reachability.h"
#import "Network.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "MKNumberBadgeView.h"
#import "CartViewController.h"
#import "MKNumberBadgeView.h"
#import "CartViewController.h"






@interface TermsViewController : UIViewController
{
      GlobalValues * objglobalValues;
}
@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButtonTerms;
@property (weak, nonatomic) IBOutlet UIWebView *webViewTerms;

//METHODS
-(void)setUpUI;
-(void)intializerMethod;
-(void)webserviceCallTerms;
-(void)setWebViewUsingHtlml:(NSMutableString*)strhtml;


@end
