//
//  OrderReviewViewController.h
//  kazumi
//
//  Created by Yashvir on 17/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "DejalActivityView.h"
#import "GlobalValues.h"
#import "Network.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "TSMessage.h"
#import "KazumiMacros.h"
#import "CartModel.h"
#import "UIImageView+AFNetworking.h"
#import "OrderReviewTableViewCell.h"
#import "PaymentMethodTableViewController.h"

@interface OrderReviewViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    GlobalValues * objGlobalValue;
    NSMutableArray * marrDataCartModel;
    NSDictionary * mdictResult;

}


@property (strong, nonatomic) IBOutlet UITableView *tblvOrderReview;

//Footer View
@property (strong, nonatomic) IBOutlet UIView *viewFooter;

@property (strong, nonatomic) IBOutlet UIButton *btnSelectPayment;

@property (strong, nonatomic) IBOutlet UILabel *lblTotalPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblCouponCode;
@property (strong, nonatomic) IBOutlet UILabel *lblCouponValue;
@property (strong, nonatomic) IBOutlet UILabel *lblDiscount;
@property (strong, nonatomic) IBOutlet UILabel *lblPayableAmount;


@property (strong, nonatomic) IBOutlet UIView *viewFreeShipping;
@property (strong, nonatomic) IBOutlet UIView *viewPayment;




- (IBAction)btnSelectPaymentClicked:(UIButton *)sender;







//METHODS
-(void)setUpUI;
-(void)intializerMethod;
-(void)customBackButton;
-(void)webserviceCallForCartList;
-(void)webserviceCallForDeleteProductFromCart:(CartModel*)objcart;
-(void)setProductPriceLabels:(NSDictionary*)dict;


@end
