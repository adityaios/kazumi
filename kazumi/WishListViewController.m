//
//  WishListViewController.m
//  kazumi
//
//  Created by NM8 on 01/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "WishListViewController.h"

@interface WishListViewController ()

@end

@implementation WishListViewController


#pragma mark
#pragma mark VC LIFE CYCLE
#pragma mark
- (void)viewDidLoad {
    [super viewDidLoad];
    [self  intializerMethod];
    [self  setUpUI];
    [self  CustomBackButton];
    
    if ([Reachability  isConnected]) {
        [self  webserviceCallForproductwishlist];
    }
}

#pragma mark
#pragma mark INTIALIZER
#pragma mark
-(void)intializerMethod
{
    self.title = @"My Wish List";
    // GLOBAL VALUE
    objglobalValues = [GlobalValues  sharedManager];
    
    UINib *nib = [UINib nibWithNibName:@"WishListViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"WishListViewCell"];
    
}


#pragma mark
#pragma mark VIEW SET UP
#pragma mark
-(void)setUpUI
{
    self.tableView.frame = CGRectMake(0, 0, SelfViewWidth, SelfViewHeight);
   
}




#pragma mark Custom Back Button
-(void)CustomBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}



#pragma mark
#pragma mark IBACTION
#pragma mark

- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark btnDeleteClicked
-(void)btnDeleteClicked:(UIButton*)sender{
    UIButton *button = (UIButton *)sender;
    CGPoint buttonOriginInTableView = [button convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonOriginInTableView];
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    
    //ALERT FOR DELETE
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"WishList Item Delete"
                                  message:@"Remove item from Wish List?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             CatModel * objCatModel = [marrCatModelData   objectAtIndex:indexPath.row];
                             [self  webserviceCallForproductwishlistDelete:objCatModel];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}







#pragma mark
#pragma mark WEBSERVICE CALL
#pragma mark


#pragma mark Product Wishlist
-(void)webserviceCallForproductwishlist
{
    if ([Reachability isConnected]) {
        [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
        NSDictionary * dict = @{@"action":@"productwishlist",@"customer_id":objglobalValues.strCustomerId};
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
            [DejalBezelActivityView  removeViewAnimated:YES];
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Success" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
                    NSMutableArray * arr = [result  objectForKey:@"Result"];
                    NSLog(@"%@",arr);
                    
                    marrCatModelData = [[NSMutableArray  alloc] init];
                    for (NSDictionary * dict in [result  objectForKey:@"Result"]) {
                        ProductModelClass * objModelProduct = [ProductModelClass  new];
                        objModelProduct.strActual_price = [dict  objectForKey:@"actual_price"];
                        objModelProduct.strDiscount_type = [dict  objectForKey:@"discount_type"];
                        objModelProduct.strDiscount_value = [dict  objectForKey:@"discount_value"];
                        objModelProduct.strDiscounted_amount = [dict  objectForKey:@"discounted_amount"];
                        objModelProduct.strProduct_id = [dict  objectForKey:@"product_id"];
                        objModelProduct.strProduct_image = [dict  objectForKey:@"product_image"];
                        objModelProduct.strProduct_Name = [dict  objectForKey:@"product_name"];
                        [marrCatModelData   addObject:objModelProduct];
                    }
                    [self.tableView  reloadData];
                    
                }
                else
                {
                    [DejalBezelActivityView  removeViewAnimated:YES];
                    NSLog(@"%@",result);
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg  type:TSMessageNotificationTypeError];
                }
            }
        }
         ];
    }
}







#pragma mark Product Wishlist Delete

-(void)webserviceCallForproductwishlistDelete:(ProductModelClass *)objCatModel
{
    //    deleteFromWishlist
    //    product_id
    //    customer_id
    NSDictionary * dictPara = @{@"action":@"deleteFromWishlist",@"product_id":objCatModel.strProduct_id,@"customer_id":objglobalValues.strCustomerId};
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            
            NSLog(@"%@",result);
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Success" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
                [self  webserviceCallForproductwishlist];
                
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg  type:TSMessageNotificationTypeError];
            }
        }
    }];
    
}





#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark


#pragma mark number Of Sections
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



#pragma mark number Of Rows In Section
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return marrCatModelData ? marrCatModelData.count :0;
    
}



#pragma mark cell For Row At IndexPath
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    WishListViewCell * tcell =  [tableView dequeueReusableCellWithIdentifier:@"WishListViewCell"];
    
    
    //CELL BORDER
    tcell.contentView.layer.borderColor =  [UIColor   lightGrayColor].CGColor;
    tcell.contentView.layer.borderWidth = 1.0;
    
    
    //Modal Class Object
    ProductModelClass * objModelClass = [marrCatModelData objectAtIndex:indexPath.row];
    
    //PRODUCT IMAGEVIEW
    [tcell.imgProduct setImageWithURL:[NSURL  URLWithString:objModelClass.strProduct_image] placeholderImage:[UIImage  imageNamed:@"placeholder"]];
    
    //Lbl name
    objModelClass.strProduct_Name  = [objModelClass.strProduct_Name  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    tcell.lblProName.text = objModelClass.strProduct_Name;
    
    
    
    //Btn Delete
    [tcell.btnDelete   addTarget:self action:@selector(btnDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    if (objModelClass.strDiscount_value.integerValue == 0) {
        
        //PRICE
        tcell.lblActualPrice.text = [NSString  stringWithFormat:@"Rs %.02f",[objModelClass.strActual_price floatValue]];
        
        //DISCOUNT
        tcell.lblDiscount.text =  @"";
        
        //DISCOUNT VALUE
        tcell.lblDisAmount.text = @"";
        
    }
    else
    {
        
        //PRICE
        NSString * strActualPrice = [NSString  stringWithFormat:@"Rs %.02f",[objModelClass.strActual_price floatValue]];
        
        
        NSMutableAttributedString *attributeStringPrice = [[NSMutableAttributedString alloc] initWithString:strActualPrice];
        
        //ATTRIBUTE STRIKE
        [attributeStringPrice addAttribute:NSStrikethroughStyleAttributeName
                                     value:@2
                                     range:NSMakeRange(0, [attributeStringPrice length])];
        
        //FONT ATTRIBUTE
        UIFont *font = [UIFont fontWithName:kFontSFUIDisplay_Light size:12.0];
        NSDictionary *  attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                      forKey:NSFontAttributeName];
        [attributeStringPrice addAttributes:attrsDictionary range:NSMakeRange(0, [attributeStringPrice length])];
        tcell.lblActualPrice.attributedText = attributeStringPrice;
        
        //DISCOUNT TYPE
        NSString * strdiscount_type = [NSString  stringWithFormat:@"%@",objModelClass.strDiscount_type];
        
        if ([strdiscount_type caseInsensitiveCompare:kDiscountTypeAppliedRs] == NSOrderedSame)
        {
            
            tcell.lblDiscount.text = [NSString  stringWithFormat:@"Rs %.02f off",[objModelClass.strDiscount_value floatValue]];
        }
        else
        {
            tcell.lblDiscount.text = [NSString  stringWithFormat:@"%@ %% off",objModelClass.strDiscount_value];
            
        }
        
        tcell.lblDisAmount.text = [NSString  stringWithFormat:@"Rs %.02f",[objModelClass.strDiscounted_amount floatValue]];
    }
    
    return tcell;
}



#pragma mark height For Row At IndexPath
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}



#pragma mark did Select Row At IndexPath
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductDetailsViewController * objProductDetailsVC =    [self.storyboard   instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    objProductDetailsVC.objProductModelPass = [marrCatModelData  objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:objProductDetailsVC animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



@end
