//
//  ShippingDetailsViewController.m
//  kazumi
//
//  Created by Yashvir on 15/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "ShippingDetailsViewController.h"

@interface ShippingDetailsViewController ()

@end

@implementation ShippingDetailsViewController
#define KNoValueCheck @"0"

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  viewSetUP];
    [self  intializerMethod];
    [self  customBackButton];
    if ([Reachability  isConnected]) {
        [self  webserviceCallForAddressDetails];
    }
}



#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    
    objglobalValues = [GlobalValues  sharedManager];
}



#pragma mark
#pragma mark customBackButton
#pragma mark
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark viewSetUP
#pragma mark
-(void)viewSetUP
{
    
    int Xpadding = 10;
    int Ypadding = 8;
    int Y = Ypadding;
    
    self.title = @"Billing/Shipping Details";
    
    
    //SCROLLVIEW
    self.bgScrollView.frame = CGRectMake(0,64,SelfViewWidth,SelfViewHeight);
    // self.bgScrollView.backgroundColor = UIColorFromRGBWithAlpha(kazumiYellowColor, 1);
    self.bgScrollView.backgroundColor = [UIColor  whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    //TEXTFIELD FIRST NAME
    self.txfdFirstName.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewfirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewfirstName.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdFirstName.leftView = paddingViewfirstName;
    self.txfdFirstName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdFirstName.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdFirstName.layer.borderWidth = 0.3;
    self.txfdFirstName.layer.masksToBounds = true;
    
    
    Y = Y + Ypadding + 40;
    
    
    //TEXTFIELD LAST NAME
    self.txfdLastName.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewlastName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewlastName.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdLastName.leftView = paddingViewlastName;
    self.txfdLastName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdLastName.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdLastName.layer.borderWidth = 0.3;
    self.txfdLastName.layer.masksToBounds = true;
    
    
    Y = Y + Ypadding + 40;
    
    
    //TEXTFIELD CONTACT NO
    self.txfdContactNo.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewContact = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewContact.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdContactNo.leftView = paddingViewContact;
    self.txfdContactNo.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdContactNo.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdContactNo.layer.borderWidth = 0.3;
    self.txfdContactNo.layer.masksToBounds = true;
    [self  textfieldToolBar:self.txfdContactNo];
    
    
    
    Y = Y + Ypadding + 40;
    
    
    //TEXTFIELD EMAIL
    self.txfdEmail.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewEmail = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewEmail.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdEmail.leftView = paddingViewEmail;
    self.txfdEmail.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdEmail.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdEmail.layer.borderWidth = 0.3;
    self.txfdEmail.layer.masksToBounds = true;
    
    Y = Y + Ypadding + 40;
    
    //ADDRESS
    self.lblAddress.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 20);
    Y = Y + Ypadding/2 + 20;
    
    //Country
    self.lblSelectCountry.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 20);
    Y = Y + 20;
    
    //TEXTFIELD COUNTRY
    self.txfdcountry.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewCountry = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewCountry.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdcountry.leftView = paddingViewCountry;
    self.txfdcountry.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdcountry.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdcountry.layer.borderWidth = 0.3;
    self.txfdcountry.layer.masksToBounds = true;
    [self  pickerViewAsInputView:self.txfdcountry];
    
    
    Y = Y + Ypadding + 40;
    
    //State
    self.lblState.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 20);
    Y = Y + 20;
    
    //TEXTFIELD ZONE
    self.txfdState.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewState = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewState.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdState.leftView = paddingViewState;
    self.txfdState.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdState.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdState.layer.borderWidth = 0.3;
    self.txfdState.layer.masksToBounds = true;
    [self  pickerViewAsInputView:self.txfdState];
    
    
    //ADD1
    Y = Y + Ypadding + 40;
    
    //TEXTFIELD ADD1
    self.txfdadd1.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewadd1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewadd1.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdadd1.leftView = paddingViewadd1;
    self.txfdadd1.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdadd1.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdadd1.layer.borderWidth = 0.3;
    self.txfdadd1.layer.masksToBounds = true;
    
    
    
    //ADD2
    Y = Y + Ypadding + 40;
    
    //TEXTFIELD ADD1
    self.txfdAdd2.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewadd2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewadd2.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdAdd2.leftView = paddingViewadd2;
    self.txfdAdd2.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdAdd2.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdAdd2.layer.borderWidth = 0.3;
    self.txfdAdd2.layer.masksToBounds = true;
    
    //CITY
    Y = Y + Ypadding + 40;
    
    //TEXTFIELD  CITY
    self.txfdCity.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewCity = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewCity.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdCity.leftView = paddingViewCity;
    self.txfdCity.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdCity.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdCity.layer.borderWidth = 0.3;
    self.txfdCity.layer.masksToBounds = true;
    
    
    //POSTCODE
    Y = Y + Ypadding + 40;
    
    //TEXTFIELD  POSTCODE
    self.txfdPostCode.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewPostCode = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingViewPostCode.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.txfdPostCode.leftView = paddingViewPostCode;
    self.txfdPostCode.leftViewMode = UITextFieldViewModeAlways;
    
    self.txfdPostCode.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdPostCode.layer.borderWidth = 0.3;
    self.txfdPostCode.layer.masksToBounds = true;
    [self  textfieldToolBar:self.txfdPostCode];
    
    Y = Y + Ypadding + 40;
    
    //BTN PROCEED TO PAYMENT
    self.btnProceedToPayment.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    self.btnProceedToPayment.layer.borderWidth = 0.1;
    self.btnProceedToPayment.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    self.btnProceedToPayment.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnProceedToPayment.layer.cornerRadius = kButtonCornerRadius;
    self.btnProceedToPayment.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
    
    
    Y = Y + Ypadding + 40*4;
    
    //SCROLLVIEW CONTENT SIZE
    self.bgScrollView.contentSize = CGSizeMake(self.bgScrollView.frame.size.width, Y);
    
    
    
    
        
    
    
    
    
    
}



-(void)textfieldToolBar:(UITextField *)txfd
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,SelfViewWidth, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor  redColor];
    
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    txfd.inputAccessoryView = numberToolbar;
}


-(void)cancelNumberPad{
    [self.view  endEditing:YES];
    [self.bgScrollView setContentOffset:
     CGPointMake(0, -self.bgScrollView.contentInset.top) animated:YES];
}

-(void)doneWithNumberPad{
    [self.view  endEditing:YES];
    [self.bgScrollView setContentOffset:
     CGPointMake(0, -self.bgScrollView.contentInset.top) animated:YES];
}









#pragma mark
#pragma mark TEXTFIELD DELEGATE
#pragma mark
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField.tag == 9) {
        [self.bgScrollView setContentOffset:
         CGPointMake(0, -self.bgScrollView.contentInset.top) animated:YES];
        [textField  resignFirstResponder];
        return YES;
    }
    
    
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [self.bgScrollView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        [self.bgScrollView setContentOffset:CGPointMake(0,0) animated:YES];
        [textField resignFirstResponder];
        return YES;
    }
    return NO;
    
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view  endEditing:YES];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (marrZone.count == 0 && textField == self.txfdState) {
        [self.view   endEditing:YES];
        [TSMessage  showNotificationInViewController:self title:@"Please Select Country First" subtitle:nil type:TSMessageNotificationTypeError];
        return;
    }
    
    
    [self.bgScrollView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.txfdcountry) {
        NSLog(@"Country Textfield End Editing");
        if (objlocSelectedCountry.strLocationId.integerValue == 0 ) {
            
        }
        else
        {
            if ([Reachability   isConnected]) {
                [self  webserviceCallForZones:objlocSelectedCountry.strLocationId];
            }
        }
    }
}

#pragma mark
#pragma mark PICKER VIEW DELEGATE
#pragma mark
-(void)pickerViewAsInputView:(UITextField *)txfd
{
    UIPickerView *  picker = [[UIPickerView alloc] init];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    txfd.inputView = picker;
    
    //TOOLBAR
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(PickerdoneBtnPress:)];
    [toolBar setItems:[NSArray arrayWithObject:btn]];
    txfd.inputAccessoryView = toolBar;
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.txfdcountry.inputView == pickerView) {
        return marrCountry.count;
    }
    else
    {
        return marrZone.count;
    }
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.txfdcountry.inputView == pickerView) {
        Location * objLoc = [Location   new];
        objLoc = [marrCountry   objectAtIndex:row];
        return objLoc.strLocationName;
    }
    else
    {
        Location * objLoc = [Location   new];
        objLoc = [marrZone   objectAtIndex:row];
        return objLoc.strLocationName;
    }
}



- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (self.txfdcountry.inputView == pickerView) {
        objlocSelectedCountry = [Location   new];
        objlocSelectedCountry = [marrCountry  objectAtIndex:row];
        NSLog(@"strCountrySelected = %@",objlocSelectedCountry.strLocationName);
        self.txfdcountry.text = objlocSelectedCountry.strLocationName;
        
    }
    else
    {
        objlocSelectedZone = [Location   new];
        objlocSelectedZone = [marrZone  objectAtIndex:row];
        NSLog(@"strZoneSelected = %@",objlocSelectedZone.strLocationName);
        self.txfdState.text = objlocSelectedZone.strLocationName;
        
    }
}



#pragma mark
#pragma mark PickerdoneBtnPress
#pragma mark
-(void)PickerdoneBtnPress:(id)sender
{
    
    [self.view  endEditing:YES];
    NSLog(@"%@",sender);
    UIBarButtonItem * bar = (UIBarButtonItem*)sender;
    UIToolbar * toolbar = (UIToolbar*)self.txfdcountry.inputAccessoryView;
    if ([toolbar.items  containsObject:bar]) {
        NSLog(@"Country Bar Button Clicked");
    }
    else
    {
        NSLog(@"State Bar Button Clicked");
        
    }
}






#pragma mark
#pragma mark IBACTION
#pragma mark
- (IBAction)btnProceedToPayment:(UIButton *)sender {
    /* to add shiipping and billing address
     addaddressdetails
     firstname
     lastname
     telephone
     email
     address_1
     address_2
     postcode
     city
     zone_id
     country_id
     
     customer_id
     device_id
     address_id
     */
    
    if ([Reachability   isConnected]) {
        
        //First Name Validation
        self.txfdFirstName.text = [self.txfdFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        while ([self.txfdFirstName.text rangeOfString:@"  "].location != NSNotFound) {
            self.txfdFirstName.text = [self.txfdFirstName.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        }
        
        if (self.txfdFirstName.text.length == 0 ) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter First Name" type:TSMessageNotificationTypeError];
            return;
        }
        
        
        //Last Name Validation
        self.txfdLastName.text = [self.txfdLastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        while ([self.txfdLastName.text rangeOfString:@"  "].location != NSNotFound) {
            self.txfdLastName.text = [self.txfdLastName.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        }
        
        if (self.txfdLastName.text.length == 0 ) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter Last Name" type:TSMessageNotificationTypeError];
            return;
        }
        
        
        //Contact No
        self.txfdContactNo.text = [self.txfdContactNo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        while ([self.txfdContactNo.text rangeOfString:@"  "].location != NSNotFound) {
            self.txfdContactNo.text = [self.txfdContactNo.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        }
        
        if (self.txfdContactNo.text.length == 0 ) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter Contant No" type:TSMessageNotificationTypeError];
            return;
        }
        
        if (self.txfdContactNo.text.length > 15 || self.txfdContactNo.text.length < 8 ) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter valid Contant No" type:TSMessageNotificationTypeError];
            return;
        }
        
        
        //Email
        self.txfdEmail.text = [self.txfdEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        while ([self.txfdEmail.text rangeOfString:@"  "].location != NSNotFound) {
            self.txfdEmail.text = [self.txfdEmail.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        }
        
        if (self.txfdEmail.text.length == 0 ) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter Email" type:TSMessageNotificationTypeError];
            return;
        }
        
        if (![self  NSStringIsValidEmail:self.txfdEmail.text]) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter valid Email" type:TSMessageNotificationTypeError];
            return;
            
        }
        
        
        //ADDRESS 1
        self.txfdadd1.text = [self.txfdadd1.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        while ([self.txfdadd1.text rangeOfString:@"  "].location != NSNotFound) {
            self.txfdadd1.text = [self.txfdadd1.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        }
        
        if (self.txfdadd1.text.length == 0 ) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter Address 1" type:TSMessageNotificationTypeError];
            return;
        }
        
        
        
        //ADDRESS 2
        self.txfdAdd2.text = [self.txfdAdd2.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        while ([self.txfdAdd2.text rangeOfString:@"  "].location != NSNotFound) {
            self.txfdAdd2.text = [self.txfdAdd2.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        }
        
        if (self.txfdAdd2.text.length == 0 ) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter Address 2" type:TSMessageNotificationTypeError];
            return;
        }
        
        
        //POSTCODE
        self.txfdPostCode.text = [self.txfdPostCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        while ([self.txfdPostCode.text rangeOfString:@"  "].location != NSNotFound) {
            self.txfdPostCode.text = [self.txfdPostCode.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        }
        
        if (self.txfdPostCode.text.length == 0 ) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter PostCode" type:TSMessageNotificationTypeError];
            return;
        }
        
        
        //City
        self.txfdCity.text = [self.txfdCity.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        while ([self.txfdCity.text rangeOfString:@"  "].location != NSNotFound) {
            self.txfdCity.text = [self.txfdCity.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        }
        
        if (self.txfdCity.text.length == 0 ) {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter City" type:TSMessageNotificationTypeError];
            return;
        }
        
        
        //COUNTRY VALIDATION
        if (objlocSelectedCountry) {
            if (objlocSelectedCountry.strLocationName.length <= 2) {
                [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter Country" type:TSMessageNotificationTypeError];
                return;
            }
            
        }
        else
        {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter Country" type:TSMessageNotificationTypeError];
            return;
            
        }
        
        
        //ZONE VALIDATION
        if (objlocSelectedZone) {
            if (objlocSelectedZone.strLocationName.length <= 2) {
                [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter Zone" type:TSMessageNotificationTypeError];
                return;
            }
            
        }
        else
        {
            [TSMessage  showNotificationInViewController:self title:@"Validation Error" subtitle:@"Please enter Zone" type:TSMessageNotificationTypeError];
            return;
            
        }
        
        
        //DEVICE ID
        NSString  *strDeviceID = objglobalValues.strDeviceID;
        
        
        //PARAMETERS
        NSMutableDictionary * mdict = [NSMutableDictionary   new];
        [mdict  setObject:@"addaddressdetails" forKey:@"action"];
        [mdict  setObject:self.txfdFirstName.text forKey:@"firstname"];
        [mdict  setObject:self.txfdLastName.text forKey:@"lastname"];
        [mdict  setObject:self.txfdContactNo.text forKey:@"telephone"];
        [mdict  setObject:self.txfdEmail.text forKey:@"email"];
        [mdict  setObject:self.txfdadd1.text forKey:@"address_1"];
        [mdict  setObject:self.txfdAdd2.text forKey:@"address_2"];
        [mdict  setObject:self.txfdPostCode.text forKey:@"postcode"];
        [mdict  setObject:self.txfdCity.text forKey:@"city"];
        [mdict  setObject:objglobalValues.strCustomerId forKey:@"customer_id"];
        [mdict  setObject:strDeviceID forKey:@"device_id"];
        [mdict  setObject:strAddressId forKey:@"address_id"];
        [mdict  setObject:objlocSelectedCountry.strLocationId forKey:@"country_id"];
        [mdict  setObject:objlocSelectedZone.strLocationId forKey:@"zone_id"];
        
        //    [mdict  setObject:objlocSelectedCountry.strLocationName forKey:@"country_Name"];
        //    [mdict  setObject:objlocSelectedZone.strLocationName forKey:@"zone_Name"];
        
        
        NSLog(@"mdict = %@",mdict);
        
        
        [DejalBezelActivityView  activityViewForView:self.view withLabel:@"Please Wait"];
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:mdict andCompletionHandler:^(id result, NSError *error) {
            [DejalBezelActivityView  removeViewAnimated:YES];
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                
                NSLog(@"%@",result);
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Success" subtitle:strmsg type:TSMessageNotificationTypeSuccess];
                    [self  performSegueWithIdentifier:ksegue_segue_OrderReview sender:nil];
                }
                else
                {
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg type:TSMessageNotificationTypeError];
                }
            }
        }];
    }
}




-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}





#pragma mark
#pragma mark ZONE WEBSERVICE
#pragma mark


#pragma mark webserviceCallForAddressDetails
-(void)webserviceCallForAddressDetails
{
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary * dictPara = @{@"action":@"addressdetails",@"user_id":objglobalValues.strCustomerId,@"cart_login_type":@"L"};
    
    [Network   getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView   removeViewAnimated:YES];
        if (error) {
            NSLog(@"Error = %@",error);
        }
        else
        {
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Success" subtitle:strmsg type:TSMessageNotificationTypeSuccess];
                NSLog(@"result = %@",result);
                
                NSDictionary * dictResult = [[NSDictionary  alloc]  init];
                dictResult = [result  objectForKey:@"Result"];
                
                //DETAILS
                NSDictionary * dictAddressDetails = [dictResult objectForKey:@"address_details"];
                
                //ADDRESS ID
                strAddressId = [NSString  stringWithFormat:@"%@",[dictAddressDetails objectForKey:@"address_id"]];
                if (strAddressId.intValue == 0) {
                    strAddressId = @"0";
                }
                
                
                //First Name
                self.txfdFirstName.text = [dictAddressDetails objectForKey:@"fname"];
                //Last Name
                self.txfdLastName.text = [dictAddressDetails objectForKey:@"lname"];
                //Contact No
                self.txfdContactNo.text = [dictAddressDetails objectForKey:@"telephone"];
                //Email
                self.txfdEmail.text = [dictAddressDetails objectForKey:@"email"];
                
                //Add1
                NSString * strAdd1 = [NSString  stringWithFormat:@"%@",[dictAddressDetails objectForKey:@"address_1"]];
                
                if ([strAdd1  isEqualToString:KNoValueCheck]) {
                    
                }
                else
                {
                    self.txfdadd1.text = strAdd1;
                    
                }
                
                //Add2
                NSString * strAdd2 = [NSString  stringWithFormat:@"%@",[dictAddressDetails objectForKey:@"address_2"]];
                
                if ([strAdd2  isEqualToString:KNoValueCheck]) {
                    
                }
                else
                {
                    self.txfdAdd2.text = strAdd2;
                    
                }
                
                //City
                NSString * strCity = [NSString  stringWithFormat:@"%@",[dictAddressDetails objectForKey:@"city"]];
                
                if ([strCity  isEqualToString:KNoValueCheck]) {
                    
                }
                else
                {
                    self.txfdCity.text = strCity;
                    
                }
                
                //PostCode
                NSString * strPostCode = [NSString  stringWithFormat:@"%@",[dictAddressDetails objectForKey:@"postcode"]];
                if ([strPostCode  isEqualToString:KNoValueCheck]) {
                    
                }
                else
                {
                    self.txfdPostCode.text = strPostCode;
                    
                }
                
                
                //COUNTRY
                NSString * strCountryID = [NSString  stringWithFormat:@"%@",[dictAddressDetails objectForKey:@"country_id"]];
                
                //Zone
                NSString * strZoneID = [NSString  stringWithFormat:@"%@",[dictAddressDetails objectForKey:@"zone_id"]];
                
                
                NSArray * arrCountry = [NSArray   new];
                arrCountry = [dictResult  objectForKey:@"countries"];
                if ([strCountryID  isEqualToString:KNoValueCheck]) {
                    marrCountry = [NSMutableArray  new];
                    for (NSDictionary * dict in arrCountry) {
                        Location * objLoc = [Location   new];
                        objLoc.strLocationId = [dict  objectForKey:@"country_id"];
                        objLoc.strLocationName = [dict  objectForKey:@"country_name"];
                        [marrCountry  addObject:objLoc];
                        
                    }
                }
                else
                {
                    objlocSelectedCountry = [Location  new];
                    objlocSelectedCountry.strLocationId = strCountryID;
                    
                    
                    
                    marrCountry = [NSMutableArray  new];
                    for (NSDictionary * dict in arrCountry) {
                        Location * objLoc = [Location   new];
                        objLoc.strLocationId = [dict  objectForKey:@"country_id"];
                        objLoc.strLocationName = [dict  objectForKey:@"country_name"];
                        [marrCountry  addObject:objLoc];
                        if (strCountryID.integerValue == objLoc.strLocationId.integerValue) {
                            objlocSelectedCountry.strLocationName = objLoc.strLocationName;
                        }
                    }
                    
                    
                    //COUNTRY NAME
                    self.txfdcountry.text = objlocSelectedCountry.strLocationName;
                    
                    //ZONE
                    NSArray * arrZone = [NSArray   new];
                    arrZone = [dictResult  objectForKey:@"zone"];
                    
                    
                    objlocSelectedZone = [Location  new];
                    objlocSelectedZone.strLocationId = strZoneID;
                    
                    marrZone = [NSMutableArray  new];
                    for (NSDictionary * dict in arrZone) {
                        Location * objLoc = [Location   new];
                        objLoc.strLocationId = [dict  objectForKey:@"zone_id"];
                        objLoc.strLocationName = [dict  objectForKey:@"name"];
                        [marrZone  addObject:objLoc];
                        if (strZoneID.integerValue == objLoc.strLocationId.integerValue) {
                            objlocSelectedZone.strLocationName = objLoc.strLocationName;
                        }
                    }
                    
                    //ZONE NAME
                    self.txfdState.text = objlocSelectedZone.strLocationName;
                    
                }
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg type:TSMessageNotificationTypeError];
            }
        }
    }];
}





#pragma mark webserviceCallForZone
-(void)webserviceCallForZones:(NSString *)strCountryId
{
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait Fetching Zones"];
    NSDictionary * dictPara = @{@"action":@"getzone",@"country_id":strCountryId};
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView   removeViewAnimated:YES];
        
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //SUCCESS
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                
                //RESPONSE
                NSLog(@"%@",result);
                
                
                //Zone NAMES
                NSArray * arrZoneNames = [result  objectForKey:@"Result"] ;
                if (marrZone) {
                    [marrZone   removeAllObjects];
                }
                else
                {
                    marrZone = [NSMutableArray   new];
                }
                
                for (NSDictionary * dict in arrZoneNames) {
                    Location * objLocation = [Location  new];
                    objLocation.strLocationId = [dict  objectForKey:@"zone_id"];
                    objLocation.strLocationName = [dict  objectForKey:@"name"];
                    [marrZone  addObject:objLocation];
                }
                
                
                self.txfdState.text = @"";
                objlocSelectedZone = nil;
                
                UIPickerView * pick = (UIPickerView*)self.txfdState.inputView;
                [pick   reloadAllComponents];
                
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeError];
            }
        }
    }];
}





@end
