//
//  CartModel.h
//  kazumi
//
//  Created by Aditya Kumar on 12/10/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CartModel : NSObject
@property (strong, nonatomic) NSString * strCartId;
@property (strong, nonatomic) NSString * strProductId;
@property (strong, nonatomic) NSString * strProductQuant;
@property (strong, nonatomic) NSString * strProductName;
@property (strong, nonatomic) NSString * strProductImgUrl;
@property (strong, nonatomic) NSString * strProductPrice;
@property (strong, nonatomic) NSArray * arrOptions;
@end
