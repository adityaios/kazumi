//
//  CatModel.m
//  kazumi
//
//  Created by Yashvir on 19/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "CatModel.h"

@implementation CatModel
- (id)init {
    self = [super init];
    if (self) {
        _catId = [[NSString alloc] init];
        _catImgUrl = [[NSString alloc] init];
        _catName = [[NSString alloc] init];
    }
    return self;
}

@end
