//
//  WishListViewController.h
//  kazumi
//
//  Created by NM8 on 01/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "Network.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "GlobalValues.h"
#import "CatModel.h"
#import "UIImageView+AFNetworking.h"
#import "KazumiMacros.h"
#import "Constant.h"
#import "ProductDetailsViewController.h"
#import "WishListViewCell.h"


@interface WishListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
 NSMutableArray * marrCatModelData;
GlobalValues * objglobalValues;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

//Methods
-(void)CustomBackButton;
-(void)setUpUI;
-(void)intializerMethod;
-(void)webserviceCallForproductwishlist;
-(void)webserviceCallForproductwishlistDelete:(CatModel*)objCatModel;

@end
