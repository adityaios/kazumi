//
//  ReviewsListViewController.m
//  kazumi
//
//  Created by Yashvir on 08/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "ReviewsListViewController.h"

@interface ReviewsListViewController ()

@end

@implementation ReviewsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // getreviews	product_id
    [self  intializerMethod];
    [self  viewSetUp];
    [self  customBackButton];
    if ([Reachability isConnected]) {
         [self  webserviceCallForProductReviews];
     }
}


#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
   
    NSString * strTitle = [NSString  stringWithFormat:@"%@",self.objProductModeleReview.strProduct_Name];
    self.title = strTitle;
    
    
    UINib *nib = [UINib nibWithNibName:@"ReviewTableViewCell" bundle:nil];
    [self.tblvReview registerNib:nib forCellReuseIdentifier:@"ReviewCell"];
    
    
    self.tblvReview.estimatedRowHeight = 100.0;
    self.tblvReview.rowHeight = UITableViewAutomaticDimension;
    self.tblvReview.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    
}



#pragma mark
#pragma mark setUpUI
#pragma mark
-(void)viewSetUp
{
    self.automaticallyAdjustsScrollViewInsets = NO;
}



#pragma mark
#pragma mark  customBackButton
#pragma mark

-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}

- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark
#pragma mark webserviceCallForProductReviews
#pragma mark
-(void)webserviceCallForProductReviews
{
    //LOADER
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSString * strProductId = self.objProductModeleReview.strProduct_id;
    
    //PARAMETER DICT
    NSDictionary *dictPara = @{@"action" : @"getreviews",@"product_id":strProductId};
    NSLog(@"dictPara = %@",dictPara);
    
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
        //REMOVE LOADER
        [DejalBezelActivityView  removeViewAnimated:YES];
        
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSLog(@" %@",result);
                //SUCESS MESSAGE
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Product Details" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
                
                //DATA PARSING
                marrReviewData = [NSMutableArray  new];
                for (NSDictionary * dict in [result  objectForKey:@"Result"]) {
                    [marrReviewData  addObject:dict];
                }
                [self.tblvReview reloadData];
   }
            else
            {
            //ERROR MESSAGE
            NSString * strmsg = [result   objectForKey:@"msg"];
            [TSMessage  showNotificationInViewController:self title:@"Product Details" subtitle:strmsg  type:TSMessageNotificationTypeError];
                
            }
        }
    }];
}





#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return marrReviewData?marrReviewData.count:0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReviewTableViewCell * cell =  [tableView dequeueReusableCellWithIdentifier:@"ReviewCell"];
    
    cell.view.layer.borderColor = UIColor.grayColor.CGColor;
    cell.view.layer.borderWidth = 0.3;
    
    
  NSDictionary * dictReview = [marrReviewData  objectAtIndex:indexPath.row];
    
    
    //STAR VIEW
    if ([dictReview  objectForKey:@"rating"] != nil) {
        NSString * strRating = [NSString  stringWithFormat:@"%@",[dictReview  objectForKey:@"rating"]];
        cell.viewStar.canEdit = NO;
        cell.viewStar.maxRating = 05;
        cell.viewStar.rating = strRating.integerValue;
    }
    
    
    //AUTHOR
    if ([dictReview  objectForKey:@"author"] != nil) {
        NSString * strAuthor = [NSString  stringWithFormat:@"%@",[dictReview  objectForKey:@"author"]];
        cell.lblName.textColor = UIColorFromRGBWithAlpha(kazumiRedColor,1.0);
        cell.lblName.text = strAuthor;
    }
    
    //DATE
    if ([dictReview  objectForKey:@"date_added"] != nil) {
        NSString * strDate = [NSString  stringWithFormat:@"%@",[dictReview  objectForKey:@"date_added"]];
        cell.lblDate.textColor = [UIColor  lightGrayColor];
        cell.lblDate.text = strDate;
    }
    
    //TEXT
    if ([dictReview  objectForKey:@"review_text"] != nil) {
        NSString * strText = [NSString  stringWithFormat:@"%@",[dictReview  objectForKey:@"review_text"]];
        cell.lblText.text = strText;
    }
    
    return cell;
}




-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    NSString * strTitle = @"Reviews";
    return strTitle;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}



@end
