//
//  PrivacyPolicyViewController.h
//  kazumi
//
//  Created by Yashvir on 20/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Reachability.h"
#import "Network.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "MKNumberBadgeView.h"
#import "CartViewController.h"


@interface PrivacyPolicyViewController : UIViewController
{
   GlobalValues * objglobalValues;

}
@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButtonPrivacy;
@property (weak, nonatomic) IBOutlet UIWebView *webViewPrivacy;

//METHODS
-(void)setUpUI;
-(void)intializerMethod;
-(void)webserviceCallPrivacy;
-(void)setWebViewUsingHtlml:(NSMutableString*)strhtml;



@end
