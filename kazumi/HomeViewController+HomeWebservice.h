//
//  HomeViewController+HomeWebservice.h
//  kazumi
//
//  Created by Yashvir on 19/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "HomeViewController.h"

@interface UIViewController (HomeWebservice)
//METHODS
-(void)getCategoryListForHome;

@end
