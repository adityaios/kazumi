//
//  ProductListViewController.m
//  kazumi
//
//  Created by Yashvir on 26/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "ProductListViewController.h"

@interface ProductListViewController ()

@end

@implementation ProductListViewController


#pragma mark
#pragma mark VIEW CONTROLLER LIFE CYCLE
#pragma mark


#pragma mark View Did Load
- (void)viewDidLoad {
    [super viewDidLoad];
    [self  intializerMethod];
    [self  setUpUI];
    
    //WEBSERVICE CALL FOR PRODUCTS
    [self  webserviceCallForProducts];
    
}


#pragma mark View Did Appear
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self cartBarButton];
}



#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    objglobalValues = [GlobalValues  sharedManager];
    self.title = self.objCatModelPass.catName;
    intWebPagination = 1;
    
    
    //Grid To List Flag
    //Grid - FALSE
    //List - TRUE
    flagProductGridList = FALSE;
    
    
    
    
    //COLLECTION VIEW
    
    //COLLECTION VIEW LAYOUT
    UICollectionViewFlowLayout *layout =
    [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.collectionViewProduct setCollectionViewLayout:layout];
    
    //COLLECTION FOOTER
    [self.collectionViewProduct registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:UICollectionElementKindSectionFooter];
    
    //COLLECTION HEADER
    [self.collectionViewProduct registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:UICollectionElementKindSectionHeader];
    
    //Set Main Cell in Collection View
    [self.collectionViewProduct registerNib:[UINib nibWithNibName:@"ProductCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ProductCustomCell"];
    
    
    // ProductSingleListCollectionViewCell
    // ProductSingleCustomCell
    //Product Single List Collection Cell
    [self.collectionViewProduct registerNib:[UINib nibWithNibName:@"ProductSingleListCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ProductSingleCustomCell"];
    
    
}





#pragma mark
#pragma mark SET UI
#pragma mark
-(void)setUpUI
{
    //BACK BUTTON
    [self   customBackButton];
    self.collectionViewProduct.frame = CGRectMake(0,44, SelfViewWidth, SelfViewHeight);
    [self.collectionViewProduct setContentOffset:CGPointMake(0,44)];
    //SEARCH BAR
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,-44, CGRectGetWidth(self.collectionViewProduct.frame), 44)];
    self.searchBar.delegate = self;
    [self.collectionViewProduct addSubview:self.searchBar];
    
}

#pragma mark Custom Back Btn
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}



#pragma mark cartBarButton
-(void)cartBarButton
{
    
    
    MKNumberBadgeView *number = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(25, -10,20,20)];
    number.value = objglobalValues.strCartCount.integerValue;
    number.shadow = NO;
    number.shine = YES;
    number.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:14];
    number.strokeColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    number.layer.cornerRadius = 2.0;
    
    
    //CART
    UIImage * imgcart = [UIImage  imageNamed:@"cart"];
    // Allocate UIButton
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0,0,30,30);
    [btn setImage:imgcart forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:number];
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
    
    /*
     //FILTER Bar Button
     UIImage * imgFilter = [UIImage  imageNamed:@"filter"];
     UIButton *btnFilter = [UIButton  buttonWithType:UIButtonTypeCustom];
     btnFilter.frame = CGRectMake(0,0,30,30);
     [btnFilter setImage:imgFilter forState:UIControlStateNormal];
     [btnFilter addTarget:self action:@selector(filterButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem * barbtnFilter = [[UIBarButtonItem alloc] initWithCustomView:btnFilter];
     self.navigationItem.rightBarButtonItems = @[proe,barbtnFilter];
     */
}




#pragma mark
#pragma mark  IBACTION
#pragma mark

#pragma mark  Cart Btn
-(IBAction)cartButtonClicked:(id)sender
{
    if (objglobalValues.strCartCount.integerValue == 0) {
        [TSMessage  showNotificationInViewController:self title:@"Cart" subtitle:@"No Product Available in Cart" type:TSMessageNotificationTypeMessage];
    }
    else
    {
        CartViewController * objCartVC = [self.storyboard   instantiateViewControllerWithIdentifier:@"CartViewController"];
        [self.navigationController pushViewController:objCartVC animated:YES];
        
    }
}


#pragma mark  Filter Btn
-(IBAction)filterButtonClicked:(id)sender
{
    if (marrProductsList.count == 0) {
        [TSMessage  showNotificationInViewController:self title:@"No products found for this Category" subtitle:nil  type:TSMessageNotificationTypeWarning];
    }
    else
    {
        [self  performSegueWithIdentifier:ksegue_segue_FilterOptions sender:nil];
    }
}



#pragma mark  Custom Back Btn
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark  Load More Btn
-(IBAction)btnLoadMoreSend:(id)sender{
    NSLog(@"btn clicked");
    [self  webserviceCallForProducts];
    
}

#pragma mark  Grid Btn
-(IBAction)gridButtonClicked:(UIButton*)sender{
    NSLog(@"grid Button Clicked");
    if (flagProductGridList) {
        flagProductGridList = FALSE;
        [self.collectionViewProduct   reloadData];
    }
    else
    {
        
        flagProductGridList = TRUE;
        [self.collectionViewProduct   reloadData];
    }
}






#pragma mark
#pragma mark - WEBSERVICE CALL
#pragma mark

#pragma mark - product webservice
-(void)webserviceCallForProducts
{
    
    
    //    getproductbycategory
    //    cat_id
    //    customer_group_id		user type like 1 for normal user, 2 for wholeseller
    //    start		            for pagination like 1 for first page etc
    
    
    NSString * strCatid = self.objCatModelPass.catId;
    NSString * strCatName = self.objCatModelPass.catName;
    NSLog(@"strCatid = %@",strCatid);
    NSLog(@"strCatName = %@",strCatName);
    
    NSString * strWebPagination = [NSString  stringWithFormat:@"%ld",(long)intWebPagination];
    NSLog(@"strWebPagination = %@",strWebPagination);
    
    if ([Reachability isConnected]) {
        //LOADER
        [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
        
        //PARAMETER DICT
        NSDictionary *dictPara = @{@"action" : @"getproductbycategory",@"cat_id":strCatid,@"customer_group_id":objglobalValues.strCustomerGroupId,@"start":strWebPagination};
        NSLog(@"dictPara = %@",dictPara);
        
        
        //NETWORK REQUEST
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
            //REMOVE LOADER
            [DejalBezelActivityView  removeViewAnimated:YES];
            
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                //CHECK STATUS CODE
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    
                    //SUCCESS MSG
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                    
                    //RESPONSE
                    NSLog(@"%@",result);
                    
                    NSArray * arrJsonResponse = [result  objectForKey:@"Result"];
                    
                    
                    
                    //Webservice Json Parsing
                    [self  webserviceResponseParsing:arrJsonResponse];
                    
                    
                }
                else
                {
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage showNotificationWithTitle:@"Error"
                                                subtitle:strmsg
                                                    type:TSMessageNotificationTypeError];
                    
                }
            }
            
        }];
        
    }
    
}



-(void)webserviceResponseParsing:(NSArray *)arrResponse
{
    
    if (marrProductsList.count == 0) {
        marrProductsList = [[NSMutableArray  alloc] init];
    }
    else
    {
        
    }
    
    
    for (NSDictionary * dict in arrResponse) {
        NSLog(@"dict = %@",dict);
        ProductModelClass * objModelProduct = [ProductModelClass  new];
        objModelProduct.strActual_price = [dict  objectForKey:@"actual_price"];
        objModelProduct.strDiscount_type = [dict  objectForKey:@"discount_type"];
        objModelProduct.strDiscount_value = [dict  objectForKey:@"discount_value"];
        objModelProduct.strDiscounted_amount = [dict  objectForKey:@"discounted_amount"];
        objModelProduct.strProduct_id = [dict  objectForKey:@"product_id"];
        objModelProduct.strProduct_Name = [dict  objectForKey:@"product_name"];
        //Product Image
        if ([Helper_Class  nullValueCheck:[dict  objectForKey:@"product_image"]]) {
            objModelProduct.strProduct_image = @"";
        }
        else
        {
            objModelProduct.strProduct_image = [dict  objectForKey:@"product_image"];
        }
        
        
        
        [marrProductsList     addObject:objModelProduct];
    }
    
    [self.collectionViewProduct  reloadData];
    
    if (arrResponse.count >= 1) {
        intWebPagination = intWebPagination+1;
        
    }
}


#pragma mark - webservice Call ForSearching Products
-(void)webserviceCallForSearchingProducts:(NSString *)strSearchtext
{
    /*
     getsearchproduct
     search_text		           to search products
     customer_group_id
     */
    
    
    [DejalBezelActivityView  activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary *dictPara = @{@"action" : @"getsearchproduct",@"search_text":strSearchtext,@"customer_group_id":objglobalValues.strCustomerId};
    
    [Network   getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
        //REMOVE LOADER
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                
                //RESPONSE
                NSLog(@"%@",result);
                NSArray * arrJsonResponse = [result  objectForKey:@"Result"];
                NSLog(@"arrJsonResponse =  %@",arrJsonResponse);
                //Webservice Json Parsing
                
                [marrProductsList  removeAllObjects];
                [self  webserviceResponseParsing:arrJsonResponse];
                
            }
            
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage showNotificationWithTitle:@"Error"
                                            subtitle:strmsg
                                                type:TSMessageNotificationTypeError];
                
            }
            
        }
    }];
}






#pragma mark
#pragma mark - UICOLLECTION VIEW DATA SOURCE
#pragma mark


#pragma mark - number of items
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return marrProductsList?marrProductsList.count:0;
}

#pragma mark - cell for row at indexpath
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell * tcell;
    
    
    if (flagProductGridList) {
        //CUSTOM CELL
        tcell = (ProductSingleListCollectionViewCell *)[collectionView  dequeueReusableCellWithReuseIdentifier:@"ProductSingleCustomCell" forIndexPath:indexPath];
        
    }
    else
    {
        
        //CUSTOM CELL
        tcell = (ProductCollectionViewCell *)[collectionView  dequeueReusableCellWithReuseIdentifier:@"ProductCustomCell" forIndexPath:indexPath];
        
    }
    
    
    ProductModelClass * objModelClass = [ProductModelClass  new];
    objModelClass = [marrProductsList  objectAtIndex:indexPath.row];
    objModelClass.strProduct_Name  = [objModelClass.strProduct_Name  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
   //PRODUCT IMAGEVIEW
    UIImageView * imgViewProduct = (UIImageView *)[tcell  viewWithTag:100];
    NSLog(@"objModelClass.strProduct_image = %@",objModelClass.strProduct_image);
    [imgViewProduct setImageWithURL:[NSURL  URLWithString:objModelClass.strProduct_image] placeholderImage:[UIImage  imageNamed:@"placeholder"]];
    
    
   
    //LABEL NAME
    UILabel * lblname = (UILabel*)[tcell  viewWithTag:200];
    
    //4 Words Name
    objModelClass.strProduct_Name = [objModelClass.strProduct_Name stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
    
    NSArray * arrComp = [objModelClass.strProduct_Name componentsSeparatedByString:@" "];
    
    if (arrComp.count >= 4) {
        NSInteger nWords = 4;
        NSRange wordRange = NSMakeRange(0, nWords);
        NSArray * firstWords = [arrComp subarrayWithRange:wordRange];
        NSString * strResultProductName = [firstWords componentsJoinedByString:@" "];
        lblname.text = strResultProductName;
    }
    else
    {
        lblname.text = objModelClass.strProduct_Name;
    }
    
    
    
    
    
    if (objModelClass.strDiscount_value.integerValue == 0) {
        
        //PRICE
        UILabel * lblprice = (UILabel*)[tcell  viewWithTag:300];
        lblprice.text = [NSString  stringWithFormat:@"Rs %.02f",[objModelClass.strActual_price floatValue]];
        
        
        
        //DISCOUNT
        UILabel * lbldiscount = (UILabel*)[tcell  viewWithTag:400];
        lbldiscount.text =  @"";
        
        
        //DISCOUNT VALUE
        UILabel * lbldiscountprice = (UILabel*)[tcell  viewWithTag:500];
        lbldiscountprice.text = @"";
        
    }
    else
    {
        
        
        UILabel * lblprice = (UILabel*)[tcell  viewWithTag:300];
        
        UILabel * lbldiscount = (UILabel*)[tcell  viewWithTag:400];
        lbldiscount.textColor = [UIColor  darkGrayColor];
        lbldiscount.font = [UIFont  fontWithName:kfontDefaultItalics size:14];
        
        
        UILabel * lbldiscountprice = (UILabel*)[tcell  viewWithTag:500];
        lbldiscountprice.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
        //lbldiscountprice.textColor = UIColorFromRGBWithAlpha(kazumiRedColor,1.0);
        lbldiscountprice.textColor = [UIColor blackColor];
        
        
        
        
        //PRICE
        NSString * strActualPrice = [NSString  stringWithFormat:@"Rs %.02f",[objModelClass.strActual_price floatValue]];
        
        
        
        NSMutableAttributedString *attributeStringPrice = [[NSMutableAttributedString alloc] initWithString:strActualPrice];
        
        
        
        //ATTRIBUTE STRIKE
        [attributeStringPrice addAttribute:NSStrikethroughStyleAttributeName
                                     value:@2
                                     range:NSMakeRange(0, [attributeStringPrice length])];
        
        
        //FONT ATTRIBUTE
        UIFont *font = [UIFont fontWithName:kFontSFUIDisplay_Light size:12.0];
        NSDictionary *  attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                      forKey:NSFontAttributeName];
        [attributeStringPrice addAttributes:attrsDictionary range:NSMakeRange(0, [attributeStringPrice length])];
        lblprice.attributedText = attributeStringPrice;
        
        
        
        //DISCOUNT TYPE
        NSString * strdiscount_type = [NSString  stringWithFormat:@"%@",objModelClass.strDiscount_type];
        
        if ([strdiscount_type caseInsensitiveCompare:kDiscountTypeAppliedRs] == NSOrderedSame)
        {
            
            lbldiscount.text = [NSString  stringWithFormat:@"Rs %.02f off",[objModelClass.strDiscount_value floatValue]];
            
        }
        else
        {
            lbldiscount.text = [NSString  stringWithFormat:@"%@ %% off",objModelClass.strDiscount_value];
            
        }
        
        lbldiscountprice.text = [NSString  stringWithFormat:@"Rs %.02f",[objModelClass.strDiscounted_amount floatValue]];
        
        
    }
    
    
    return tcell;
}





#pragma mark - didSelectItemAtIndexPath
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ProductModelClass * objProductModel = [marrProductsList  objectAtIndex:indexPath.row];
    NSLog(@"Product Image = %@",objProductModel.strProduct_image);
    
    
    
    if ([objProductModel.strProduct_image caseInsensitiveCompare:@""] == NSOrderedSame) {
        [TSMessage  showNotificationInViewController:self title:@"Message" subtitle:@"No Product Images Found" type:TSMessageNotificationTypeError];
    }
    else
    {
        
        
        NSLog(@"Url Validated");
        ProductDetailsViewController * objProductDetailsVC =    [self.storyboard   instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        objProductDetailsVC.objProductModelPass = objProductModel;
        [self.navigationController pushViewController:objProductDetailsVC animated:YES];
        
    }
}



#pragma mark
#pragma mark - UICollectionViewDelegateFlowLayout
#pragma mark


#pragma mark - size for item at Indexpath
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize  sizeCell;
    if (flagProductGridList) {
        sizeCell = CGSizeMake(SelfViewWidth,120);
    }
    else
    {
        sizeCell = CGSizeMake(150, 170);
        if (IS_IPHONE_5) {
            sizeCell =  CGSizeMake(155,245);
        }
        else if (IS_IPHONE_6){
            sizeCell =  CGSizeMake(180,270);
        }
        
    }
    
    return sizeCell;
    
    
}


#pragma mark - refrence size for footer
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(SelfViewWidth, 120.f);
    
}


#pragma mark - refrence size for Header
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(SelfViewWidth, 50.f);
    
}


#pragma mark - view for Supplementary Element Of Kind
-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview ;
    if (kind == UICollectionElementKindSectionFooter) {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:UICollectionElementKindSectionFooter     forIndexPath:indexPath];
        
        //LOAD MORE
        btnLoadMore = [UIButton buttonWithType:UIButtonTypeCustom];
        btnLoadMore.frame = CGRectMake(10,05,SelfViewWidth-20,40.0);
        [btnLoadMore  addTarget:self action:@selector(btnLoadMoreSend:) forControlEvents:UIControlEventTouchUpInside];
        
        btnLoadMore.layer.borderWidth = 2.0;
        btnLoadMore.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
        btnLoadMore.layer.borderColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0).CGColor;
        btnLoadMore.layer.cornerRadius = kButtonCornerRadius;
        btnLoadMore.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
        [btnLoadMore setTitle:@"Show More" forState:UIControlStateNormal];
        
        [reusableview addSubview:btnLoadMore];
        if (marrProductsList.count==0) {
            [btnLoadMore  removeFromSuperview];
            
            
            self.lblNoProduct = [[UILabel alloc] initWithFrame:CGRectMake(20,30,SelfViewWidth-20,40.0)];
            self.lblNoProduct.textAlignment = NSTextAlignmentCenter;
            self.lblNoProduct.text = @"No Products Available";
            self.lblNoProduct.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:15];
            self.lblNoProduct.textColor = [UIColor grayColor];
            [reusableview addSubview:self.lblNoProduct];
            
        }else
        {
            [self.lblNoProduct   removeFromSuperview];
        }
        
        
        reusableview.backgroundColor = [UIColor   whiteColor];
    }
    
    else if (UICollectionElementKindSectionHeader)
    {
        
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:UICollectionElementKindSectionHeader     forIndexPath:indexPath];
        reusableview.backgroundColor = [UIColor    clearColor];
        
        //FILTER BTN
        UIImage * imgFilter = [UIImage  imageNamed:@"filter"];
        UIButton *btnFilter = [UIButton  buttonWithType:UIButtonTypeCustom];
        btnFilter.frame = CGRectMake(SelfViewWidth - 40,10,30,30);
        [btnFilter setImage:imgFilter forState:UIControlStateNormal];
        [btnFilter addTarget:self action:@selector(filterButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [reusableview addSubview:btnFilter];
        if (marrProductsList.count==0) {
            [btnFilter  removeFromSuperview];
        }
        
        //GRID BTN
        UIImage * imgGrid = [UIImage  imageNamed:@"grid"];
        UIImage * imgList = [UIImage  imageNamed:@"List"];
        
        
        if (btnGrid) {
            [btnGrid removeFromSuperview];
            btnGrid = nil;
        }
        btnGrid = [UIButton  buttonWithType:UIButtonTypeCustom];
        btnGrid.frame = CGRectMake(10,10,30,30);
        [btnGrid setImage:nil forState:UIControlStateNormal];
        
        if (flagProductGridList) {
            [btnGrid setImage:imgList forState:UIControlStateNormal];
        }
        else
        {
            [btnGrid setImage:imgGrid forState:UIControlStateNormal];
        }
        
        
        
        
        [btnGrid addTarget:self action:@selector(gridButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [reusableview addSubview:btnGrid];
        
        if (marrProductsList.count==0) {
            [btnGrid  removeFromSuperview];
        
    }
    }
    return reusableview;
}





#pragma mark
#pragma mark - PREPARE FOR SEGUE
#pragma mark
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:ksegue_segue_FilterOptions]) {
        FilterOptionsViewController * objFilterVC = segue.destinationViewController;
        objFilterVC.objCatModelFilter = self.objCatModelPass;
        objFilterVC.FilterDelegateObject = self;
    }
}



#pragma mark
#pragma mark - FILTER DELEGATE
#pragma mark
-(void)sendFilteredProductsBackToProductListController:(NSMutableArray *)marrProducts
{
    if (marrProducts.count >= 1) {
        [self.navigationController popViewControllerAnimated:YES];
        [marrProductsList removeAllObjects];
        marrProductsList = [marrProducts   mutableCopy];
        [self.collectionViewProduct   reloadData];
        
    }
}




#pragma mark
#pragma mark - SERACH BAR DELEGATE
#pragma mark


#pragma mark - Serach Bar Button Clicked
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"GO");
    [self.searchBar  resignFirstResponder];
    if ([Reachability isConnected]) {
        [self   webserviceCallForSearchingProducts:self.searchBar.text];
    }
}


#pragma mark - Serach bar Text Did Change
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"%@",searchText);
    
}

#pragma mark - Scroll View Begin Dragging
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}





@end
