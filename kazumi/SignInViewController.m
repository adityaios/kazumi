//
//  SignInViewController.m
//  kazumi
//
//  Created by Yashvir on 14/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "SignInViewController.h"

@interface SignInViewController ()

@end

@implementation SignInViewController


#pragma mark
#pragma mark VIEW CONTROLLER CYCLE
#pragma mark


- (void)viewDidLoad {
    [super viewDidLoad];
    [self   setUpUI];
    [self   intializerMethod];
    
}


#pragma mark
#pragma mark SET UP UI
#pragma mark

-(void)setUpUI
{
    
    //NAVIGATION BAR
    self.navigationController.navigationBar.barTintColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    self.title = @"Get Started";
    
    
    
    
    //BG IMAGE VIEW
    self.bgImageView.frame = CGRectMake(0,64,SelfViewWidth,SelfViewHeight);
    
    
    int Xpadding = 10;
    int Ypadding = 8;
    int Y = Ypadding+80;
    
    
    //TEXTFIELD USER NAME
    self.txfdUserName.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView *paddingViewfirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
   // paddingViewfirstName.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    
    self.txfdUserName.leftView = paddingViewfirstName;
    self.txfdUserName.leftViewMode = UITextFieldViewModeAlways;
    
    
    Y = Y + Ypadding +50;
    
    //TEXTFIELD PASSWORD
    self.txfdpswd.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    UIView * paddingViewPswd = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    //paddingViewPswd.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewfirstName.backgroundColor = [UIColor   whiteColor];
    
    
    
    self.txfdpswd.leftView = paddingViewPswd;
    self.txfdpswd.secureTextEntry = YES;
    
    
    self.txfdpswd.leftViewMode = UITextFieldViewModeAlways;
    
    Y = Y + Ypadding +40;
    
    //BUTTON FORGET PSWD
    self.btnForgetPswd.frame = CGRectMake(self.txfdpswd.frame.origin.x +self.txfdpswd.frame.size.width - 130,Y,130,25);
    [self.btnForgetPswd   setTitleColor:UIColorFromRGBWithAlpha(kazumiRedColor, 1) forState:UIControlStateNormal];
    
    Y = Y + Ypadding +25;
    
    //BUTTON LOGIN
    self.btnLogin.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    //self.btnLogin.layer.borderWidth = 2.0;
    self.btnLogin.layer.cornerRadius = kButtonCornerRadius;
   // self.btnLogin.layer.borderColor =  [UIColor  orangeColor].CGColor;
    self.btnLogin.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
    
    self.imgViewArrowLogin.frame = CGRectMake(self.btnLogin.frame.origin.x+self.btnLogin.frame.size.width - 50, Y+04, 32, 32);
    
    
    
    Y = Y + Ypadding +40;
    
    //IMAGEVIEW DIVIDER
    self.imgViewdivider.frame = CGRectMake(Xpadding/2, Y, SelfViewWidth - Xpadding, 25);
    
    Y = Y + Ypadding +25;
    
    //LOGIN WITH CLICK
    self.lblSingleClick.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 22);
    self.lblSingleClick.textColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    self.lblSingleClick.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:14];
    self.lblSingleClick.text = @"Login With a Single Click";
    
    Y = Y + Ypadding +22;
    
    
    //FB LOGIN
    self.FBlogin.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
    
    
    Y = Y + Ypadding +35;
    
    //GOOGLE SIGIN
    self.viewGoogleSignin.frame = CGRectMake(Xpadding-02,Y,SelfViewWidth-(2*Xpadding-04), 0);
    self.viewGoogleSignin.style = kGIDSignInButtonStyleWide;
    self.viewGoogleSignin.colorScheme = kGIDSignInButtonColorSchemeDark;
    
    
    Y = Y + Ypadding *2 +40;
    
    //REGISTER BUTTON
    self.btnRegister.frame = CGRectMake(Xpadding,Y,SelfViewWidth-2*Xpadding, 40);
   // self.btnRegister.layer.borderWidth = 2.0;
    self.btnRegister.layer.cornerRadius = kButtonCornerRadius;
    
    self.btnRegister.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
   // self.btnRegister.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnRegister.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
    
    self.imgViewArrowRegister.frame = CGRectMake(self.btnRegister.frame.origin.x+self.btnRegister.frame.size.width - 50, Y+04, 32, 32);
    
    
}



#pragma mark
#pragma mark INTIALIZER METHOD
#pragma mark

-(void)intializerMethod
{
    
    objglobalValues = [GlobalValues   sharedManager];
    //DEVICE ID
    UIDevice *device = [UIDevice currentDevice];
    NSString  *strDeviceID = [[device identifierForVendor]UUIDString];
    objglobalValues.strDeviceID = strDeviceID;
    
    mdictSocial = [[NSMutableDictionary alloc]  init];
    //GOOGLE SIGIN
    [self  setGoogleSignIn];
    
}



#pragma mark
#pragma mark  TEXTFIELD DELEGATE
#pragma mark
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField  resignFirstResponder];
    return YES;
}





#pragma mark
#pragma mark  IBACTION
#pragma mark

#pragma mark FORGET PASSWORD
- (IBAction)btnForgetPswd:(UIButton *)sender {
    ForgotPasswordViewController * objForgotPsedVC =   [[ForgotPasswordViewController  alloc] initWithNibName:@"ForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:objForgotPsedVC animated:YES];
}



#pragma mark FACEBOOK LOGIN CLICKED
- (IBAction)btnFacebookloginClicked:(id)sender {
    if ([FBSDKAccessToken currentAccessToken]) {
        [self  fbGraphAPICall];
    }
    else
    {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login
         logInWithReadPermissions: @[@"public_profile",@"email"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if (error) {
                 NSLog(@"Process error");
             } else if (result.isCancelled) {
                 NSLog(@"Cancelled");
             } else {
                 NSLog(@"Logged in");
                 if ([FBSDKAccessToken currentAccessToken]) {
                     [self  fbGraphAPICall];
                 }
             }
         }];
    }
}



-(void)fbGraphAPICall
{
    //GRAPH API CALL
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id,name,email" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (error) {
             NSLog(@"Error :%@", error);
         }
         else
         {
             NSLog(@"fetched user:%@", result);
             
             /*
              fetched user:{
              email = "aditya.kumar@nishkrant.com";
              id = 156491954709359;
              name = "Aditya Kumar";
              }
              */
             
             
             NSString * strUserID = [result  objectForKey:@"id"];
             NSString * strName = [result  objectForKey:@"name"];
             NSString * strEmail = [result  objectForKey:@"email"];
             
             [mdictSocial  setObject:kSocialTypeFaceBook forKey:kSocialType];
             [mdictSocial  setObject:strUserID forKey:@"userId"];
             [mdictSocial  setObject:strName forKey:@"name"];
             [mdictSocial  setObject:strEmail forKey:@"email"];
             
             //CHECK FB LOGIN
             [self  webserviceCallForFaceBookLoginCheck:strUserID];
         }
     }];
}


#pragma mark WEBSERVICE- FB Login Check
-(void)webserviceCallForFaceBookLoginCheck:(NSString *)strFbId
{
    //    socialLoginCheck
    //    gplus_id
    //
    
    //WEBSERVICE CALL
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary *dictPara = @{@"action":@"socialLoginCheck",@"fb_id" : strFbId};
    
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            if ([[result  objectForKey:@"statusCode"] integerValue] == 0) {
                
                NSLog(@"result = %@",result);
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Social Login" subtitle:strmsg type:TSMessageNotificationTypeError];
                [self   performSegueWithIdentifier:ksegue_socialLogin sender:nil];
                
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Social Login"  subtitle:strmsg type:TSMessageNotificationTypeSuccess];
                NSLog(@"result = %@",result);
                //GOOGLE SIGN IN SUCCESS RESPONSE
                if ([[result   allKeys]  containsObject:@"Result"]) {
                    NSDictionary * dictResult = [[NSDictionary  alloc] init];
                    dictResult = [result   objectForKey:@"Result"];
                    //SESSION ID
                    if ([[dictResult   allKeys]  containsObject:@"session_id"])
                    {
                        
                        objglobalValues.strCustomerGroupId = [dictResult   objectForKey:@"customer_group_id"];
                        objglobalValues.strCustomerId = [dictResult   objectForKey:@"user_id"];
                        objglobalValues.strFirstName = [dictResult   objectForKey:@"name"];
                        objglobalValues.strSessionID = [dictResult   objectForKey:@"session_id"];
                        objglobalValues.strCartCount = [dictResult   objectForKey:@"cart_count"];
                        [self  performSegueWithIdentifier:ksegueLoginSuccess sender:nil];
                    }
                }
            }
        }
    }];
}
















#pragma mark LOGIN CLICKED
- (IBAction)btnLoginClicked:(UIButton *)sender {
    self.txfdUserName.text = [self.txfdUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.txfdpswd.text = [self.txfdpswd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    //VALIDATIONS DOUBLE SPACE
    //USER NAME
    while ([self.txfdUserName.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdUserName.text = [self.txfdUserName.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    //PASSWORD
    while ([self.txfdpswd.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdpswd.text = [self.txfdpswd.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    
    
    if (self.txfdUserName.text.length == 0 || self.txfdpswd.text.length == 0 ) {
        [TSMessage showNotificationWithTitle:@"Validation Error"
                                    subtitle:@"Please Fill all Fields"
                                        type:TSMessageNotificationTypeError];
        return;
        
    }
    
    
    if ([Reachability isConnected]) {
        [self  webserviceCallForLogin];
    }
    
}



#pragma mark
#pragma mark  WEBSERVICE
#pragma mark

#pragma mark LOGIN WEBSERVICE
-(void)webserviceCallForLogin
{
    //WEBSERVICE CALL
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary *dict = @{@"action" : @"login",@"user_name" : self.txfdUserName.text,@"user_password" : self.txfdpswd.text};
    
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
        if (error) {
            [DejalBezelActivityView  removeViewAnimated:YES];
            NSLog(@"%@",error);
        }
        else
        {
            [DejalBezelActivityView  removeViewAnimated:YES];
            NSLog(@"%@",result);
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                //NSString * strmsg = [result   objectForKey:@"msg"];
                NSDictionary * dictResult = [result   objectForKey:@"Result"];
                //SetUpGlobalValuesOnLoginSuccess
                [self  setUpGlobalValuesOnLoginSuccess:dictResult];
                /*
                 [TSMessage showNotificationWithTitle:@"msg"
                 subtitle:strmsg
                 type:TSMessageNotificationTypeSuccess];
                 */
                [self  performSegueWithIdentifier:ksegueLoginSuccess sender:nil];
                
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage showNotificationWithTitle:@"Login Failed"
                                            subtitle:strmsg
                                                type:TSMessageNotificationTypeError];
            }
        }
    }];
    
}




#pragma mark SET GLOBAL VALUES
-(void)setUpGlobalValuesOnLoginSuccess:(NSDictionary *)dictResults
{
    
    objglobalValues.strCustomerGroupId = [dictResults   objectForKey:@"customer_group_id"];
    objglobalValues.strCustomerId = [dictResults   objectForKey:@"customer_id"];
    objglobalValues.strFirstName = [dictResults   objectForKey:@"name"];
    objglobalValues.strSessionID = [dictResults   objectForKey:@"session_id"];
    objglobalValues.strCartCount = [dictResults   objectForKey:@"cart_count"];
    
    //NIL TEXTFIELDS
    self.txfdUserName.text = @"";
    self.txfdpswd.text = @"";
}






#pragma mark
#pragma mark GOOGLE SIGIN
#pragma mark


#pragma mark  SET GOOGLE SIGN IN
-(void)setGoogleSignIn
{
    GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
    [GIDSignIn sharedInstance].uiDelegate = self;
    [sigNIn setDelegate:self];
    sigNIn.shouldFetchBasicProfile = YES;
    sigNIn.allowsSignInWithBrowser = NO;
    sigNIn.allowsSignInWithWebView = YES;
    sigNIn.scopes = @[@"https://www.googleapis.com/auth/plus.login",@"https://www.googleapis.com/auth/userinfo.email",@"https://www.googleapis.com/auth/userinfo.profile"];
    sigNIn.clientID = kGoogleClientId;
    
    // Uncomment to automatically sign in the user.
    //[[GIDSignIn sharedInstance] signInSilently];
}



#pragma mark GOOGLE SIGIN DELEGATES

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    NSLog(@"1.signInWillDispatch");
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
    [DejalBezelActivityView   removeViewAnimated:YES];
    NSLog(@"2.presentViewController");
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    NSLog(@"3.dismissViewController");
    [self dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark AFTER GIDSign IN DELEGATE
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *name = user.profile.name;
    NSString *email = user.profile.email;
    
    NSLog(@"userId = %@",userId);
    NSLog(@"idToken = %@",idToken);
    NSLog(@"name = %@",name);
    NSLog(@"email = %@",email);
    
    
    //SOCIAL LOGIN CHECK G+
    if (userId.length == 0) {
    }
    else
    {
        
        [mdictSocial  setObject:kSocialTypeGoggle forKey:kSocialType];
        [mdictSocial  setObject:userId forKey:@"userId"];
        [mdictSocial  setObject:idToken forKey:@"idToken"];
        [mdictSocial  setObject:name forKey:@"name"];
        [mdictSocial  setObject:email forKey:@"email"];
        
        
        //CHECK G+ LOGIN
        [self  webserviceCallForGoogleLoginCheck:userId];
    }
}


- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}





#pragma mark WEBSERVICE- GOOGLE LOGIN CHECK
-(void)webserviceCallForGoogleLoginCheck:(NSString *)strGoogleId
{
    //    socialLoginCheck
    //    gplus_id
    //
    
    //WEBSERVICE CALL
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary *dictPara = @{@"action":@"socialLoginCheck",@"gplus_id" : strGoogleId};
    
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            if ([[result  objectForKey:@"statusCode"] integerValue] == 0) {
                
                NSLog(@"result = %@",result);
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Social Login" subtitle:strmsg type:TSMessageNotificationTypeError];
                [self   performSegueWithIdentifier:ksegue_socialLogin sender:nil];
           }
            else
            {
                
               
                
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Social Login"  subtitle:strmsg type:TSMessageNotificationTypeSuccess];
                NSLog(@"result = %@",result);
                //GOOGLE SIGN IN SUCCESS RESPONSE
                if ([[result   allKeys]  containsObject:@"Result"]) {
                    NSDictionary * dictResult = [[NSDictionary  alloc] init];
                    dictResult = [result   objectForKey:@"Result"];
                    //SESSION ID
                    if ([[dictResult   allKeys]  containsObject:@"session_id"])
                    {
                        
                        objglobalValues.strCustomerGroupId = [dictResult   objectForKey:@"customer_group_id"];
                        objglobalValues.strCustomerId = [dictResult   objectForKey:@"user_id"];
                        objglobalValues.strFirstName = [dictResult   objectForKey:@"name"];
                        objglobalValues.strSessionID = [dictResult   objectForKey:@"session_id"];
                        objglobalValues.strCartCount = [dictResult   objectForKey:@"cart_count"];
                        [self  performSegueWithIdentifier:ksegueLoginSuccess sender:nil];
                    }
                }
            }
        }
    }];
}



#pragma mark
#pragma mark PREPARE FOR SEGUE
#pragma mark

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier  isEqualToString:ksegue_socialLogin]) {
        RegistrationViewController * objRegistration = segue.destinationViewController;
        objRegistration.dictSocialData = [mdictSocial mutableCopy];
        
    }
}








/*
 
 //G+ LOG OUT
 GIDSignIn*sign=[GIDSignIn sharedInstance];
 NSLog(@"userId = %@",sign.currentUser.userID);
 if (sign.currentUser.userID.length == 0) {
 NSLog(@"Need to sign IN");
 }
 else
 {
 NSLog(@"Need to sign OUT");
 [[GIDSignIn sharedInstance] signOut];
 }
 */


@end
