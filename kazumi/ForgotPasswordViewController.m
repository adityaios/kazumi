//
//  ForgotPasswordViewController.m
//  kazumi
//
//  Created by NM8 on 27/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "ForgotPassValidationViewController.h"
@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self CustomBackButton];
    [self  viewSetUP];
    self.title = @"Forget Password";
    
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.txfdEmail    becomeFirstResponder];
}





-(void)viewSetUP
{
    
    
    //EMAIL
    self.txfdEmail.layer.borderColor = [UIColor  grayColor].CGColor;
    self.txfdEmail.layer.borderWidth = 0.3;
    self.txfdEmail.layer.masksToBounds = true;

    
    
    UIView * paddingViewEmail = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    //paddingViewEmail.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    paddingViewEmail.backgroundColor = [UIColor   whiteColor];
    
    
    self.txfdEmail.leftView = paddingViewEmail;
    self.txfdEmail.leftViewMode = UITextFieldViewModeAlways;
    
    //self.btnSubmit.layer.borderWidth = 2.0;
    self.btnSubmit.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    //self.btnSubmit.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnSubmit.layer.cornerRadius = kButtonCornerRadius;
    self.btnSubmit.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
}














#pragma mark
#pragma mark CustomBackButton
#pragma mark
-(void)CustomBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)btnSubmitClicked:(id)sender {
    //Validation
    self.txfdEmail.text = [self.txfdEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    //EMAIL
    while ([self.txfdEmail.text rangeOfString:@"  "].location != NSNotFound) {
        self.txfdEmail.text = [self.txfdEmail.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    if (self.txfdEmail.text.length == 0 ) {
        [TSMessage showNotificationWithTitle:@"Validation Error"
                                    subtitle:@"Please Fill all Fields"
                                        type:TSMessageNotificationTypeError];
        return;
    }
    
    
    /*
    //EMAIL VALIDATION
    if (![self  NSStringIsValidEmail:self.txfdEmail.text]) {
        [TSMessage showNotificationWithTitle:@"EMAIL ERROR"
                                    subtitle:@"Please enter valid email"
                                        type:TSMessageNotificationTypeError];
        return;
        
    }
     */
    
    
    if ([Reachability  isConnected]) {
        [self  webserviceCallForForgetPassword];
    }
}





#pragma mark
#pragma mark webserviceCallForForgetPassword
#pragma mark
-(void)webserviceCallForForgetPassword
{
    //WEBSERVICE CALL
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary *dict = @{@"action" : @"forgotPassword",@"user_name":self.txfdEmail.text};
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"error %@",error);
        }
        else
        {
            NSLog(@"result %@",result);
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Forget Password" subtitle:strmsg type:TSMessageNotificationTypeSuccess];
                ForgotPassValidationViewController *frtPass= [[ForgotPassValidationViewController alloc] init];
                [self.navigationController pushViewController:frtPass animated:YES];
                
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Forget Password" subtitle:strmsg type:TSMessageNotificationTypeError];
            }
        }
        
    }
     ];
    
}




-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


#pragma mark
#pragma mark  TEXTFILED DELEGATE
#pragma mark
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField   resignFirstResponder];
    return YES;
}




@end
