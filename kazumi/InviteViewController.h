//
//  InviteViewController.h
//  kazumi
//
//  Created by NM8 on 02/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "Network.h"
#import "DejalActivityView.h"
#import "TSMessage.h"
#import "GlobalValues.h"
#import "Constant.h"
#import "KazumiMacros.h"


@interface InviteViewController : UIViewController
{
    GlobalValues * objglobalValues;
    NSMutableArray * marrAboutUsOptions;
}
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UIButton *btn;



//METHODS
-(void)setUpUI;
-(void)intializerMethod;
-(void)customBackButton;
-(void)webserviceCallForinvitefriend;
-(BOOL) NSStringIsValidEmail:(NSString *)checkString;

@end
