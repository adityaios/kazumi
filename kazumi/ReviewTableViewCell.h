//
//  ReviewTableViewCell.h
//  kazumi
//
//  Created by Yashvir on 08/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface ReviewTableViewCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet ASStarRatingView *viewStar;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblText;





@end
