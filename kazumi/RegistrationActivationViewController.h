//
//  RegistrationActivationViewController.h
//  kazumi
//
//  Created by Yashvir on 14/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KazumiMacros.h"
#import "TSMessage.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#import "Network.h"
#import "Constant.h"


@interface RegistrationActivationViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblActivation;
@property (weak, nonatomic) IBOutlet UITextField *txfdActivation;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)btnsubmitclicked:(UIButton *)sender;



//METHODS
-(void)viewSetUP;
-(void)customBackButton;
-(void)webserviceCallForRegistrationActivationCode;



@end
