//
//  Network.m
//  kazumi
//
//  Created by Yashvir on 18/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "Network.h"

@implementation Network



#pragma mark
#pragma mark  GET REQUEST
#pragma mark



+(void)getWebserviceWithBaseUrl:(NSString *)strbaseUrl withParameters:(NSDictionary *)dictPara andCompletionHandler:(void (^)(id, NSError *))completionHandler
{
    
    NSLog(@"dictPara = %@",dictPara);
    
    NSURL *baseURL = [NSURL URLWithString:strbaseUrl];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    /*
    [manager GET:kBaseURLGetRequest parameters:dictPara success:^(NSURLSessionDataTask *task, id responseObject) {
        completionHandler(responseObject,nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completionHandler(nil,error);
    }];
    
    */
    
    [manager POST:kBaseURLGetRequest parameters:dictPara success:^(NSURLSessionDataTask *task, id responseObject) {
      completionHandler(responseObject,nil);
         NSLog(@"responseObject = %@",responseObject);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
       completionHandler(nil,error);
    }];
    
}






@end
