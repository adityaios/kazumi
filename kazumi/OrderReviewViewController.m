//
//  OrderReviewViewController.m
//  kazumi
//
//  Created by Yashvir on 17/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "OrderReviewViewController.h"

@interface OrderReviewViewController ()

@end

@implementation OrderReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  setUpUI];
    [self  intializerMethod];
    [self  customBackButton];
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([Reachability isConnected]) {
        [self  webserviceCallForCartList];
    }
}




#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    self.title = @"Order Review";
    objGlobalValue = [GlobalValues   sharedManager];
    
    UINib *nib = [UINib nibWithNibName:@"OrderReviewTableViewCell" bundle:nil];
    [self.tblvOrderReview registerNib:nib forCellReuseIdentifier:@"OrderCell"];
    
    // self.tblvOrderReview.estimatedRowHeight = 130.0;
    // self.tblvOrderReview.rowHeight = UITableViewAutomaticDimension;
    self.tblvOrderReview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.view.backgroundColor =  UIColorFromRGBWithAlpha(kazumiRedColor,1.0);
}




#pragma mark
#pragma mark SET UI
#pragma mark
-(void)setUpUI
{
    self.tblvOrderReview.frame = CGRectMake(0,0,SelfViewWidth, SelfViewHeight);
    self.view.backgroundColor =  UIColorFromRGBWithAlpha(kazumiRedColor,1.0);
    //Footer View
    //   self.viewFooter.backgroundColor =  UIColorFromRGBWithAlpha(kazumiYellowColor,1.0);
    //   self.viewFooter.layer.borderColor = [UIColor  grayColor].CGColor;
    //   self.viewFooter.layer.borderWidth = 1.0;
    
    //BTN SELECT PAYMENT
    //self.btnSelectPayment.layer.borderWidth = 2.0;
    self.btnSelectPayment.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    //self.btnSelectPayment.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btnSelectPayment.layer.cornerRadius = kButtonCornerRadius;
    self.btnSelectPayment.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Regular size:17];
    
    
    //VIEW  SHIPPING
    self.viewFreeShipping.layer.borderWidth = 1.0;
    self.viewFreeShipping.layer.borderColor =  [UIColor  grayColor].CGColor;
    self.viewFreeShipping.layer.cornerRadius = 2.0;
    
    //VIEW  PRICING
    self.viewPayment.layer.borderWidth = 1.0;
    self.viewPayment.layer.borderColor =  [UIColor  grayColor].CGColor;
    self.viewPayment.layer.cornerRadius = 2.0;
    
}



#pragma mark customBackButton
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark set ProductPrice Labels
-(void)setProductPriceLabels:(NSDictionary *)dict
{
    
    //TOTAL PRICE
    if ([[dict  allKeys]  containsObject:@"total"]) {
        NSString * strtotalPrice = [NSString   stringWithFormat:@"Rs.%@",[dict  objectForKey:@"total"]];
        self.lblTotalPrice.text = strtotalPrice;
    }
    else
    {
        self.lblTotalPrice.text = @"";
    }
    
    
    //Coupon_Code
    if ([[dict  allKeys]  containsObject:@"coupon_code"]) {
        NSString * strCouponCode = [NSString   stringWithFormat:@"%@",[dict  objectForKey:@"coupon_code"]];
        if (strCouponCode.length == 0) {
            strCouponCode = @"NA";
        }
        self.lblCouponCode.text = strCouponCode;
    }
    else
    {
        self.lblCouponCode.text = @"NA";
    }
    
    
    
    
    //Coupon_Value
    if ([[dict  allKeys]  containsObject:@"coupon_value"]) {
        NSString * strCouponValue = [NSString   stringWithFormat:@"%@",[dict  objectForKey:@"coupon_value"]];
        
        if (strCouponValue.length == 0) {
            strCouponValue = @"NA";
            self.lblCouponValue.text = strCouponValue;
            
        }
        else
        {
            self.lblCouponValue.text = [NSString   stringWithFormat:@"Rs.%@", strCouponValue];
        }
        
        
    }
    else
    {
        self.lblCouponValue.text = @"NA";
    }
    
    
    //Discount_on_refer
    if ([[dict  allKeys]  containsObject:@"discount_on_refer"]) {
        NSString * strDiscount = [NSString   stringWithFormat:@"Rs.%@",[dict  objectForKey:@"discount_on_refer"]];
        if (strDiscount.length == 0) {
            strDiscount = @"NA";
        }
        
        self.lblDiscount.text = strDiscount;
    }
    else
    {
        self.lblDiscount.text = @"NA";
    }
    
    
    //PAYABLE AMOUNT
    if ([[dict  allKeys]  containsObject:@"grand_total"]) {
        NSString * strPayableAmount = [NSString   stringWithFormat:@"Rs.%@",[dict  objectForKey:@"grand_total"]];
        self.lblPayableAmount.text = strPayableAmount;
    }
    else
    {
        self.lblPayableAmount.text = @"NA";
    }
}







#pragma mark
#pragma mark IBACTIONS
#pragma mark

#pragma mark btnDeleteClicked
-(void)btnDeleteClicked:(UIButton*)sender{
    UIButton *button = (UIButton *)sender;
    CGPoint buttonOriginInTableView = [button convertPoint:CGPointZero toView:self.tblvOrderReview];
    NSIndexPath *indexPath = [self.tblvOrderReview indexPathForRowAtPoint:buttonOriginInTableView];
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    CartModel * objCart = [marrDataCartModel  objectAtIndex:indexPath.row];
    
    
    
    //ALERT FOR DELETE
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Cart Item Delete"
                                  message:@"Remove item from Cart?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             [self  webserviceCallForDeleteProductFromCart:objCart];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}






#pragma mark btnSelectPaymentClicked
- (IBAction)btnSelectPaymentClicked:(UIButton *)sender {
    NSLog(@"Btn select Payment");
    if (marrDataCartModel.count == 0) {
        [TSMessage  showNotificationInViewController:self title:@"No product Available" subtitle:nil  type:TSMessageNotificationTypeWarning];
    }
    else
    {
        PaymentMethodTableViewController *  VCPayment = [[PaymentMethodTableViewController alloc] init];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        VCPayment = [storyboard instantiateViewControllerWithIdentifier:@"SelectPayMethod"];
        VCPayment.dictOrderPass = [NSDictionary  new];
        VCPayment.dictOrderPass = [mdictResult  mutableCopy];
        NSLog(@"dictOrderPass = %@",VCPayment.dictOrderPass);
        [self.navigationController pushViewController:VCPayment animated:YES];
        
    }
}











#pragma mark
#pragma mark WEBSERVICE CALL
#pragma mark

#pragma mark webserviceCallForCartList
-(void)webserviceCallForCartList
{
    
    
    //LOADER
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSString  *strDeviceID =  objGlobalValue.strDeviceID;
    
    //PARAMETER DICT
    NSDictionary *dictPara = @{@"action":@"cartlist",@"customer_id":objGlobalValue.strCustomerId,@"device_id":strDeviceID,@"cart_login_type":@"L"};
    NSLog(@"dictPara = %@",dictPara);
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dictPara andCompletionHandler:^(id result, NSError *error) {
        //REMOVE LOADER
        [DejalBezelActivityView  removeViewAnimated:YES];
        
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                
                //RESPONSE
                NSLog(@"%@",result);
                mdictResult = [NSDictionary  new];
                mdictResult  = (NSDictionary*)result;
                
                
                
                
                NSArray * arrJsonResponse = [result  objectForKey:@"product"];
                
                marrDataCartModel = [NSMutableArray  new];
                for (NSDictionary * dictProduct in arrJsonResponse) {
                    CartModel * objcart = [CartModel  new];
                    objcart.strProductName = [dictProduct objectForKey:@"product_name"];
                    objcart.strCartId = [dictProduct objectForKey:@"cart_id"];
                    objcart.strProductId = [dictProduct objectForKey:@"product_id"];
                    objcart.arrOptions = [dictProduct objectForKey:@"options"];
                    objcart.strProductImgUrl = [dictProduct objectForKey:@"product_image"];
                    objcart.strProductQuant = [dictProduct objectForKey:@"quantity"];
                    objcart.strProductPrice = [dictProduct objectForKey:@"price"];
                    [marrDataCartModel   addObject:objcart];
                }
                
                
                
                //Product Prices
                [self  setProductPriceLabels:mdictResult];
                [self.tblvOrderReview   reloadData];
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage showNotificationWithTitle:@"Error"
                                            subtitle:strmsg
                                                type:TSMessageNotificationTypeError];
                
            }
        }
    }];
    
    
    
    /*
     //FOR DEBUGGing
     NSString *filePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"CartListProducts" ofType:@"json"];
     NSData *data = [NSData dataWithContentsOfFile:filePath];
     
     NSError *error = nil;
     NSDictionary * result  = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
     
     if (error) {
     NSLog(@"error = %@",error.localizedDescription);
     }
     else
     {
     NSLog(@"result = %@",result);
     }
     
     
     NSArray * arrJsonResponse = [result  objectForKey:@"product"];
     
     marrDataCartModel = [NSMutableArray  new];
     for (NSDictionary * dictProduct in arrJsonResponse) {
     CartModel * objcart = [CartModel  new];
     objcart.strProductName = [dictProduct objectForKey:@"product_name"];
     objcart.strCartId = [dictProduct objectForKey:@"cart_id"];
     objcart.strProductId = [dictProduct objectForKey:@"product_id"];
     objcart.arrOptions = [dictProduct objectForKey:@"options"];
     objcart.strProductImgUrl = [dictProduct objectForKey:@"product_image"];
     objcart.strProductQuant = [dictProduct objectForKey:@"quantity"];
     objcart.strProductPrice = [dictProduct objectForKey:@"price"];
     [marrDataCartModel   addObject:objcart];
     }
     [self.tblvOrderReview   reloadData];
     */
    
    
}


#pragma mark webserviceCallForDeleteProductFromCart
-(void)webserviceCallForDeleteProductFromCart:(CartModel *)objcart
{
    /*
     deletefromcart
     cart_id
     customer_id
     device_id
     cart_login_type		L - for login users, G- guest login
     */
    
    
    
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSMutableDictionary * mdict = [NSMutableDictionary   new];
    [mdict  setObject:@"deletefromcart" forKey:@"action"];
    [mdict  setObject:objcart.strCartId forKey:@"cart_id"];
    [mdict  setObject:objGlobalValue.strCustomerId forKey:@"customer_id"];
    [mdict  setObject:objGlobalValue.strDeviceID forKey:@"device_id"];
    [mdict  setObject:@"L" forKey:@"cart_login_type"];
    NSLog(@"mdict = %@",mdict);
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:mdict andCompletionHandler:^(id result, NSError *error) {
        
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            //CHECK STATUS CODE
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                
                //SUCCESS MSG
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:strmsg subtitle:nil  type:TSMessageNotificationTypeSuccess];
                
                //RESPONSE
                NSLog(@"%@",result);
                objGlobalValue.strCartCount=[result objectForKey:@"product_count"];
                [self  webserviceCallForCartList];
                
            }
            else
            {
                
                
                //REMOVE LOADER
                [DejalBezelActivityView  removeViewAnimated:YES];
                NSString * strmsg = [result   objectForKey:@"msg"];
                
                //When Only One Product Item is in Cart
                if ([strmsg  isEqualToString:@"No product in the cart"]) {
                    objGlobalValue.strCartCount = @"0";
                    [self.navigationController  popToRootViewControllerAnimated:YES];
                }
                
                [TSMessage  showNotificationInViewController:self title:@"Error" subtitle:strmsg  type:TSMessageNotificationTypeError];
                
            }
        }
    }];
}



#pragma mark
#pragma mark TABLEVIEW DATASOURCE
#pragma mark


#pragma mark numberOfSectionsInTableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



#pragma mark numberOfRowsInSection
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return marrDataCartModel?marrDataCartModel.count:0;
    
}



#pragma mark cellForRowAtIndexPath
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderReviewTableViewCell * tcell =  [tableView dequeueReusableCellWithIdentifier:@"OrderCell"];
    
    //CELL BORDER
    tcell.contentView.layer.borderColor =  [UIColor   lightGrayColor].CGColor;
    tcell.contentView.layer.borderWidth = 1.0;
    
    //Display Data
    
    CartModel * objcart = [CartModel  new];
    objcart  =    [marrDataCartModel objectAtIndex:indexPath.row];
    
    //Img
    [tcell.imgViewProduct setImageWithURL:[NSURL  URLWithString:objcart.strProductImgUrl] placeholderImage:[UIImage  imageNamed:@"placeholder"]];
    
    //Name
    objcart.strProductName = [objcart.strProductName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    tcell.lblProductName.text = objcart.strProductName;
    
    //Price
    objcart.strProductPrice = [objcart.strProductPrice  stringByReplacingOccurrencesOfString:@"," withString:@""];
    NSString * strPrice = [NSString  stringWithFormat:@"Rs %.02f",[objcart.strProductPrice floatValue]];
    tcell.lblTotalAmount.text = strPrice;
    
    
    //Quant
    NSString * strQuant = [NSString  stringWithFormat:@"Qty : %ld",(long)objcart.strProductQuant.integerValue];
    tcell.lblQty.text = strQuant;
    //Options
    NSMutableString * mstrOpt = [NSMutableString   new];
    NSArray * arrOpt = [[NSArray  alloc]  init];
    arrOpt = [objcart.arrOptions  mutableCopy];
    for (NSDictionary * dict in arrOpt) {
        [mstrOpt   appendString:[dict  objectForKey:@"opt_name"]];
        [mstrOpt   appendString:@" : "];
        [mstrOpt   appendString:[dict  objectForKey:@"opt_value"]];
        [mstrOpt   appendString:@"\n"];
    }
    tcell.lblOptions.text = mstrOpt;
    [tcell.btnDelete   addTarget:self action:@selector(btnDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    return tcell;
    
}



#pragma mark titleForHeaderInSection
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString * strTitle = @"Order Review";
    return strTitle;
}

#pragma mark heightForHeaderInSection
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return UITableViewAutomaticDimension;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}


@end
