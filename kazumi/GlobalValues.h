//
//  GlobalValues.h
//  AirSpace
//
//  Created by EL GROUP TOUCHSCREEN MAC on 30/06/15.
//  Copyright (c) 2015 EL Group International. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GlobalValues : NSObject
+ (id)sharedManager;
@property(nonatomic,strong) NSString * strCustomerGroupId;
@property(nonatomic,strong) NSString * strCustomerId;
@property(nonatomic,strong) NSString * strFirstName;
@property(nonatomic,strong) NSString * strSessionID;
@property(nonatomic,strong) NSString * strCartCount;
@property(nonatomic,strong) NSString * strDeviceID;

@end
