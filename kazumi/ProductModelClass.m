//
//  ProductModelClass.m
//  kazumi
//
//  Created by Yashvir on 26/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "ProductModelClass.h"

@implementation ProductModelClass
- (id)init {
    self = [super init];
    if (self) {
        _strActual_price = [[NSString alloc] init];
        _strDiscount_type = [[NSString alloc] init];
        _strDiscount_value = [[NSString alloc] init];
        _strDiscounted_amount = [[NSString alloc] init];
        _strProduct_id = [[NSString alloc] init];
        _strProduct_image = [[NSString alloc] init];
        _strProduct_Name = [[NSString alloc] init];
        
    }
    return self;
}


@end
