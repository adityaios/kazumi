//
//  FilterOptionsViewController.h
//  kazumi
//
//  Created by Yashvir on 17/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "DejalActivityView.h"
#import "GlobalValues.h"
#import "Network.h"
#import "Constant.h"
#import "WebserviceConstant.h"
#import "TSMessage.h"
#import "KazumiMacros.h"
#import "CatModel.h"
#import "ProductModelClass.h"
#import "ASValueTrackingSlider.h"

@protocol FilterProductListDelegate <NSObject>
- (void)sendFilteredProductsBackToProductListController:(NSMutableArray*)marrProducts;
@end


@interface FilterOptionsViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>{
    NSString  * strmin ;
    NSString  * strmax;
    NSArray * arrOption;
    GlobalValues * objGlobalValues;
    NSIndexPath * lastIndexPath;
    NSMutableArray * marrFilterProducts;
    
    NSMutableArray * marrSelectedRows;
    
}

@property (nonatomic, strong) id <FilterProductListDelegate> FilterDelegateObject;
@property(nonatomic,strong) CatModel * objCatModelFilter;




@property (strong, nonatomic) IBOutlet UILabel *lblMax;
@property (weak, nonatomic) IBOutlet UILabel *lblPricehead;
@property (weak, nonatomic) IBOutlet UITableView *tblvOption;
@property (strong, nonatomic) IBOutlet UIView *tblvHeader;
@property (strong, nonatomic) IBOutlet ASValueTrackingSlider *sliderPrice;



//METHODS
-(void)viewSetUP;
-(void)intializerMethod;
-(void)customBackButton;
-(void)webserviceCallForFilterOptions;
-(void)webserviceCallToFindFilterProducts:(NSDictionary*)dict;
-(void)customizeSliderMin:(NSString *)min AndWithMax:(NSString*)max;


@end
