//
//  ForgotPasswordViewController.h
//  kazumi
//
//  Created by NM8 on 27/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "DejalActivityView.h"
#import "Network.h"
#import "GlobalValues.h"
#import "TSMessage.h"
#import "Reachability.h"
#import "KazumiMacros.h"
#import "Constant.h"

@interface ForgotPasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txfdEmail;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

- (IBAction)btnSubmitClicked:(id)sender;

//Methods
-(void)viewSetUP;
-(void)CustomBackButton;
-(void)webserviceCallForForgetPassword;







@end
