//
//  GlobalValues.m
//  AirSpace
//
//  Created by EL GROUP TOUCHSCREEN MAC on 30/06/15.
//  Copyright (c) 2015 EL Group International. All rights reserved.
//

#import "GlobalValues.h"

@implementation GlobalValues

+ (id)sharedManager
{
    static id sharedManager;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}


@end
