//
//  SignInViewController.h
//  kazumi
//
//  Created by Yashvir on 14/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

/*
SushilNM
123456
*/

#import <UIKit/UIKit.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "Constant.h"
#import "KazumiMacros.h"
#import "TSMessage.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#import "Network.h"
#import "GlobalValues.h"
#import "ForgotPasswordViewController.h"
#import "RegistrationViewController.h"


@interface SignInViewController : UIViewController<GIDSignInUIDelegate,GIDSignInDelegate,UITextFieldDelegate>{
     GlobalValues * objglobalValues;
     NSMutableDictionary * mdictSocial;
}




@property (weak, nonatomic) IBOutlet UIImageView *imgViewdivider;

@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButtonbck;
@property (weak, nonatomic) IBOutlet UITextField *txfdUserName;
@property (weak, nonatomic) IBOutlet UITextField *txfdpswd;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnForgetPswd;
@property (weak, nonatomic) IBOutlet UIButton *FBlogin;

@property (weak, nonatomic) IBOutlet UIButton *googleSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UILabel *lblSingleClick;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewArrowLogin;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewArrowRegister;
@property (weak, nonatomic) IBOutlet GIDSignInButton *viewGoogleSignin;





- (IBAction)btnFacebookloginClicked:(id)sender;
- (IBAction)btnLoginClicked:(UIButton *)sender;
- (IBAction)btnForgetPswd:(UIButton *)sender;






//METHODS
-(void)setUpUI;
-(void)intializerMethod;
-(void)setGoogleSignIn;
-(void)webserviceCallForLogin;
-(void)setUpGlobalValuesOnLoginSuccess:(NSDictionary*)dictResults;
-(void)webserviceCallForGoogleLoginCheck:(NSString*)strGoogleId;
-(void)webserviceCallForFaceBookLoginCheck:(NSString*)strFbId;
-(void)fbGraphAPICall;


@end
