//
//  PrivacyPolicyViewController.m
//  kazumi
//
//  Created by Yashvir on 20/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "PrivacyPolicyViewController.h"

@interface PrivacyPolicyViewController ()

@end

@implementation PrivacyPolicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self   intializerMethod];
    [self   setUpUI];
    [self  webserviceCallPrivacy];
}



#pragma mark
#pragma mark intializerMethod
#pragma mark

-(void)intializerMethod
{
    
    
    objglobalValues = [GlobalValues   sharedManager];
    
    
    //REVEAL VIEW CONTROLLER
    self.barButtonPrivacy.target = self.revealViewController;
    self.barButtonPrivacy.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    //WEBVIEW
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    self.title = @"Privacy Policy";
}




#pragma mark
#pragma mark setUpUI
#pragma mark
-(void)setUpUI
{
    
    [self cartBarButton];
    
    
}


-(void)cartBarButton
{
    
    
    MKNumberBadgeView *number = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(25, -10,20,20)];
    number.value = objglobalValues.strCartCount.integerValue;
    number.shadow = NO;
    number.shine = YES;
    number.font = [UIFont  fontWithName:kFontSFUIDisplay_Bold size:14];
    number.strokeColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);;
    number.layer.cornerRadius = 2.0;
    
    
    
    UIImage * imgcart = [UIImage  imageNamed:@"cart"];
    // Allocate UIButton
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0,0,30,30);
    [btn setImage:imgcart forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cartButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:number]; //Add NKNumberBadgeView as a subview on UIButton
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
    
}



-(IBAction)cartButtonClicked:(id)sender
{
    if (objglobalValues.strCartCount.integerValue == 0) {
        [TSMessage  showNotificationInViewController:self title:@"Cart" subtitle:@"No Product Available in Cart" type:TSMessageNotificationTypeMessage];
    }
    else
    {
        CartViewController * objCartVC = [self.storyboard   instantiateViewControllerWithIdentifier:@"CartViewController"];
        [self.navigationController pushViewController:objCartVC animated:YES];
        
    }
}






#pragma mark
#pragma mark webserviceCallPrivacy
#pragma mark
-(void)webserviceCallPrivacy
{
    if ([Reachability isConnected]) {
        //WEBSERVICE CALL
        [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
        NSDictionary * dict = @{@"action":@"getpageinfo",@"page_id":@"2"};
        [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
            [DejalBezelActivityView  removeViewAnimated:YES];
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                    
                    NSDictionary * dictResult = [[result  objectForKey:@"Result"] objectAtIndex:0];
                    NSMutableString * mstrhtmlContent = [dictResult  objectForKey:@"content"];
                    [self   setWebViewUsingHtlml:mstrhtmlContent];
                    
                }
                else
                {
                    
                    NSString * strmsg = [result   objectForKey:@"msg"];
                    [TSMessage showNotificationWithTitle:@"Error"
                                                subtitle:strmsg
                                                    type:TSMessageNotificationTypeError];
                    
                    
                }
            }
        }];
    }
}


-(void)setWebViewUsingHtlml:(NSMutableString *)strhtml
{
    NSLog(@"%@",strhtml);
    
    NSMutableString *html = [NSMutableString stringWithString: @"<html><head><title></title></head><body style=\"background:transparent;\">"];
    //continue building the string
    [html appendString:strhtml];
    [html appendString:@"</body></html>"];
    [self.webViewPrivacy loadHTMLString:[html description] baseURL:nil];
    
}













@end
