//
//  ProductModelClass.h
//  kazumi
//
//  Created by Yashvir on 26/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductModelClass : NSObject

@property(nonatomic,strong) NSString * strActual_price;
@property(nonatomic,strong) NSString * strDiscount_type;
@property(nonatomic,strong) NSString * strDiscount_value;
@property(nonatomic,strong) NSString * strDiscounted_amount;
@property(nonatomic,strong) NSString * strProduct_id;
@property(nonatomic,strong) NSString * strProduct_image;
@property(nonatomic,strong) NSString * strProduct_Name;



@end

