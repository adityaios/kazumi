//
//  Network.h
//  kazumi
//
//  Created by Yashvir on 18/11/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "WebserviceConstant.h"

@interface Network : NSObject

+(void)getWebserviceWithBaseUrl:(NSString*)strbaseUrl  withParameters:(NSDictionary*)dictPara andCompletionHandler:(void (^)(id result,NSError * error))completionHandler;



@end
