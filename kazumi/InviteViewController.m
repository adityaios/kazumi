//
//  InviteViewController.m
//  kazumi
//
//  Created by NM8 on 02/12/15.
//  Copyright © 2015 Nishkrant Media. All rights reserved.
//

#import "InviteViewController.h"

@interface InviteViewController ()
- (IBAction)btnInvite:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailField;

@end

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self  setUpUI];
    [self  intializerMethod];
    [self  customBackButton];
    
    self.emailField.layer.borderColor = UIColor.grayColor.CGColor;
    self.emailField.layer.borderWidth = 0.3;
    self.emailField.layer.masksToBounds = true;
    
    
    
    
}



#pragma mark
#pragma mark intializerMethod
#pragma mark
-(void)intializerMethod
{
    self.title = @"Invite Friends";
    objglobalValues=[GlobalValues sharedManager];
    
}



#pragma mark
#pragma mark setUpUI
#pragma mark
-(void)setUpUI
{
    
    self.lblEmail.font = [UIFont  fontWithName:kFontSFUIDisplay_Regular size:17];
    self.lblEmail.text = @"Email";
    
    self.emailField.font = [UIFont  fontWithName:kFontSFUIText_Regular size:14];
    self.emailField.placeholder = @"Email";
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    paddingView.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1);
    self.emailField.leftView = paddingView;
    self.emailField.leftViewMode = UITextFieldViewModeAlways;
    
    //self.btn.layer.borderWidth = 2.0;
    self.btn.backgroundColor = UIColorFromRGBWithAlpha(kazumiRedColor, 1.0);
    //self.btn.layer.borderColor =  [UIColor  redColor].CGColor;
    self.btn.layer.cornerRadius = kButtonCornerRadius;
    self.btn.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplay_Bold size:17];
    
}



#pragma mark
#pragma mark CustomBackButton
#pragma mark
-(void)customBackButton
{
    UIImage* buttonImage = [UIImage imageNamed:@"back"];
    CGRect frame = CGRectMake(10, 0, buttonImage.size.width, buttonImage.size.height);
    UIButton *bckButton = [[UIButton alloc] initWithFrame:frame];
    [bckButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [bckButton addTarget:self action:@selector(backButtonClicked:)
        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backbarButton = [[UIBarButtonItem alloc] initWithCustomView:bckButton];
    self.navigationItem.leftBarButtonItem=backbarButton;
    
}
- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



- (IBAction)btnInvite:(id)sender {
    //VALIDATIONS DOUBLE SPACE
    // EMAIL
    while ([self.emailField.text rangeOfString:@"  "].location != NSNotFound) {
        self.emailField.text = [self.emailField.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    //EMAIL VALIDATION
    if (![self  NSStringIsValidEmail:self.emailField.text]) {
        [TSMessage  showNotificationInViewController:self title:@"EMAIL ERROR" subtitle:@"Please enter valid email"  type:TSMessageNotificationTypeError];
    }
    
    
    if ([Reachability isConnected]) {
        [self  webserviceCallForinvitefriend];
    }
    
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



#pragma mark
#pragma mark webserviceCallForchangepassword
#pragma mark
-(void)webserviceCallForinvitefriend
{
    //WEBSERVICE CALL
    [DejalBezelActivityView   activityViewForView:self.view withLabel:@"Please Wait"];
    NSDictionary * dict = @{@"action":@"invitefriend",@"customer_id":objglobalValues.strCustomerId,@"email_ids":self.emailField.text};
    NSLog(@"dict %@",dict);
    
    
    [Network  getWebserviceWithBaseUrl:kBaseURL withParameters:dict andCompletionHandler:^(id result, NSError *error) {
        [DejalBezelActivityView  removeViewAnimated:YES];
        if (error) {
            NSLog(@"%@",error);
        }
        else
        {
            if ([[result  objectForKey:@"statusCode"] integerValue] == 1) {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Invite Successfull" subtitle:strmsg  type:TSMessageNotificationTypeSuccess];
            }
            else
            {
                NSString * strmsg = [result   objectForKey:@"msg"];
                [TSMessage  showNotificationInViewController:self title:@"Invite Error" subtitle:strmsg  type:TSMessageNotificationTypeError];
            }
        }
    }];
}



#pragma mark
#pragma mark TEXTFIELD DELEGATES
#pragma mark
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField  resignFirstResponder];
    return YES;
}



@end
